<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_doctors extends CI_Model {
    
public function getDoctorExperties($id){

$query = $this->db->query("SELECT experties_description.expert FROM experties_description JOIN experties ON experties.expert_id=experties_description.id WHERE experties.doctor_id=" . (int)$id);
    
return $query->result_array();

}
    
 public function getList(){
 
 $query = $this->db->query("SELECT * FROM doctors");
     
 return $query->result_array(); 
 
 }

 public function getexperties(){
 
  $query = $this->db->query("SELECT * FROM experties_description");
     
  return $query->result_array(); 
 }
    
 public function gelenguages(){
 
  $query = $this->db->query("SELECT * FROM language_description");
     
  return $query->result_array(); 
 }
    
public function insertDoctor($data){

    
$data_new = array(
   'first_name' => $data['first_name'] ,
   'last_name' => $data['last_name'] ,
   'country' => $data['country'] , 
    'state' => $data['state'],
    'city' => $data['city'],
    'address' => $data['address'],
    'phone' => $data['phone'],
    'second_phone' => $data['second_phone'],
    'mail' => $data['email'],
    'status' => 1,
    'creator' => 'MedPay',
    'home_visit' => (isset($data['home_visit']) ? 1 : 0),
    'licens' => $data['licens'],
    'clinic' => $data['clinic'],
    'price_local' => $data['local_coin'],
    'price_dollar' => $data['dollar_coin'],
    'fax' => $data['fax'],
    'create_at' => date('Y-m-d H:i:s'),
    'comments' => $data['comments'],
    'website' => $data['website']
);

$this->db->insert('doctors', $data_new);

$id = $this->db->insert_id();  
    
 //insert lenguage
if(isset($data['lenguages'])){
 foreach($data['lenguage'] as $lenguage){
     
     $leng_array = array(
      'language_id	' => $lenguage,
      'doctor_id' => $id
     );
         
   $this->db->insert('languages', $leng_array); 
 
 }
}
  //insert experty
if(isset($data['expert'])){
 foreach($data['expert'] as $ex){
     
     $expert = array(
      'expert_id	' => $ex,
      'doctor_id' => $id
     );
         
   $this->db->insert('experties', $expert); 
 
 }
 }   
 $this->session->set_flashdata('success', 'נוסף רופא חדש');

}
    
 public function getDoctor($id){
 
  $query = $this->db->query("SELECT * FROM doctors WHERE id =" . $id);
     
 return $query->result_array();   
 
 }
    
 public function doctorLenguages($id){
 
 $query = $this->db->query("SELECT * FROM languages WHERE doctor_id =" . $id);
     
 return $query->result_array(); 
 
 }

public function doctorExperties($id){

    $query = $this->db->query("SELECT * FROM experties WHERE doctor_id =" . $id);
    return $query->result_array(); 
}
    
 public function updateDoctor($data, $id){
   $data_new = array(
    'country' => $data['country'] , 
    'state' => $data['state'],
    'city' => $data['city'],
    'address' => $data['address'],
    'phone' => $data['phone'],
    'second_phone' => $data['second_phone'],
    'mail' => $data['mail'],
    'status' => $data['status'],
    'creator' => $data['creator'],
    'licens' => $data['licens'],
    'clinic' => $data['clinic'],
    'create_at' => date('Y-m-d H:i:s'),
    'comments' => $data['comments'],
     'home_visit' => (isset($data['home_visit']) ? 1 : 0),
    'website' => $data['website']
            );

    $this->db->where('id', $id);
    $this->db->update('doctors', $data_new);
     
//update lenguages
if(isset($data['lenguages'])){
$this->db->delete('languages', array('doctor_id' => $id)); 
  foreach($data['lenguages'] as $lenguage){
     
     $leng_array = array(
      'language_id	' => $lenguage,
      'doctor_id' => $id
     );
         
   $this->db->insert('languages', $leng_array); 
 
 }
}
 //update experties
if(isset($data['expert'])){
   $this->db->delete('experties', array('doctor_id' => $id));
    foreach($data['expert'] as $ex){
     
     $expert = array(
      'expert_id	' => $ex,
      'doctor_id' => $id
     );
         
   $this->db->insert('experties', $expert); 

 }
 }
 $this->session->set_flashdata('success', 'פרטי רופא עודכנו בהצלחה');
 }

}