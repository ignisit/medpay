<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctors extends CI_Controller {
    
 public function __construct() {

		parent::__construct();
		$this -> load -> library('form_validation');
		$this -> load -> model('model_doctors');

	}


 public function index(){
  $doctors = $this->model_doctors->getList();
  $data['doctors'] = array();
  // building doctors array
  foreach($doctors as $doctor){
   $experties =  $this->model_doctors->getDoctorExperties($doctor['id']);
  $data['doctors'][]= array(
     'first_name' => $doctor['first_name'],
     'last_name' => $doctor['last_name'],
      'clinic' => $doctor['clinic'],
     'experties' => $experties,
     'city' => $doctor['city'],
    'create_at' => $doctor['create_at'],
    'status' => $doctor['status'],
    'price_dollar' => $doctor['price_dollar'],
    'id' => $doctor['id'],
    );
  }
  $data['title'] = 'doctors';            
  $data['header'] = $this->load->view('templets/header', $data);
   $data['page'] = $this->load->view('doctors', $data);
   $data['footer'] = $this->load->view('templets/footer');
  $this->load->view('templets/main', $data);    
  
 
 }

 public function insert(){
     
   $data['experties'] = $this->model_doctors->getexperties();
$data['languages'] = $this->model_doctors->gelenguages();

  if($this->input->post()){
  $this->model_doctors->insertDoctor($this->input->post());
  redirect(site_url() . 'admin/doctors');
  }
 $data['title'] = 'doctors';         
  $data['header'] = $this->load->view('templets/header', $data);
 $data['page'] = $this->load->view('insert_doctor', $data);
$data['footer'] = $this->load->view('templets/footer');
  $this->load->view('templets/main', $data);    

 
 }
    
 public function edit($id){
     
   if($this->input->post()){
    $this->model_doctors->updateDoctor($this->input->post(), $id);
    redirect(site_url() . 'admin/doctors');
   }  
   
     
   $data['title'] = 'doctors'; 
  $data['doctor'] = $this->model_doctors->getDoctor($id);
    $lenguages = $this->model_doctors->doctorLenguages($id);
    //create lenguage array
    $lenguage_arr = array();
    foreach($lenguages as $lenguage){
        
        $lenguage_arr[] = $lenguage['language_id'];
    
    
    }
   $data['doctorlen'] = $lenguage_arr;
   //create experties array
   $experties = $this->model_doctors->doctorExperties($id);
   $experties_arr = array();
   foreach($experties as $experty){
       
       $experties_arr[] = $experty['expert_id'];
   
   }
   $data['doctorEx'] = $experties_arr;
     $data['experties'] = $this->model_doctors->getexperties();
$data['languages'] = $this->model_doctors->gelenguages();
$data['doctor_id'] = $id;
  $data['header'] = $this->load->view('templets/header', $data);
 $data['page'] = $this->load->view('edit_doctor', $data);
$data['footer'] = $this->load->view('templets/footer');
  $this->load->view('templets/main', $data);    

 
 }


}