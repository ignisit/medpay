
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar" style="display:none">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Data Tables</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Responsive Datatables</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                    <?php if($this->session->flashdata('success')){ ?>
					<div class="note note-success">
						<p>
							 <?php echo $this->session->flashdata('success') ?>
						</p>
					</div>
                    <?php } ?>
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>רשימת רופאים
							</div>
							<div class="tools" style="display:none">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-bordered">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										 שם הרופא
									</th>
									<th>
										 התמחות
									</th>
									<th>
										 מיקום
									</th>
									<th>
										 תאריך עדכון אחרון
									</th>
									<th>
										 מספר מבוטחים
									</th>
                                    <th>
										 דירוג ממוצע
									</th>
                                    <th>
										 סטטוס
									</th>
                                    <th>
										 מחיר בסיסי
									</th>
                                    <th>
										 כרטיס רופא
									</th>
								</tr>
								</thead>
								<tbody>
                              <?php $counter = 1; ?>
                              <?php foreach($doctors as $doctor){ ?>
								<tr>
									<td>
										 <?php echo $counter ?>
									</td>
									<td>
										 <?php echo (!empty($doctor['first_name'])?$doctor['first_name'] . ' ' . $doctor['last_name'] : $doctor['clinic'])  ?>
									</td>
									<td>
										 <?php if(!empty($doctor['experties'])){ ?>
                                          <?php foreach($doctor['experties'] as $experty){ ?>
                                          <?php echo $experty['expert'] . ',' ?>
                                          <?php } ?>
                                        <?php } ?>
									</td>
									<td>
										<?php echo $doctor['city'] ?>
									</td>
									<td>
										 <?php echo $doctor['create_at'] ?>
									</td>
									<td>
										 200
									</td>
                                    <td>
										 5
									</td>
                                    <td>
                                            <?php if($doctor['status'] == 2){ ?>
												ממתין לאישור
                                              <?php }else if($doctor['status'] == 1){ ?>
												פעיל
                                                <?php }else if($doctor['status'] == 0){ ?>
												לא פעיל
                                                 <?php }else if($doctor['status'] == 3){ ?>
												רשימה שחורה
                                                <?php } ?>
									</td>
                                    <td>
										<?php echo $doctor['price_dollar'] . ' דולר' ?>
									</td>
                                    <td>
                                        <a href="<?php echo base_url() ?>admin/doctors/edit/<?php echo $doctor['id'] ?>">כרטיס רופא</a>
									</td>
								</tr>
                                <?php $counter = $counter + 1; ?>
								<?php }  ?>
								
								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
			</div>
			
