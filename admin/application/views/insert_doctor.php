<?php
//echo "<pre>";
//print_r($experties);
//die();
?>

			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->

			<div class="page-bar" style="display: none">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Form Stuff</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Material Design Form Controls</a>
					</li>
				</ul>

			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->

			<div class="row">
				<div class="col-md-12 ltr-area" style="float:left">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-green-haze">
								<i class="icon-settings font-green-haze"></i>
								<span class="caption-subject bold uppercase">רופא חדש</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" method="post" action="<?= base_url(); ?>admin/doctors/insert">
								<div class="form-body">
									<div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" name="first_name" class="form-control" id="form_control_1" placeholder="first name">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">first name</label>
									</div>
                                    <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" name="last_name" class="form-control" id="form_control_1" placeholder="last name">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">last name</label>
									</div>
                                     <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" name="licens" class="form-control" id="form_control_1" placeholder="license number">
											<div class="form-control-focus">
											</div>
										</div>
                                    <label class="col-md-2 control-label" for="form_control_1">licens number</label>
									</div>
                                       <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" name="email" class="form-control" id="form_control_1" placeholder="email">
											<div class="form-control-focus">
											</div>
										</div>
                                    <label class="col-md-2 control-label" for="form_control_1">email</label>
									</div>
                                         <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">התמחות</label>
										<div class="col-md-10">
                                        <?php foreach($experties as $expert){ ?>
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="expert[]" id="checkbox<?php echo $expert['id'] ?>" class="md-check" value="<?php echo $expert['id'] ?>">
													<label for="checkbox<?php echo $expert['id'] ?>">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													<?php echo $expert['expert'] ?></label>
												</div>

											</div>
                                            <?php } ?>
										</div>
									</div>
                                    <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="clinic" name="clinic">
											<div class="form-control-focus">
											</div>
										</div>
                                    <label class="col-md-2 control-label" for="form_control_1">clinic</label>
									</div>
                                     <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="price in local currency" name="local_coin">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">price in local corency</label>
									</div>
                                     <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="price in USD" name="dollar_coin">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">price in USD</label>
									</div>
                                     <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="address" name="address">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">address</label>
									</div>
                                    <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="city" name="city">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">city</label>
									</div>
                                     <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="state" name="state">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">state</label>
									</div>
                                     <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="country" name="country">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">country</label>
									</div>
                                     <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="cell phone" name="phone">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">cell phone</label>
									</div>
                                     <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="office phone" name="second_phone">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">office phone</label>
									</div>
                                     <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="fax" name="fax">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">fax</label>
									</div>
                                   <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="website" name="website">
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">website</label>
									</div>
                                   <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">שפות</label>
										<div class="col-md-10">
                                        <?php foreach($languages as $language){ ?>
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="lenguage[]" id="checkbox<?php echo (int)$language['id'] + 200  ?>" class="md-check" value="<?php echo $language['id'] ?>">
													<label for="checkbox<?php echo $language['id'] + 200 ?>">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													<?php echo $language['name'] ?> </label>
												</div>

											</div>
                                            <?php } ?>
										</div>
									</div>
                                    
                                 <div class="form-group form-md-line-input">
										<div class="col-md-10">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
                                                    	<label for="checkbox300">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													home visit</label>
													<input type="checkbox" name="home_visit" id="checkbox300" class="md-check" value="1">
												</div>

											</div>
                                          
										</div>
                                     <label class="col-md-2 control-label" for="form_control_1">home visit</label>
									</div>
                                     <div class="form-group form-md-line-input">
										<div class="col-md-10">
                                            <textarea style="direction:ltr" name="comments" placeholder="comments" cols="100" rows="5"></textarea>
											<div class="form-control-focus">
											</div>
										</div>
                                        <label class="col-md-2 control-label" for="form_control_1">comments</label>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-2 col-md-10">
                                            <button type="submit" class="btn blue">שלח</button>
											<button type="button" class="btn default" onclick="window.history.back()">ביטול</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
                    
				</div>
			</div>
			
