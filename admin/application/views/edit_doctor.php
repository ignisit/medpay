<?php
//echo "<pre>";
//print_r($doctorlen);
//print_r($languages);
//die();
?>

			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->

			<div class="page-bar" style="display:none">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Form Stuff</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Material Design Form Controls</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
              <div class="row">
				<div class="col-md-12" style="direction:ltr">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase"> פרטי הרופא</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form" method="post" class="form-horizontal" action="<?= base_url(); ?>admin/doctors/edit/<?= $doctor_id ?>">
								<div class="form-body">
									<div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">שם</label>
										<div class="col-md-10">
											<input type="text"  class="form-control" id="form_control_1" placeholder="הכנס שם" value="<?php echo $doctor[0]['first_name'] . ' ' . $doctor[0]['last_name'] ?>">
											<div class="form-control-focus">
											</div>
										</div>
									</div>

									<div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">סטטוס</label>
										<div class="col-md-10">
											<select class="form-control" id="form_control_1" name="status">
                                            <?php if($doctor[0]['status'] == 2){ ?>
												<option value="2" selected>ממתין לאישור</option>
                                            <?php }else{ ?>
                                                <option value="2">ממתין לאישור</option>
                                            <?php } ?>
                                              <?php if($doctor[0]['status'] == 1){ ?>
												<option value="1" selected>פעיל</option>
                                                  <?php }else{ ?>
                                                <option value="1">פעיל</option>
                                                <?php } ?>
                                                <?php if($doctor[0]['status'] == 0){ ?>
												<option value="0" selected>לא פעיל</option>
                                                 <?php }else{ ?>
                                                  <option value="0">לא פעיל</option>
                                                  <?php } ?>
                                                 <?php if($doctor[0]['status'] == 3){ ?>
												<option value="3" selected>רשימה שחורה</option>
                                                 <?php }else{ ?>
                                                <option value="3">רשימה שחורה</option>
                                                <?php } ?>
											</select>
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                    
                                    	<div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">מספר רישיון</label>
										<div class="col-md-10">
											<input type="text" name="licens" class="form-control" id="form_control_1" placeholder="הכנס מספר רישוי" value="<?php echo $doctor[0]['licens'] ?>">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
								</div>
							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
            
            
			<div class="row">
				<div class="col-md-6 " style="direction:ltr">
                    
                    					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered" style="min-height:1300px">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">פרטים כלליים</span>
							</div>

						</div>
						<div class="portlet-body form">
							
								<div class="form-body">
                                	<div class="form-group form-md-line-input has-info">
										<select class="form-control" name="creator" id="form_control_1">
                                           <?php if($doctor[0]['creator'] == 'MedPay'){ ?>
											<option value="MedPay" selected>MedPay</option>
											<option value="מבוטח" >מבוטח</option>
                                         <?php }else{ ?>
                                            <option value="MedPay">MedPay</option>
											<option value="מבוטח" selected>מבוטח</option>
                                         <?php } ?>
										</select>
										<label for="form_control_1">כיצד נוצר</label>
									</div>
                                    <!-- start of experties -->   
                            <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">התמחויות</label>
										<div class="col-md-10">
											<div class="md-checkbox-list">
                                          <?php foreach($experties as $experty){ ?>
                                            <?php if(in_array($experty['id'], $doctorEx)){ ?>
												<div class="md-checkbox">
													<input type="checkbox" name="expert[]" id="checkbox<?php echo $experty['id'] ?>" class="md-check" value="<?php echo $experty['id'] ?>" checked>
													<label for="checkbox<?php echo $experty['id'] ?>">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													<?php echo $experty['expert'] ?> </label>
												</div>
                                             <?php }else{ ?>
                                             <div class="md-checkbox">
													<input type="checkbox" name="expert[]" id="checkbox<?php echo $experty['id'] ?>" class="md-check" value="<?php echo $experty['id'] ?>" >
													<label for="checkbox<?php echo $experty['id'] ?>">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													<?php echo $experty['expert'] ?> </label>
												</div>
                                                <?php } ?>
                                          <?php } ?>
											</div>
										</div>
									</div>
                              <!-- end of experties -->      
                            <div class="form-group form-md-line-input col-md-10">
										<input type="text" name="clinic" class="form-control" id="form_control_1" placeholder="מרפאה" value="<?php echo $doctor[0]['clinic'] ?>">
										<label for="form_control_1">מרפאה</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    
                            <div class="form-group form-md-line-input col-md-10">
										<label class="col-md-2 control-label" for="form_control_1">שפות</label>
										<div class="col-md-10">
											<div class="md-checkbox-list">
                                          <?php foreach($languages as $language){ ?>
                                            <?php if(in_array($language['id'], $doctorlen)){ ?>
												<div class="md-checkbox">
													<input type="checkbox" name="lenguages[]" id="checkbox<?php echo (int)$language['id'] + 200 ?>" class="md-check" value="<?php echo $language['id'] ?>" checked>
													<label for="checkbox<?php echo (int)$language['id'] + 200 ?>">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													<?php echo $language['name'] ?> </label>
												</div>
                                             <?php }else{ ?>
                                             <div class="md-checkbox">
													<input type="checkbox" name="lenguages[]" id="checkbox<?php echo (int)$language['id'] + 200 ?>" class="md-check" value="<?php echo $language['id'] ?>" >
													<label for="checkbox<?php echo (int)$language['id'] + 200 ?>">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													<?php echo $language['name'] ?> </label>
												</div>
                                                <?php } ?>
                                          <?php } ?>
											</div>
										</div>
									</div>
                                
                                    <div class="form-group form-md-line-input col-md-10">
                                        <label class="col-md-3 control-label" for="form_control_1">ביקור בית</label>
                                     <div class="col-md-9">
											<div class="md-checkbox-list">
                                <div class="md-checkbox">
                                                    <?php if($doctor[0]['home_visit'] == 1){ ?>
													<input type="checkbox" name="home_visit" id="checkbox900" class="md-check" value="1" checked>
                                                     <?php }else{ ?>
                                                     <input type="checkbox" name="home_visit" id="checkbox900" class="md-check" value="1">
                                                     <?php } ?>
													<label for="checkbox900">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													home visit </label>
												</div>
                                         </div>
                                        </div>
                                     
                                    </div>
								</div>

							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
                    
                    
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">פרטי התקשרות</span>
							</div>

						</div>
						<div class="portlet-body form">
							
								<div class="form-body">
                             	<div class="form-group form-md-line-input">
										<input type="text" name="phone" class="form-control" id="form_control_1" placeholder="הכנס טלפון נייד" value="<?php echo $doctor[0]['phone'] ?>">
										<label for="form_control_1">טלפון נייד</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                   <div class="form-group form-md-line-input">
										<input type="text" name="second_phone" class="form-control" id="form_control_1" placeholder="הכנס טלפון נייח" value="<?php echo $doctor[0]['second_phone'] ?>">
										<label for="form_control_1">טלפון נייח</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                   <div class="form-group form-md-line-input">
										<input type="text" name="mail" class="form-control" id="form_control_1" placeholder="הכנס מייל" value="<?php echo $doctor[0]['mail'] ?>">
										<label for="form_control_1">מייל</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                      <div class="form-group form-md-line-input">
										<input type="text" name="website" class="form-control" id="form_control_1" placeholder="אתר אינטרנט" value="<?php echo $doctor[0]['website'] ?>">
										<label for="form_control_1">אתר אינטרנט</label>
										<span class="help-block">Some help goes here...</span>
									</div>
									<div class="form-group form-md-line-input">
										<input type="text" name="address" class="form-control" id="form_control_1" placeholder="הכנס רחוב" value="<?php echo $doctor[0]['address'] ?>">
										<label for="form_control_1">כתובת מלאה</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    		<div class="form-group form-md-line-input">
										<input type="text" name="city" class="form-control" id="form_control_1" placeholder="הכנס עיר" value="<?php echo $doctor[0]['city'] ?>">
										<label for="form_control_1">עיר</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    		<div class="form-group form-md-line-input">
										<input type="text" name="state" class="form-control" id="form_control_1" placeholder="הכנס מדינה" value="<?php echo $doctor[0]['state'] ?>">
										<label for="form_control_1">מדינה</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    <div class="form-group form-md-line-input">
										<input type="text" name="country" class="form-control" id="form_control_1" placeholder="הכנס ארץ" value="<?php echo $doctor[0]['country'] ?>">
										<label for="form_control_1">ארץ</label>
										<span class="help-block">Some help goes here...</span>
									</div>
									<div class="form-group form-md-line-input has-error">
										<input type="text" class="form-control" readonly value="5" id="form_control_1">
										<label for="form_control_1">דירוג</label>
									</div>
								</div>
								<div class="form-actions noborder">
									<button type="submit" class="btn blue">שלח</button>
									<button type="button" class="btn default" onclick="window.history.back()">ביטול</button>
								</div>
							
                            
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
				<div class="col-md-6 " style="direction:ltr">

            	<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">מחירים</span>
							</div>

						</div>
						<div class="portlet-body form">
							
								<div class="form-body">
                             	<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מחיר של המבוטח האחרון" value="54 דולר">
										<label for="form_control_1">מחיר של המבוטח האחרון</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                   <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="ממוצע של כל המבוטחים" value="60 דולר">
										<label for="form_control_1">ממוצע של כל המבוטחים</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                   <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מחיר ממוצע באותה עיר" value="40 דולר">
										<label for="form_control_1">מחיר ממוצע באותה עיר</label>
										<span class="help-block">Some help goes here...</span>
									</div>

								</div>
	
							
						</div>
					</div>
                    <!-- END SAMPLE FORM PORTLET-->
                    
                    <!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered" style="direction:rtl">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">מבוטחים שהיו אצל הרופא</span>
							</div>

						</div>
						<div class="portlet-body form">
							
								<div class="form-body">
                             	<table border="1" style="width: 100%;" id="list-doctor">
                                    <tr>
                                        <th>id</th>
                                        <th>שם</th>
                                        <th>מתי נוצר</th>
                                        <th>כמה שילמו</th>
                                        <th>דירוג</th>
                                        <th>ביקורת</th>
                                    </tr>
                                      <tr>
                                          <td>123456789</td>
                                          <td>יוסי יוסף</td>
                                          <td>22 ביוני 2015</td>
                                          <td>22 דולר</td>
                                          <td>5</td>
                                          <td>היה רופא מצויין</td>
                                    </tr>
                                        <tr>
                                          <td>123456789</td>    
                                          <td>יוסי יוסף</td>
                                          <td>22 ביוני 2015</td>
                                          <td>22 דולר</td>
                                          <td>5</td>
                                          <td>היה רופא מצויין</td>
                                    </tr>
                                        <tr>
                                          <td>123456789</td>    
                                          <td>יוסי יוסף</td>
                                          <td>22 ביוני 2015</td>
                                          <td>22 דולר</td>
                                          <td>5</td>
                                          <td>היה רופא מצויין</td>
                                    </tr>
                                </table>
								</div>
						</div>
					</div>
                     <!-- END SAMPLE FORM PORTLET-->
                <!-- comments -->
                					<div class="portlet light bordered" style="height:300px">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">הערות</span>
							</div>

						</div>
						<div class="portlet-body form">
							
	                                <div class="form-group form-md-line-input">
										<div class="col-md-12">
                                            <textarea name="comments" placeholder="הערה" style="width: 80%; height: 150px;"><?php echo $doctor[0]['comments'] ?></textarea>
											<div class="form-control-focus">
											</div>
										</div>
									</div>
							</form>
						</div>
					</div>
                <!-- comments -->
				</div>
			</div>




