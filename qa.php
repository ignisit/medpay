
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" dir="rtl" lang="he-IL"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" dir="rtl" lang="he-IL"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" dir="rtl" lang="he-IL"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" dir="rtl" lang="he-IL"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" />
    <activity
android:name="com.example.android.GizmosActivity"
android:label="@string/title_gizmos" >
<intent-filter>
    <data android:scheme="anton" />
    <action android:name="android.intent.action.VIEW" />
    <category android:name="android.intent.category.DEFAULT" />
    <category android:name="android.intent.category.BROWSABLE" /> 
</intent-filter>
        
	<title>מידע כללי | MedPay</title>
	<script type='text/javascript'>SG_POPUP_DATA = [];SG_APP_POPUP_URL = 'http://medpay.c14.co.il/wp-content/plugins/popup-builder';SG_POPUO_VERSION='2.22_0'</script><meta name='robots' content='noindex,follow' />
		<script>var sf_ajax_root = 'http://medpay.c14.co.il/wp-admin/admin-ajax.php'</script>
		<link rel="alternate" type="application/rss+xml" title="MedPay &raquo; פיד‏" href="http://medpay.c14.co.il/feed/" />
<link rel="alternate" type="application/rss+xml" title="MedPay &raquo; פיד תגובות‏" href="http://medpay.c14.co.il/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/medpay.c14.co.il\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.4.3"}};
			!function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;return g&&g.fillText?(g.textBaseline="top",g.font="600 32px Arial","flag"===a?(g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3):"diversity"===a?(g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e):("simple"===a?g.fillText(h(55357,56835),0,0):g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag"),unicode8:d("unicode8"),diversity:d("diversity")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag&&c.supports.unicode8&&c.supports.diversity||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='awd-style-css'  href='http://medpay.c14.co.il/wp-content/plugins/aryo-widget-device/assets/css/style.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='-css'  href='http://medpay.c14.co.il/wp-content/plugins/bh-custom-preloader/css/bh_custom_preloader.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://medpay.c14.co.il/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-rtl-css'  href='http://medpay.c14.co.il/wp-content/plugins/contact-form-7/includes/css/styles-rtl.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-ui-theme-css'  href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css?ver=1.11.4' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-ui-timepicker-css'  href='http://medpay.c14.co.il/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='iul-style-css'  href='http://medpay.c14.co.il/wp-content/plugins/idle-user-logout/css/style.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='my_waze_style-css'  href='http://medpay.c14.co.il/wp-content/plugins/my-waze/style.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='pbpl_css-css'  href='http://medpay.c14.co.il/wp-content/plugins/pb-add-on-placeholder-labels/assets/css/style.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='pbpl_css_rtl-css'  href='http://medpay.c14.co.il/wp-content/plugins/pb-add-on-placeholder-labels/assets/css/rtl.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='http://medpay.c14.co.il/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.1.6' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<link rel='stylesheet' id='woocommerce-layout-css'  href='//medpay.c14.co.il/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=2.5.5' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='//medpay.c14.co.il/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=2.5.5' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='//medpay.c14.co.il/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=2.5.5' type='text/css' media='all' />
<link rel='stylesheet' id='pojo-a11y-css'  href='http://medpay.c14.co.il/wp-content/plugins/pojo-accessibility/assets/css/style.min.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='pojo-css-framework-css'  href='http://medpay.c14.co.il/wp-content/themes/stream/assets/bootstrap/css/bootstrap.min.css?ver=3.2.0' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='http://medpay.c14.co.il/wp-content/themes/stream/assets/font-awesome/css/font-awesome.min.css?ver=4.4.0' type='text/css' media='all' />
<link rel='stylesheet' id='pojo-base-style-css'  href='http://medpay.c14.co.il/wp-content/themes/stream/core/assets/css/style.min.css?ver=1.6.0' type='text/css' media='all' />
<link rel='stylesheet' id='pojo-style-css'  href='http://medpay.c14.co.il/wp-content/themes/stream/assets/css/style.min.css?ver=1.6.0' type='text/css' media='all' />
<link rel='stylesheet' id='pojo-base-style-rtl-css'  href='http://medpay.c14.co.il/wp-content/themes/stream/core/assets/css/rtl.min.css?ver=1.6.0' type='text/css' media='all' />
<link rel='stylesheet' id='pojo-style-rtl-css'  href='http://medpay.c14.co.il/wp-content/themes/stream/assets/css/rtl.min.css?ver=1.6.0' type='text/css' media='all' />
<link rel='stylesheet' id='photoswipe-css'  href='http://medpay.c14.co.il/wp-content/plugins/pojo-lightbox/assets/photoswipe/photoswipe.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='photoswipe-skin-css'  href='http://medpay.c14.co.il/wp-content/plugins/pojo-lightbox/assets/photoswipe/default-skin/default-skin.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='pojo-builder-animation-css'  href='http://medpay.c14.co.il/wp-content/plugins/pojo-builder-animation/assets/css/styles.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='sf-style-css'  href='http://medpay.c14.co.il/wp-content/plugins/filter-custom-fields-taxonomies-light/res/style.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='wppb_stylesheet-css'  href='http://medpay.c14.co.il/wp-content/plugins/profile-builder/assets/css/style-front-end.css?ver=4.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='wppb_stylesheet_rtl-css'  href='http://medpay.c14.co.il/wp-content/plugins/profile-builder/assets/css/rtl.css?ver=4.4.3' type='text/css' media='all' />
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>
<script>window.jQuery || document.write('<script src="http://medpay.c14.co.il/wp-content/themes/stream/core/assets/js/jquery-1.11.1.min.js"><\/script>')</script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/pb-add-on-placeholder-labels/assets/js/init.js?ver=4.4.3'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/popup-builder/javascript/sg_popup_core.js?ver=1'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.1.6'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.1.6'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/filter-custom-fields-taxonomies-light/res/sf.js?ver=4.4.3'></script>
<link rel='https://api.w.org/' href='http://medpay.c14.co.il/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://medpay.c14.co.il/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://medpay.c14.co.il/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.4.3" />
<meta name="generator" content="WooCommerce 2.5.5" />
<link rel="canonical" href="http://medpay.c14.co.il/%d7%9e%d7%99%d7%93%d7%a2/" />
<link rel='shortlink' href='http://medpay.c14.co.il/?p=315' />
<link rel="alternate" type="application/json+oembed" href="http://medpay.c14.co.il/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fmedpay.c14.co.il%2F%25d7%259e%25d7%2599%25d7%2593%25d7%25a2%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://medpay.c14.co.il/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fmedpay.c14.co.il%2F%25d7%259e%25d7%2599%25d7%2593%25d7%25a2%2F&#038;format=xml" />

	<link rel='https://github.com/WP-API/WP-API' href='http://medpay.c14.co.il/wp-json' />
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic|Noto+Sans:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic"><style type="text/css">body{background-color: rgba(255, 255, 255, 1);background-position: top center;background-repeat: repeat;background-size: auto;background-attachment: scroll;color: #878787; font-family: 'Noto Sans', Arial, sans-serif; font-weight: normal; font-size: 15px;line-height: 1.7em;}div.logo-text a{color: #e14938; font-family: 'Raleway', Arial, sans-serif; font-weight: bold; font-size: 31px;line-height: 1em;}.logo{margin-top: 13px;margin-bottom: 0px;}#header, .sticky-header{background-color: rgba(255, 255, 255, 1);background-position: top center;background-repeat: repeat-x;background-size: auto;background-attachment: scroll;}.sf-menu a, .mobile-menu a,.sf-menu a:hover,.sf-menu li.active a, .sf-menu li.current-menu-item > a,.sf-menu .sfHover > a,.sf-menu .sfHover > li.current-menu-item > a,.sf-menu li.current-menu-ancestor > a,.mobile-menu a:hover,.mobile-menu li.current-menu-item > a{color: #a3a3a3; font-family: 'Raleway', Arial, sans-serif; font-weight: bold; font-size: 13px;}.nav-main .sf-menu .sub-menu{background-color: #424242;}.nav-main .sf-menu .sub-menu li:hover > a,.nav-main .sf-menu .sub-menu li.current-menu-item > a{background-color: #565656;color: #e14938;}.nav-main .sf-menu .sub-menu li a{color: #ffffff; font-family: 'Raleway', Arial, sans-serif; font-weight: normal; font-size: 13px;line-height: 3em;}.search-section{background-color: #424242;}.search-section,.search-section .form-search .field{color: #e14938;}#page-header{height: 110px;line-height: 110px;color: #ffffff; font-family: 'Raleway', Arial, sans-serif; font-weight: bold; font-size: 25px;}#page-header.page-header-style-custom_bg{background-color: rgba(225, 73, 56, 1);background-position: center center;background-repeat: repeat;background-size: cover;background-attachment: scroll;}#page-header div.breadcrumbs, #page-header div.breadcrumbs a{color: #CCB768; font-family: 'Raleway', Arial, sans-serif; font-weight: normal; font-size: 13px;}a, .entry-meta{color: #e14938;}a:hover, a:focus{color: #878787;}::selection{color: #ffffff;background: #e14938;}::-moz-selection{color: #ffffff;background: #e14938;}h1{color: #e14938; font-family: 'Raleway', Arial, sans-serif; font-weight: bold; font-size: 37px;text-transform: uppercase;line-height: 1.3em;}h2{color: #4c4c4c; font-family: 'Raleway', Arial, sans-serif; font-weight: bold; font-size: 37px;line-height: 1.3em;}h3{color: #e14938; font-family: 'Raleway', Arial, sans-serif; font-weight: bold; font-size: 26px;text-transform: uppercase;line-height: 1.5em;}h4{color: #5b5b5b; font-family: 'Raleway', Arial, sans-serif; font-weight: bold; font-size: 18px;text-transform: uppercase;line-height: 1.3em;}h5{color: #e14938; font-family: 'Raleway', Arial, sans-serif; font-weight: bold; font-size: 19px;text-transform: uppercase;line-height: 1.7em;}h6{color: #6d6d6d; font-family: 'Raleway', Arial, sans-serif; font-weight: normal; font-size: 15px;line-height: 1.7em;}#sidebar{color: #878787; font-family: 'Noto Sans', Arial, sans-serif; font-weight: normal; font-size: 15px;line-height: 1.7em;}#sidebar a{color: #e14938;}#sidebar a:hover{color: #878787;}#sidebar .widget-title{color: #e14938; font-family: 'Raleway', Arial, sans-serif; font-weight: bold; font-size: 19px;text-transform: uppercase;line-height: 1.7em;}#footer{background-color: #e14938;color: #ffffff; font-family: 'Noto Sans', Arial, sans-serif; font-weight: normal; font-size: 13px;line-height: 1.5em;}#footer a{color: #ffffff;}#footer a:hover{color: #ffffff;}#sidebar-footer .widget-title{color: #e14938; font-family: 'Raleway', Arial, sans-serif; font-weight: bold; font-size: 19px;text-transform: uppercase;line-height: 2em;}#copyright{background-color: #e14938;color: #ffffff; font-family: 'Noto Sans', Arial, sans-serif; font-weight: normal; font-size: 12px;line-height: 60px;}#copyright a{color: #ffffff;}#copyright a:hover{color: #b8312f;}#pojo-scroll-up{width: 50px;height: 50px;line-height: 50px;background-color: rgba(51, 51, 51, 0.6);background-position: top center;background-repeat: repeat;background-size: auto;background-attachment: scroll;}#pojo-scroll-up a{color: #eeeeee;}#pojo-a11y-toolbar .pojo-a11y-toolbar-overlay{background-color: #ffffff;}#pojo-a11y-toolbar .pojo-a11y-toolbar-overlay ul.pojo-a11y-toolbar-items li.pojo-a11y-toolbar-item a, #pojo-a11y-toolbar .pojo-a11y-toolbar-overlay p.pojo-a11y-toolbar-title{color: #333333;}#pojo-a11y-toolbar .pojo-a11y-toolbar-toggle a{color: #ffffff;background-color: #4054b2;}#pojo-a11y-toolbar .pojo-a11y-toolbar-overlay ul.pojo-a11y-toolbar-items li.pojo-a11y-toolbar-item a.active{background-color: #4054b2;color: #ffffff;}.sf-menu a, .menu-no-found,.sf-menu li.pojo-menu-search,.search-header{line-height: 60px;}.sf-menu li:hover ul, .sf-menu li.sfHover ul{top: 60px;}a.search-toggle{color: #a3a3a3;}.navbar-toggle{border-color: #a3a3a3;}.icon-bar{background-color: #a3a3a3;}.nav-main .sf-menu > li > a:hover span, sf-menu > li.active a span, .sf-menu > li.current-menu-item > a span,.sf-menu li.current-menu-ancestor > a span,.sf-menu .sfHover > a span,.sf-menu .sfHover > li.current-menu-item > a span{border-color: #e14938;}.sf-menu .sub-menu li > a{border-color: #565656;}#pojo-a11y-toolbar .pojo-a11y-toolbar-overlay, #pojo-a11y-toolbar .pojo-a11y-toolbar-overlay ul.pojo-a11y-toolbar-items.pojo-a11y-links{border-color: #4054b2;}body.pojo-a11y-focusable a:focus{outline-style: solid !important;outline-width: 1px !important;outline-color: #FF0000 !important;}#pojo-a11y-toolbar{top: 100px !important;}			article.sticky:before {background-color: #e14938;}
			.image-link:hover {background-color: #e14938;}
			.nav-main .pojo-menu-cart li.cart-checkout a {color: #e14938 !important;border-color: #e14938 !important;}
			.nav-main .pojo-menu-cart li.cart-checkout a:hover {color: #f2b7b2 !important;border-color: #f2b7b2 !important;}
			.align-pagination .pagination > li > a:hover,.align-pagination .pagination > li > span:hover,.align-pagination .pagination > .active > a,.align-pagination .pagination > .active > span,.align-pagination .pagination > .active > a:hover,.align-pagination .pagination > .active > span:hover,.align-pagination .pagination > .active > a:focus,.align-pagination .pagination > .active > span:focus {border-color: #e14938; color: #e14938;}
			.pojo-loadmore-wrap .button:hover,.pojo-loadmore-wrap .pojo-loading,.pojo-loading-wrap .button:hover,.pojo-loading-wrap .pojo-loading:hover {border-color: #e14938; color: #e14938;}
			.gallery-item.grid-item .inbox .caption small {color: #e14938;}
			.woocommerce ul.products li.product .caption .price, .woocommerce-page ul.products li.product .caption .price {color: #e14938;}
			.category-filters li a {color: #f2b7b2;}
			.category-filters li a:hover,.category-filters li a.active {color: #e14938;}
			.widget_tag_cloud a, #sidebar-footer .widget_tag_cloud a {color: #f2b7b2;}
			.widget_tag_cloud a:hover, #sidebar-footer .widget_tag_cloud a:hover {background-color: #e14938; color: #f2b7b2;}
			ul.social-links li a .social-icon:before {background-color: #e14938;}
			ul.social-links li a .social-icon:before {color: #f2b7b2; }
			ul.social-links li a:hover .social-icon:before {background-color: #f2b7b2; }
			ul.social-links li a:hover .social-icon:before {color: #e14938; }
			.read-more,.more-link span {color: #e14938;}
			.read-more:hover,.more-link span:hover {color: #f2b7b2;}
			.navbar-toggle:hover .icon-bar, .navbar-toggle:focus .icon-bar {background-color: #e14938;}
			.catalog-item-price {color: #e14938;}
			.button,.button.size-small,.button.size-large,.button.size-xl,.button.size-xxl {border-color: #e14938;color: #e14938;}
			.button:hover,.button.size-small:hover,.button.size-large:hover,.button.size-xl:hover {border-color: #f2b7b2;color: #f2b7b2;}@media (max-height: 600px) { #pojo-a11y-toolbar { top: 50px !important } }</style>
				<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<meta name="generator" content="Powered by Slider Revolution 5.1.6 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
        <!-- link to font-awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
     <!-- Load jQuery UI CSS  -->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    
    <!-- Load jQuery JS -->
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <!-- Load jQuery UI Main JS  -->
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript"
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbhbqAsUpmtfmLdmE2WBqNn2b8ojgBwkY&libraries=geometry">
</script></head>

<body class="rtl page page-id-315 page-template-default logged-in layout-full-width pojo-title-bar format-page-builder">
   
    <link rel="stylesheet" type="text/css" href="http://medpay.c14.co.il/wp-content/themes/stream/custom.css"> 
    <link rel="stylesheet" type="text/css" href="http://medpay.c14.co.il/wp-content/themes/stream/custom-extra.css"> 
     <script src="http://medpay.c14.co.il/wp-content/themes/stream/assets/js/autocomplete.js"></script>
    <script src="http://medpay.c14.co.il/wp-content/themes/stream/assets/js/custom.js"></script>
    <script src="http://medpay.c14.co.il/wp-content/themes/stream/assets/js/custom2.js"></script>  
     <script src="http://medpay.c14.co.il/wp-content/themes/stream/assets/js/custom3.js"></script>
     <script src="http://medpay.c14.co.il/wp-content/themes/stream/assets/js/custom4.js"></script> 
    


        
  <input id='uid' type='hidden' name='uid' value='4'>  
      
<!--[if lt IE 7]><p class="chromeframe">Your browser is <em>ancient!</em>
	<a href="http://browsehappy.com/">Upgrade to a different browser</a> or
	<a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.
</p><![endif]-->

<div id="container" class="wide">
	
	
	<div class="container-wrapper">

	<header id="header" class="logo-right" role="banner">
			<div class="container">
				<div class="logo">
                    <div class="home-img">
							<a href="http://medpay.c14.co.il/dashboard/" rel="home"><img src="http://medpay.c14.co.il/wp-content/themes/stream/assets/images/home.png" alt="MedPay" class="home-img-primary" /></a>
						</div>
                    <div class="back_btn"> <a href="javascript: history.go(-1)" style="font-size: 140%; color: white;">
                        <!-- &#65515; --><i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
					                    <div class='logo_php'>מידע כללי</div>						<div class="logo-img">
							<a href="/" rel="home"><img src="http://medpay.c14.co.il/wp-content/uploads/2016/02/Screen-Shot-2016-02-12-at-16.45.22.png" alt="MedPay" class="logo-img-primary" /></a>
						</div>
															<button type="button" class="navbar-toggle visible-xs home-btn-left" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">תפריט</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
									</div><!--.logo -->
				<nav class="nav-main" role="navigation">
					<div class="navbar-collapse collapse">
						<div class="nav-main-inner">
															<ul id="menu-%d7%a8%d7%90%d7%a9%d7%99" class="sf-menu hidden-xs"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-%d7%a8%d7%90%d7%a9%d7%99 first-item menu-item-327"><a href="http://medpay.c14.co.il/dashboard/"><span>ראשי</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-%d7%94%d7%92%d7%a9%d7%aa-%d7%aa%d7%91%d7%99%d7%a2%d7%94 menu-item-325"><a href="http://medpay.c14.co.il/%d7%94%d7%92%d7%a9%d7%aa-%d7%aa%d7%91%d7%99%d7%a2%d7%94/"><span>הגשת תביעה</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-%d7%91%d7%99%d7%a8%d7%95%d7%a8-%d7%9e%d7%a6%d7%91-%d7%a4%d7%a0%d7%99%d7%94 menu-item-324"><a href="http://medpay.c14.co.il/%d7%9e%d7%a6%d7%91-%d7%a4%d7%a0%d7%99%d7%94/"><span>בירור מצב פניה</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-%d7%97%d7%99%d7%a4%d7%95%d7%a9-%d7%a8%d7%95%d7%a4%d7%90 menu-item-370"><a href="http://medpay.c14.co.il/%d7%97%d7%99%d7%a4%d7%95%d7%a9-%d7%a8%d7%95%d7%a4%d7%90/"><span>חיפוש רופא</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-%d7%94%d7%aa%d7%99%d7%a2%d7%a6%d7%95%d7%aa-%d7%a2%d7%9d-%d7%a8%d7%95%d7%a4%d7%90 menu-item-371"><a href="http://medpay.c14.co.il/%d7%94%d7%aa%d7%99%d7%a2%d7%a6%d7%95%d7%aa-%d7%a2%d7%9d-%d7%a8%d7%95%d7%a4%d7%90/"><span>התיעצות עם רופא</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-315 current_page_item active menu-%d7%9e%d7%99%d7%93%d7%a2-%d7%9b%d7%9c%d7%9c%d7%99 menu-item-323"><a href="http://medpay.c14.co.il/%d7%9e%d7%99%d7%93%d7%a2/"><span>מידע כללי</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-%d7%a6%d7%a8%d7%95-%d7%a7%d7%a9%d7%a8-%d7%98%d7%9c%d7%a4%d7%95%d7%a0%d7%99 menu-item-415"><a href="http://medpay.c14.co.il/%d7%a6%d7%a8%d7%95-%d7%a7%d7%a9%d7%a8/"><span>צרו קשר טלפוני</span></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-%d7%94%d7%aa%d7%a0%d7%aa%d7%a7 menu-item-416"><a href="/wp-login.php?action=logout&amp;_wpnonce=0ef8b1ecd4"><span>התנתק</span></a></li>
</ul><ul id="menu-%d7%a8%d7%90%d7%a9%d7%99-1" class="mobile-menu visible-xs"><li class="menu-item menu-item-type-post_type menu-item-object-page first-item menu-item-327"><a href="http://medpay.c14.co.il/dashboard/"><span>ראשי</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-325"><a href="http://medpay.c14.co.il/%d7%94%d7%92%d7%a9%d7%aa-%d7%aa%d7%91%d7%99%d7%a2%d7%94/"><span>הגשת תביעה</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-324"><a href="http://medpay.c14.co.il/%d7%9e%d7%a6%d7%91-%d7%a4%d7%a0%d7%99%d7%94/"><span>בירור מצב פניה</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-370"><a href="http://medpay.c14.co.il/%d7%97%d7%99%d7%a4%d7%95%d7%a9-%d7%a8%d7%95%d7%a4%d7%90/"><span>חיפוש רופא</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-371"><a href="http://medpay.c14.co.il/%d7%94%d7%aa%d7%99%d7%a2%d7%a6%d7%95%d7%aa-%d7%a2%d7%9d-%d7%a8%d7%95%d7%a4%d7%90/"><span>התיעצות עם רופא</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-315 current_page_item active menu-item-323"><a href="http://medpay.c14.co.il/%d7%9e%d7%99%d7%93%d7%a2/"><span>מידע כללי</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-415"><a href="http://medpay.c14.co.il/%d7%a6%d7%a8%d7%95-%d7%a7%d7%a9%d7%a8/"><span>צרו קשר טלפוני</span></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-416"><a href="/wp-login.php?action=logout&amp;_wpnonce=0ef8b1ecd4"><span>התנתק</span></a></li>
</ul>																	<div class="search-header hidden-xs">
										<a href="javascript:void(0);" class="search-toggle" data-target="#search-section-primary">
											<i class="fa fa-search"></i>
										</a>
									</div>
																					</div>
					</div>
				</nav><!--/#nav-menu -->
			</div><!-- /.container -->
		</header>

					<div class="hidden-xs">
				<div id="search-section-primary" class="search-section" style="display: none;">
					<div class="container">
						<form role="search" method="get" class="form form-search" action="http://medpay.c14.co.il/">
	<label for="s">
		<span class="sr-only">חיפוש עבור:</span>
		<input type="search" title="חיפוש" name="s" value="" placeholder="חיפוש..." class="field search-field">
	</label>
	<button value="חיפוש" class="search-submit button" type="submit">חיפוש</button>
</form><i class="fa fa-search"></i>
					</div>
				</div>
			</div>
		
		<div class="sticky-header-running"></div>

			
		<div id="page-header" class="page-header-style-custom_bg">
	<div class="page-header-title container">
					<div class="title-primary">
				מידע כללי			</div>
					</div><!-- /.page-header-title -->
</div><!-- /#page-header -->		        
		<div id="primary">
          
		<div class="container">
			<div id="content" class="row">		<section id="main" class="col-sm-12 col-md-12 full-width" role="main">
			<article id="post-315" class="post-315 page type-page status-publish hentry">
						<div class="entry-page">
								<div class="entry-content">
								<section id="builder-section-1" class="section" data-anchor-target="#builder-section-1">

									
					<div class="container">
						<div class="columns advanced-columns">
															<div class="column-12 advanced-column">
									<div class="columns widget-columns">
																					<div class="column-12 widget-column">
												<div class="pb-widget-inner">
															<div class="textwidget"><h2>מהי MEDPAY ?</h2><p>MEDPAYהינה אפליקציה ייחודית המיועדת למחזיקי פוליסת נסיעות לחו"ל של חברת מגדל.</p><p>MEDPAY הנה פרי ניסיון ארוך שנים במתן שירותי סיוע רפואי בחו"ל של חברת IMA אותו היא</p><p>מספקת למטיילים ישראלים מעל לעשור.</p><h2>באמצעות MEDPAY ניתן :</h2><p>1. לאתר רופא שנמצא בקרבת מקום.</p><p>2. להתייעץ עם רופא ישראלי.</p><p>3. לקבל החזר בגין התשלום לרופא ישירות לכרטיס האשראי שלך תוך זמן קצר.</p><p>MEDPAY אינה נותנת שירות במקרים של אובדן מטען.</p><h2>מה עושים במקרה של בעיה רפואית בחו"ל ?<br />לא מרגישים טוב? חום? נפלת? כאב שיניים שלא עובר?</h2><p>1. המלצתנו היא ללכת להיבדק אצל רופא. אשר יכול לבדוק את החולה בדיקה גופנית</p><p>ולהתרשם ממצבו באופן הטוב ביותר, ניתן לאתר רופא באמצעות האפליקציה או לגשת</p><p>לרופא באופן עצמאי ואף להזמין רופא לחדר המלון שלכם. לחץ כאן על מנת לעבור לדף</p><p>2. להתייעץ עם רופא ישראלי באמצעות האפליקציה. לחץ כאן לעבור לעמוד התייעצות עם</p><p>3. במקרי חירום ניתן לגשת לבית חולים או חדר מיון וליצור קשר עם מוקד IMA. לחץ כאן</p><p>איתור רופא.</p><p>רופא.</p><p>לעמוד יצירת קשר</p><h2>איך מתבצע ייעוץ רפואי ?</h2><p>הייתם אצל רופא ועדיין ישנן שאלות? מעוניין לקבל חוות דעת לגביי מרשם לתרופה?</p><p>באפשרותך להתייעץ עם רופא דרך האפליקציה.</p><p>איך זה עובד? בעמוד "התייעצות עם רופא" (ניתן ללחוץ למעבר לעמוד) יש למלא את פרטי הבעיה</p><p>המלצתנו היא להתייעץ טלפונית עם רופא רק לאחר שנבדקת אצל רופא. כל מקרה רפואי דורש</p><p>הרפואית בצורה המפורטת ביותר ואנחנו ניצור איתך קשר בתוך שעה.</p><p>בדיקה גופנית ולכן אבחון טלפוני הוא תמיד אבחון לא מלא.</p><h2>מיהם הרופאים המופיעים במאגר MEDPAY ?</h2><p>הרופאים הינם רופאים הנמצאים באזורים בהם ישנו ריבוי תיירים, אשר חלקם אף טיפלו בחולים</p><p>חשוב מאוד ליצור קשר עם הרופא לפני הגעתכם אליו. מצאתם טעות בפרטים המוצגים? לחצו על</p><p>קבעתם תור? באפשרותכם לנווט אל הרופא דרך האפליקציה על ידי לחיצה על "נווט" (יש לוודא כי</p><p>ישראלים וקיבלו מהם חוות דעת.</p><p>כפתור "דווח על טעות" וספרו לנו היכן הטעות.</p><p>הכתובת היא כתבות עדכנית ומדוייקת).</p><h2>האם עלי לגשת רק לרופאים מתוך רשימה ?</h2><p>באפשרות לגשת לכל רופא בעל רישיון בארץ בה הינך נמצא (כולל רופא שיניים). הפוליסה מכסה גם</p><p>ביקור בית של רופא. חשוב כמובן ליצור קשר טלפוני עם הרופא על מנת לוודא שהוא יכול לקבל</p><p>אתכם.</p><h2>הגעתי לרופא. מה עליי לעשות?</h2><p>בהגיעך לרופא, יש להסביר את הבעיה אשר בגללה פנית לקבל טיפול רפואי.</p><p>לאחר סיום הביקור, כאשר קיבלת מענה לסיבה בגללה הגעתם, יש לשלם לרופא את הסכום</p><p>יש לשמור את המסמכים, תצטרכו לצלם אותם בהמשך על מנת לקבל החזר מהיר דרך MEDPAY.</p><p>המבוקש ולבקש ממנו סיכום ביקור וקבלה עבור התשלום.</p><p>לא משנה איך שילמתם לרופא (מזומן או בכרטיס אשראי), ההחזר יבוצע לכרטיס האשראי הישראלי</p><p>לדוגמה: הבעל חולה והאישה שילמה, ההחזר יכול להתבצע אך ורק לכרטיס האשראי של הבעל.</p><p>קטין מתחת לגיל 18 שאין לו כרטיס אשראי על שמו: ההחזר יבוצע לכרטיס האשראי של אחד</p><h2>כיצד מקבלים החזר?</h2><p>של המבוטח בלבד.</p><p>מההורים, אם יש לקטין כרטיס אשראי על שמו אז ההחזר יכול להיות מועבר אליו.</p><p>לחץ כאן לעמוד הגשת תביעה.</p><h2>האם כרטיס האשראי שלי צריך להיות בינלאומי?</h2><p>לא. ההחזר יבוצע לכל כרטיס אשראי ישראלי (רגיל או בינלאומי) ובלבד שהינו על שם החולה (או</p><p>הורה החולה במקרה של קטין).</p><h2>איך אדע אם המקרה שלי מכוסה על ידי הביטוח?</h2><p>כיסוי ביטוח ניתן לדעת בוודאות רק לאחר הגשת המסמכים הרפואיים לMEDPAY או לחברת</p><p>באופן כללי ניתן לומר כי הפוליסה מכסה מקרי חירום פתאומיים ובלתי צפויים (שיניים או מקרה</p><p>הפוליסה אינה מכסה טיפולים רפואיים מתוכננים או כאלה שהחלו בישראל – מומלץ לקרוא את</p><p>הביטוח.</p><p>רפואי).</p><p>הפוליסה שנשלחה אליך מחברת מגדל.</p><h2>האם ישנה השתתפות עצמית?</h2><p>כן. במקרה של טיפול רפואי ישנה השתתפות עצמית של 30$ אשר ינוכו מהסכום ששילמת לרופא.</p><h2>אילו מסמכים עלי להגיש על מנת לקבל החזר ?</h2><p>במהלך הגשת התביעה דרך אפליקציית MEDPAY תתבקש לצלם את המסמכים הבאים:</p><p> דו"ח רפואי</p><p> קבלה על כל דרישת תשלום (רופא, בדיקות דם, תרופות)</p><p>בנוסף, ייתכן ויידרשו צילום רישיון נהיגה / דו"ח משטרה וכן מסמכים אחרים בהתאם לאירוע.</p><p>חשוב מאד:</p><p> לרשום בראש כל מסמך את תעודת הזהות של מגיש התביעה.</p><p> לוודא שהמסמכים צולמו באיכות המרבית. הגשת מסמך לא ברור עלולה לעכב את קבלת</p><p>ההחזר.</p><h2>מהו הסכום המקסימאלי שאוכל לקבל בחזרה באמצעותכם ?</h2><p>אפליקציית MEDPAY מאפשרת קבלת החזר של עד 10,000 ₪, בתנאי שההחזר תואם לסכום</p><p>המקסימאלי הנקבע בפוליסה.</p><p>במידה והסכום ששילמת לגורם הרפואי עולה על 10,000 ₪ , באפשרותך להגיש בקשה להחזר עם</p><p>שובך לארץ דרך מחלקת התביעות.</p><p>איזה החזרים אוכל לקבל באפליקציה</p><p>ניתן לקבל החזרים בגין ביקור רופא, ביקור במרפאה, בדיקות רפואיות ותרופות.</p><p>לא ניתן לקבל החזרים על הוצאות אחרות ואותן יש לדרוש (אם קיימות) ישירות מחברת מגדל.</p><h2>איפה אפשר למצוא מספרי חירום במדינה בה אני נמצא?<br />זקוק לאמבולנס? משטרה? מכבי אש?</h2><p>בקישור הבא ניתן למצוא את מספרי הטלפון הרלוונטיים.</p><h2>מה עושים במקרה של איבוד מזוודה או מטען?</h2><p>יש לדאוג לקבלת אישורים מהמשטרה (במקרה של גנבה) או חברת התעופה (במקרה של אובדן). יש</p><p>ליצור קשר עם מחלקת התביעות בטלפון XXXX בשעות הפעילות הרגילות</p></div>
														</div>
											</div>
																			</div>
								</div>
													</div>
					</div>
							</section>
						</div>
				<footer class="entry-footer">
					<div class="entry-edit">
											</div>
				</footer>
			</div>
		</article>
					</section><!-- section#main -->
				</div><!-- #content -->
		</div><!-- .container -->
	</div><!-- #primary -->

				<footer id="footer">
					</footer>
		<section id="copyright" role="contentinfo">
			<div class="container">
				<div class="pull-left-copyright">
					<a href="http://www.kcsnet.net">KCS</a>				</div>
				<div class="pull-right-copyright">
					כל הזכויות שמורות ima				</div>
			</div><!-- .container -->
		</section>

	
	</div><!-- .container-wrapper -->
</div><!-- #container -->
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>


<script type="text/javascript">
	jQuery(window).load(function(){	
		jQuery('#status').fadeOut('fast'); // will first fade out the loading animation
		jQuery('#preloader').delay(3000).slideUp('fast'); // will fade out the white DIV that covers the website.
		jQuery('.home').delay(3000).css({'overflow':'visible'});
	})
</script>

<script type="text/javascript">var PojoSliders=[];</script>		<div id="pojo-scroll-up" class="pojo-scroll-up-right" data-offset="50" data-duration="750" style="font-size: 20px;border-radius: 6px">
			<div class="pojo-scroll-up-inner">
				<a class="pojo-scroll-up-button" href="javascript:void(0);" title="גלילה לראש העמוד">
					<span class="fa fa-chevron-up"></span><span class="sr-only">גלילה לראש העמוד</span>
				</a>
			</div>
		</div>
				<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="pswp__bg"></div>
			<div class="pswp__scroll-wrap">
				<div class="pswp__container">
					<div class="pswp__item"></div>
					<div class="pswp__item"></div>
					<div class="pswp__item"></div>
				</div>
				<div class="pswp__ui pswp__ui--hidden">
					<div class="pswp__top-bar">
						<div class="pswp__counter"></div>
						<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
						<button class="pswp__button pswp__button--share" title="Share"></button>
						<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
						<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
						<div class="pswp__preloader">
							<div class="pswp__preloader__icn">
								<div class="pswp__preloader__cut">
									<div class="pswp__preloader__donut"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
						<div class="pswp__share-tooltip"></div>
					</div>

					<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
					</button>
					<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
					</button>
					<div class="pswp__caption">
						<div class="pswp__caption__center"></div>
					</div>
				</div>
			</div>
		</div>
		<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/medpay.c14.co.il\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","recaptchaEmpty":"Please verify that you are not a robot.","sending":"\u05e9\u05d5\u05dc\u05d7...","cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/i18n/datepicker-he.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.js?ver=4.4.3'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-timepicker/i18n/jquery-ui-timepicker-he.js?ver=4.4.3'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-includes/js/jquery/ui/slider.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-includes/js/jquery/ui/button.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/contact-form-7-datepicker/js/jquery-ui-sliderAccess.js?ver=4.4.3'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/idle-user-logout/js/idle-timer.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/idle-user-logout/js/uikit.min.js?ver=1.2.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var iul = {"ajaxurl":"http:\/\/medpay.c14.co.il\/wp-admin\/admin-ajax.php","actions":{"action_type":"2","timer":"900","action_value":""},"is_mobile":""};
/* ]]> */
</script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/idle-user-logout/js/script.js?ver=2.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/%D7%9E%D7%99%D7%93%D7%A2\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View Cart","cart_url":"","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='//medpay.c14.co.il/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=2.5.5'></script>
<script type='text/javascript' src='//medpay.c14.co.il/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/%D7%9E%D7%99%D7%93%D7%A2\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='//medpay.c14.co.il/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=2.5.5'></script>
<script type='text/javascript' src='//medpay.c14.co.il/wp-content/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js?ver=1.4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/%D7%9E%D7%99%D7%93%D7%A2\/?wc-ajax=%%endpoint%%","fragment_name":"wc_fragments"};
/* ]]> */
</script>
<script type='text/javascript' src='//medpay.c14.co.il/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=2.5.5'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/pojo-forms/assets/js/app.min.js?ver=4.4.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var PojoA11yOptions = {"focusable":"","remove_link_target":"","add_role_links":"1","enable_save":"1","save_expiration":"12"};
/* ]]> */
</script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/pojo-accessibility/assets/js/app.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/themes/stream/assets/bootstrap/js/bootstrap.min.js?ver=3.2.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var Pojo = {"ajaxurl":"http:\/\/medpay.c14.co.il\/wp-admin\/admin-ajax.php","css_framework_type":"bootstrap","superfish_args":{"delay":150,"animation":{"opacity":"show","height":"show"},"speed":"fast"}};
/* ]]> */
</script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/themes/stream/assets/js/frontend.min.js?ver=1.6.0'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/themes/stream/core/assets/masterslider/masterslider.min.js?ver=2.9.5'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/pojo-lightbox/assets/photoswipe/photoswipe.min.js?ver=4.0.7'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/pojo-lightbox/assets/photoswipe/photoswipe-ui-default.min.js?ver=4.0.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var PojoLightboxOptions = {"script_type":"photoswipe","smartphone":"","woocommerce":"disable","lightbox_args":{"loop":true,"closeOnScroll":true,"closeOnVerticalDrag":true,"escKey":true,"arrowKeys":true,"history":true,"captionEl":true,"closeEl":true,"fullscreenEl":true,"zoomEl":false,"counterEl":true,"arrowEl":true,"shareEl":true}};
/* ]]> */
</script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/pojo-lightbox/assets/js/app.min.js?ver=4.4.3'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/pojo-builder-animation/assets/js/scripts.min.js?ver=4.4.3'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-content/plugins/pojo-news-ticker/assets/js/app.min.js?ver=4.4.3'></script>
<script type='text/javascript' src='http://medpay.c14.co.il/wp-includes/js/wp-embed.min.js?ver=4.4.3'></script>
		<a id="pojo-a11y-skip-content" class="pojo-skip-link pojo-skip-content" tabindex="1" accesskey="s" href="#content">דלג לתוכן</a>
		
</body>
</html>