<?php
/**
 * @package   Awesome Support/Custom Fields
 * @author    ThemeAvenue <web@themeavenue.net>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2014 ThemeAvenue
 *
 * @wordpress-plugin
 * Plugin Name:       Awesome Support: My Custom Fields
 * Plugin URI:        http://getawesomesupport.com
 * Description:       Adds custom fields to the Awesome Support ticket submission form.
 * Version:           0.1.0
 * Author:            ThemeAvenue
 * Author URI:        http://themeavenue.net
 * Text Domain:       wpas
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action( 'plugins_loaded', 'wpas_user_custom_fields' );
/**
 * Register all custom fields after the plugin is safely loaded.
 */
function wpas_user_custom_fields() {
/* You can start adding your custom fields safely after this line */

//wpas_add_custom_taxonomy( 'my_custom_services', array( 'title' => 'סוגי פניות', 'label' => 'פנייה', 'label_plural' => 'סוגי פניות' ) );
                           //id field
    
                           //TAB 1 
    
    wpas_add_custom_field( 'התביעה_על_שם', array( 'title' => 'על שם מי התביעה', 'options' => array('option1' => 'התביעה על שמי', 'option2' => 'התביעה על שם ילדי הקטינים'), 'field_type' => 'select'));    
                                    // TAB 1 option2
    
    wpas_add_custom_field( 'שם_הורה',  array( 'title' => __( 'שם ההורה', 'wpas' ), 'placeholder' => 'שם פרטי ושם משפחה'));
    wpas_add_custom_field( 'שם_מלא',  array( 'title' => __( 'שם החולה', 'wpas' ), 'placeholder' => 'שם פרטי ושם משפחה', 'required'));
    wpas_add_custom_field( 'ת_ז',  array( 'title' => __( 'תעודת זהות (של החולה)', 'wpas' ), 'placeholder' => '(9 ספרות)'));
    
  //  wpas_add_custom_field( 'תאריך_לידה',  array( 'title' => __( 'תאריך לידה', 'wpas' ), 'field_type'=>'number' ));
  //  wpas_add_custom_field( 'מין',  array( 'title' => 'מין', 'options'radio => array( 'option1' => 'נקבה','option2'=>'זכר'), 'field_type'=>'radio'));
     wpas_add_custom_field( 'היכן_נרכשה_הפוליסה',  array( 'title' => 'היכן נרכשה הפוליסה?', 'options' => array('option1' => 'ישירות ממגדל','option2'=>'באמצעות סוכן','option3'=>'אחר'), 'field_type'=>'select'));
      wpas_add_custom_field( 'שם_הסוכן_סוכנות',  array( 'title' => __( 'שם הסוכן/סוכנות', 'wpas' ), 'placeholder'=>'שם הסוכן/סוכנות', 'field_type'=>'text'));    
     wpas_add_custom_field( 'תאריך_יציאה_מהארץ', array('title' => __( 'תאריך יציאה מהארץ', 'wpas' ),'field_type'=>'text', 'placeholder'=>'תאריך יציאה מהארץ' )); 
                         

                                //TAB 2 
    
 wpas_add_custom_field( 'סוג_אירוע', array( 'title' => 'סוג האירוע','options' => array( 'option1' => 'מחלה','option2'=>'תאונה או נפילה','option3'=>'תאונת סקי', 'option4' => 'תאונת דרכים', 'option5' => 'רפואת שיניים', 'option6' => 'הריון ולידה'), 'field_type'=>'select'));  
    
                    //OPTION 1
  wpas_add_custom_field( 'תאריך_אירוע',  array( 'title' => __( 'תאריך האירוע', 'wpas' ),'placeholder'=>'תאריך האירוע', 'field_type'=>'text'));     
  wpas_add_custom_field( 'מחלה', array('title' => 'תאור המחלה','textarea' => __( 'מחלה', 'wpas' ), 'placeholder' => 'אנא רשמו לנו כאן בצורה מפורטת מהי הבעיה הרפואית ומתי התחילה...', 'field_type'=>'textarea','rows'=>'5')); 
  wpas_add_custom_field( 'מתי_התחלת_לסבול',  array( 'title' => 'מתי התחלת לסבול מהמחלה?', 'options' => array('option1'=>'הבעיה החלה בישראל, לפני יציאה לחו"ל', 'option2'=>'הבעיה החלה אחרי שטסתי לחו"ל'), 'field_type'=>'select'));
    
                 //OPTION 2
 wpas_add_custom_field( 'תאור_התאונה',  array( 'title' => 'תאור התאונה', 'placeholder'=>'אנא רשמו כאן בצורה מפורטת ככל האפשר איך התרחשה התאונה, היכן ומתי ?','field_type'=>'textarea'));  
    
 wpas_add_custom_field( 'תאור_הפגיעה',  array( 'title' => __( 'תאור הפגיעה', 'wpas' ), 'field_type'=>'textarea','placeholder'=>' אנא תארו כאן את הפגיעה בכם מבחינה רפואית...' ));
    
 
    wpas_add_custom_field( 'כלי_הרכב',  array( 'title' => 'כלי הרכב', 'options' => array('option1'=>'סוג כלי רכב? - מכונית', 'option2'=>'מונית','option3'=>'טרקטורון','option4'=>'אחר','option5'=>'כלי שייט'), 'field_type'=>'select'));

    wpas_add_custom_field( 'מי_נהג_ברכב',  array( 'title' => 'מי נהג ברכב', 'options' => array('option1' =>'מי נהג ברכב? - אני', 'option2'=>'קרוב משפחה','option3'=>'חבר/ה','option4'=>'נהג מונית','option5'=>'נהג לא מוכר לי'), 'field_type'=>'select'));
    
   
    wpas_add_custom_field( 'אני_הייתי',  array( 'title' => __( 'אני הייתי', 'wpas' ), 'field_type'=>'select', 'options' => array ('option1' => ' נהג באופנוע','option2'=>'נהג ברכב','option3'=>'הנוסע: אני מכיר את הנהג','option4'=>'הנוסע: אני לא מכיר את הנהג','option5'=>'הולך רגל שנפגע'))); 
    
    wpas_add_custom_field( 'משטרה',  array( 'title' => 'משטרה', 'options' => array( 'option1' => 'יש ברשותי דו"ח משטרה','option2'=>'אין ברשותי דו"ח משטרה','option3'=>'לא היתה מעורבת משטרה'), 'field_type'=>'select'));
    
    wpas_add_custom_field( 'סיבת_הפניה_לטיפול',  array( 'title' => __( 'סיבת הפניה לטיפול', 'wpas' ) ) );  
    
    wpas_add_custom_field( ' איזה_טיפול_נעשה ',  array( 'title' =>  '?איזה טיפול נעשה', 'options'=> array('option1'=> 'טיפול בכאב', 'option2'=>'סתימה', 'option3'=>'עקירה','option4'=>'טיפול חירום אחר'),'field_type'=>'select' ));

    wpas_add_custom_field( 'היכן_החלה_הבעיה',  array( 'title' => __( 'היכן החלה הבעיה', 'wpas' ), 'field_type'=>'text')); 
        
    wpas_add_custom_field( 'אנא_רשמי_באיזה_שבוע_את', array( 'title' =>  __( 'אנא רשמי באיזה שבוע את', 'wpas' ), 'field_type'=>'text', 'placeholder' => 'שבוע הריון'));
    
    wpas_add_custom_field( 'כמה_עוברים_בהריון_זה', array( 'title' => 'כמה עוברים בהריון זה', 'options'=>array('option1' => '1', 'options2' => '2', 'options3' => '3'), 'field_type'=>'select')); 
    
  wpas_add_custom_field( 'ההריון_הושג',  array( 'title' => 'האם ההריון הושג באופן...', 'options'=>array( 'options1' => 'באופן טבעי', 'options2' => 'באמצעות טיפולי פוריות'), 'field_type'=>'select')); 
    wpas_add_custom_field( 'אם_כן_-_פירוט_הריון_הושג',  array( 'title' => __( 'אם כן - פירוט', 'wpas' ),'field_type'=>'textarea', 'placeholder'=>'פירוט הריון הושג'));  
     
wpas_add_custom_field( 'מתי_החלו_הסימנים',  array( 'title' => __( 'מתי החלו הסימנים', 'wpas' ), 'field_type'=>'text','placeholder'=>'מתי החלו הסימנים' ) );   
wpas_add_custom_field( 'האם_מדובר_במחלה_קיימת', array( 'title' => 'אם מדובר במחלה קיימת, מהי המחלה', 'options'=>array('option1' => 'טיפול בכאבים', 'options2' => 'סתימה', 'options3' => 'טיפול שורש', 'options4' => 'ניקוז', 'options5' => 'טיפול חניכיים', 'options6' => 'כתר זמני', 'options7' => 'השתלת שן', 'options8' => 'כתר זמני', 'options9' => 'כתר קבוע', 'options10' => 'טיפול אורטודנטי'), 'field_type'=>'select'));

wpas_add_custom_field( 'אם_כן_-_פירוט_במחלה_קיימת',  array( 'title' => __( 'אם כן - פירוט', 'wpas' ), 'field_type'=>'textarea', 'placeholder'=>'פירוט מחלה קיימת'));   
  
wpas_add_custom_field( 'מהי_סיבת_הפניה_לרופא_השינים',  array( 'title' => __( 'מהי סיבת הפניה לרופא השינים', 'wpas' ), 'field_type'=>'text','placeholder'=>'סיבת הפניה לרופא שיניים'));     
wpas_add_custom_field( 'האם_מדובר_במחלה_קיימת', array( 'title' => 'האם מדובר במחלה קיימת', 'options'=>array('option1'=>'האם מדובר במחלה קיימת? - כן', 'options2' => 'לא'), 'field_type'=>'select')); 
wpas_add_custom_field( 'תאריך_קבלת_הטיפול',  array( 'title' => __( 'תאריך קבלת הטיפול', 'wpas' ), 'field_type'=>'text', 'placeholder'=>'תאריך קבלת טיפול')); 
wpas_add_custom_field( 'האם_נדרש_המשך_טיפול',array( 'title' => 'האם נדרש המשך טיפול', 'options'=>array('option1'=>'האם נדרש המשך טיפול? - כן', 'options2' => 'לא'), 'field_type'=>'select')); 
    wpas_add_custom_field( 'פירוט_האם_נדרש_המשך_טיפול', array( 'title' => __( 'אם כן - פרט', 'wpas' ), 'field_type'=>'textarea', 'placeholder'=>'תיאור המשך הטיפול הנדרש?')); 
    wpas_add_custom_field( 'תאריך_תאונה', array( 'title' => __( 'תאריך התאונה', 'wpas' ),'field_type'=>'text', 'placeholder'=>'תאריך התאונה'));
    
                                //TAB 3
    
                                   //doctor
  wpas_add_custom_field( 'תאריך_ביקור_אצל_רופא', array( 'title' => __( 'תאריך ביקור אצל הרופא', 'wpas' ),'placeholder'=>'תאריך ביקור אצל הרופא' ) ); 
  wpas_add_custom_field( 'סוג_שירות', array( 'title' => 'סוג שירות', 'options' => array( 'option1' => 'רופא','option2'=>'מרפאה/בית חולים','option3'=>'ביקור בית של רופא'),'field_type'=>'select')); 
  wpas_add_custom_field( 'שם', array( 'title' => __('שם הרופא/מרפאה', 'wpas' ),'placeholder'=>' שם הרופא/מרפאה'));    
  wpas_add_custom_field( 'התמחות', array( 'title' => __('התמחות', 'wpas' ), 'placeholder'=>'התמחות')); 
  //wpas_add_custom_field( 'טלפון_קידומת', array( 'title' => __('קידומת', 'wpas' ), 'placeholder'=>'קידומת טלפון', 'size'=>'5', "maxlength"=>"5", 'field_type'=>'number' )); 
  wpas_add_custom_field( 'טלפון', array( 'title' => __('טלפון כולל קידומת בינלאומית', 'wpas' ), 'placeholder'=>'טלפון כולל קידומת בינלאומית', 'field_type'=>'number' )); 
 // wpas_add_custom_field( 'פקס', array( 'title' => __('פקס', 'wpas' ), 'placeholder'=>'פקס', 'field_type'=>'number' ));     
  wpas_add_custom_field( 'מדינה',  array( 'title' => __( 'מדינה', 'wpas' ), 'placeholder'=>'מדינה'));     
  wpas_add_custom_field( 'עיר',  array( 'title' => __( 'עיר', 'wpas' ), 'placeholder'=>'עיר' ));    
  wpas_add_custom_field( 'אימייל',  array( 'title' => __( 'אימייל', 'wpas' ), 'placeholder'=>'אימייל' ));        
  wpas_add_custom_field( 'ביקורת',  array( 'title' => __( 'חוות דעת', 'wpas'), 'placeholder'=>'רשום חוות דעתך על הטיפול'));     
  wpas_add_custom_field( 'שפות',  array( 'title' => __( 'שפות', 'wpas' ), 'placeholder'=>'באילו שפות מדבר הרופא'));     

  //wpas_add_custom_field( 'בחירה_מהמאגר',  array( 'title' => __( 'בחירה מהמאגר', 'wpas' ), 'placeholder'=>'בחירה מהמאגר'));  
                            
                                    // mirpaa (hospital) TAB 3
    
  wpas_add_custom_field( 'שם_מרפאה', array( 'title' => __('שם מרפאה/בית חולים', 'wpas' ),'placeholder'=>'שם מרפאה/בית חולים'));     
  wpas_add_custom_field( 'התמחות_מרפאה', array( 'title' => __('התמחות מרפאה/בית חולים', 'wpas' ),'placeholder'=>'התמחות מרפאה/בית חולים'));     
  wpas_add_custom_field( 'דירוג_מרפאה', array( 'title' => __('חוות דעת מרפאה/בית חולים', 'wpas' ), 'field_type'=>'text', 'placeholder'=>'רשום חוות דעתך על הטיפול'));    
  wpas_add_custom_field( 'כתובת_מרפאה', array( 'title' => __('כתובת מרפאה/בית חולים', 'wpas' ), 'field_type'=>'text', 'placeholder'=>'כתובת מרפאה/בית חולים'));      
  wpas_add_custom_field( 'דירוג_והערות',  array( 'title' => __( 'דירוג', 'wpas' ), 'field_type'=>'text', 'placeholder'=>'דירוג', 'options'=>array('option1' => '5', 'options2' => '4', 'options3' => '3', 'options4' => '2', 'options5' => '1'), 'field_type'=>'select'));    
                                     // bikur bait (VISITING HOME)
    
   wpas_add_custom_field( 'שם_החברה', array( 'title' => __('שם החברה', 'wpas' ),'placeholder'=>'שם החברה '));  
  wpas_add_custom_field( 'דירוג_רופא',  array( 'title' => __( 'דירוג', 'wpas' ), 'field_type'=>'text', 'placeholder'=>'דירוג', 'options'=>array('option1' => '5',  'options3' => '4', 'options5' => '3','options7' => '2','options9' => '1'), 'field_type'=>'select'));       
   
                                //TAB 4
    
    
   wpas_add_custom_field( 'סוג_ההוצאה', array( 'title' => 'סוג ההוצאה', 'options'=>array('option1' => 'רופא', 'options2' => 'רופא שיניים', 'options3' => 'תרופות', 'options4' => 'בדיקות'), 'field_type'=>'select'));    
  wpas_add_custom_field( 'סכום_ששולם',  array( 'title' => __( 'סכום ששולם', 'wpas' ), 'placeholder'=>'סכום ששולם', 'field_type'=>'number' ) );    
  wpas_add_custom_field( 'מטבע',  array( 'title' => 'מטבע', 'options' => array('דולר ארה״ב','יורו','המטבע המקומי','מטבע אחר'), 'field_type'=>'select'));    
  wpas_add_custom_field( 'בחר_קובץ',  array( 'title' => __( 'לצירוף דו״ח רפואי, קבלה ומסמכים נוספים', 'wpas' ), 'placeholder'=>'לצירוף דו״ח רפואי, קבלה ומסמכים נוספים'));    
  wpas_add_custom_field( 'הארות',  array( 'title' => __( 'הערות בנוגע למסמך', 'wpas' ),'field_type'=>'textarea', 'placeholder'=>'בשדה זה יש לתאר את המסמכים המצורפים'));  
    
    for ($i = 2; $i<=10; $i++){// creating additional 9 options for tab 4 (hazaa nosefet)
        
          wpas_add_custom_field( 'סוג_ההוצאה'.$i, array( 'title' => 'סוג ההוצאה', 'options'=>array('option1' => 'רופא', 'options2' => 'רופא שיניים', 'options3' => 'תרופות', 'options4' => 'בדיקות'), 'field_type'=>'select'));    
          wpas_add_custom_field( 'סכום_ששולם'.$i,  array( 'title' => __( 'סכום ששולם', 'wpas' ), 'placeholder'=>'סכום ששולם', 'field_type'=>'number' ) );    
          wpas_add_custom_field( 'מטבע'.$i,  array( 'title' => 'מטבע', 'options' => array('דולר ארה״ב','יורו','המטבע המקומי','מטבע אחר'), 'field_type'=>'select'));    
          wpas_add_custom_field( 'בחר_קובץ'.$i,  array( 'title' => __( 'לצירוף דו״ח רפואי, קבלה ומסמכים נוספים', 'wpas' ), 'placeholder'=>' לצירוף דו״ח רפואי, קבלה ומסמכים נוספים'));    
          wpas_add_custom_field( 'הארות'.$i,  array( 'title' => __( 'הערות', 'wpas' ),'field_type'=>'textarea', 'placeholder'=>'בשדה זה יש לתאר את המסמכים המצורפים'));    

}//for
    
    wpas_add_custom_field( 'רופא_מתקבל',  array( 'title' => __( 'רופא מתקבל', 'wpas' )) );  
    
    
                                //TAB 5
    
  /*wpas_add_custom_field( 'שם_בעל_הכרטיס',  array( 'title' => __( 'שם בעל הכרטיס', 'wpas' ), 'placeholder'=>'שם בעל הכרטיס' ));    
  wpas_add_custom_field( 'מספר_כרטיס_אשראי',  array( 'title' => __( 'מספר כרטיס אשראי', 'wpas' ), 'placeholder'=>'מספר כרטיס אשראי' ) );    
  wpas_add_custom_field( ' 3_ספרות_בגב_הכרטיס_כרטיס',  array( 'title' => __( ' 3 ספרות בגב הכרטיס כרטיס', 'wpas' ), 'placeholder'=>'3 ספרות בגב הכרטיס כרטיס ') );    
  wpas_add_custom_field( 'תוקף_הכרטיס',  array( 'title' => __( 'תוקף הכרטיס', 'wpas' ), 'placeholder'=>'תוקף הכרטיס' ) );   
  wpas_add_custom_field( '2ת_ז',  array( 'title' => __( 'ת.ז.', 'wpas' ), 'placeholder'=>'ת.ז.') );    */ 
  wpas_add_custom_field( 'משוב',  array( 'title' => __( 'משוב על האפליקציה', 'wpas' ), 'field_type'=>'textarea', 'placeholder'=>'משוב על האפליקציה (אופציה)') );        
  wpas_add_custom_field( 'סכום_תביעה',  array( 'title' => __( 'סכום תביעה', 'wpas' ), 'placeholder'=>'סכום תביעה סה"כ') );       
      


                    //  hidden fields for reports
    
wpas_add_custom_field('שדה_נסתר', array('title'=> __('שדה נסתר', 'wpas'), 'field_type'=>'text'));      
wpas_add_custom_field('מספר_יחודי', array('title'=> __('מספר_יחודי', 'wpas'), 'field_type'=>'text'));     
    
    
    
    
/* Do NOT write anything after this line */
}
add_filter( 'wpas_ticket_statuses', 'wpas_my_custom_status' );
function wpas_my_custom_status( $status ) {
	$status['refused'] = 'Refused';
	$status['accepted'] = 'Accepted';
	return $status;
}

// base encode
/*function base64_url_encode($input) {
    return strtr(base64_encode($input), '+/=', '-_,');
}
$expiry (round(microtime(true))+ 60*60);*/

function pw_loading_scripts_wrong() {
    
      require_once('../wp-load.php');
            global $wpdb;
     
   // var_dump(get_current_screen()->base);
	?> 
<style>
#wpas-mb-cf{
        display:none;
    }
    #acf-cord_a,#acf-cord_b{
        display:none;
    }
</style>
<script>
jQuery( document ).ready(function() { 
    // on doctors page
    
    if(jQuery( ".post-type-doctors #acf_after_title-sortables" ).length){

        // Cords from map
        jQuery('.input-address,.search').change(function(){
            //console.log('changed');
            jQuery('#acf-field-cord_a').val(jQuery('.input-lat').val());
            jQuery('#acf-field-cord_b').val(jQuery('.input-lng').val());
        });
        
        
        
        var pniot = '';
        <?php 
            // get all pniot
     if ( 'doctors' == get_post_type() && get_current_screen()->base == 'post'){
         
            $docname = get_the_title();
            $sql = "SELECT ticket.ID, ticket.guid 
        FROM    wp_posts ticket
        INNER JOIN 
            wp_postmeta meta on meta.post_id = ticket.ID
        LEFT JOIN 
                wp_posts doc on meta.meta_value = doc.post_title
        WHERE 
             ticket.post_type = 'ticket'
             AND doc.post_type = 'doctors'
             AND meta.meta_key = '_wpas_שם'
             AND doc.post_title = '".$docname."'
             ORDER BY  ticket.ID ASC";
             $results = $wpdb->get_results($sql);
            foreach ( $results as $ticket ) {
                $ticket_id       = (int) $ticket->ID;
                $ticket_link    = $ticket->guid;
                ?>
                pniot += '<p><a href="/wp-admin/post.php?post=<?php echo $ticket_id; ?>&action=edit">פניה #<?php echo $ticket_id; ?></a><p>';
                <?php 
            }
       } ?>
                //console.log(pniot);
      
            
            var pniot_box = '<div id="wpas-mb-pniot" class="postbox "><button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">פתיחת/סגירת תפריט: התראות</span><span class="toggle-indicator" aria-hidden="true"></span></button><h2 class="hndle ui-sortable-handle"><span>פניות קשורות</span></h2><div class="inside"><div id="wpas-pniot">'+pniot+'</div></div></div>';
                  jQuery('#submitdiv').after(pniot_box);
            
        <?php 
            // get all pniot
     if ( 'doctors' == get_post_type() && get_current_screen()->base == 'post'){
         
            $docname = get_the_title();
            $sql = "SELECT ticket.ID,rating.meta_value FROM wp_posts ticket INNER JOIN wp_postmeta meta on meta.post_id = ticket.ID LEFT JOIN wp_posts doc on meta.meta_value = doc.post_title
RIGHT JOIN
	wp_postmeta rating on rating.post_id = ticket.ID
WHERE ticket.post_type = 'ticket' AND doc.post_type = 'doctors' AND meta.meta_key = '_wpas_שם' AND rating.meta_key = '_wpas_דירוג_והערות' AND doc.post_title = '".$docname."' ORDER BY ticket.ID ASC";
            $results = $wpdb->get_results($sql);
            $countRating = 0;
            $curRating = 0;
            foreach ( $results as $ticket ) {
                $doc_rating = $ticket->meta_value;
                if($doc_rating == 'options1'){
                    $curRating .= 5; }
                elseif($doc_rating == 'options2'){
                    $curRating .= 4.5; }
                elseif($doc_rating == 'options3'){
                    $curRating .= 4; }
                elseif($doc_rating == 'options4'){
                    $curRating .= 3.5; }
                elseif($doc_rating == 'options5'){
                    $curRating .= 3; }
                elseif($doc_rating == 'options6'){
                    $curRating .= 2.5; }
                elseif($doc_rating == 'options7'){
                    $curRating .= 2; }
                elseif($doc_rating == 'options8'){
                    $curRating .= 1.5; }
                elseif($doc_rating == 'options9'){
                    $curRating .= 1; }
                elseif($doc_rating == 'options10'){
                    $curRating .= 0.5; }
                elseif($doc_rating == 'options11'){
                    $curRating .= 0; }
                
                $countRating++;
            }
         if($countRating>0){
            $avgRating = $curRating/$countRating;
                }else{
            $avgRating = 0;
         }
       } ?>
            
            var avgRating = '<?php echo $avgRating;?>';
            var rating_box = '<div id="wpas-mb-rating" class="postbox "><button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">פתיחת/סגירת תפריט: התראות</span><span class="toggle-indicator" aria-hidden="true"></span></button><h2 class="hndle ui-sortable-handle"><span>דירוג ממוצע</span></h2><div class="inside"><div id="wpas-rating"><p style="text-align:center;color:green;">'+avgRating+'</p></div></div></div>';
                jQuery('#submitdiv').after(rating_box);
        jQuery('#acf-field-rating').val(avgRating);
       
       
    };
    
    
    
    // Change close button text
    if(jQuery( ".wpas-ticket-status" ).length){
      //  jQuery(".wpas-ticket-status.submitbox .submitdelete.deletion").text("העבר לתשלום");
        
        
        
        
        // add s3 link
        
        //jQuery('#wpas_בחר_קובץ').val('טוען קישור מאובטח...');
         setTimeout(function(){
        var userID = jQuery('#wpas-stakeholders select.wpas-select2.select2-hidden-accessible').val();
        var s3url ="https://console.aws.amazon.com/s3/home?region=eu-west-1#&bucket=medpay-filestack&prefix=docs/userid_"+userID;
            jQuery('#wpas_בחר_קובץ').val(s3url);
             jQuery('#wpas_בחר_קובץ_wrapper a').replaceWith('<a href="'+s3url+'" target="_blank">לחץ כאן למעבר לקובץ</a>');
             
             
             
             // hide all empty
        jQuery('.wpas-form-group').each(function(){
            val = jQuery(this).find('input').val();
            val2 = jQuery(this).find('textarea').val();
            if(val == '' || val2 == ''){
                jQuery(this).hide();
            }
        });
             jQuery('#wpas-mb-cf').show();
             
        },5000);
        
        
        
        
        // Add custom boxes
        var box = '<div id="wpas-mb-alerts" class="postbox "><h2 class="hndle ui-sortable-handle"><span>טוען...</span></h2></div>';
            jQuery('#wpas-mb-contacts').after(box);
        
       <?php if ( current_user_can( 'manage_options' ) ) { ?>

         var box2 = '<div id="wpas-mb-payment" class="postbox "><h2 class="hndle ui-sortable-handle"><span>טוען...</span></h2></div>';
            jQuery('#wpas-mb-contacts').after(box2);
        
        // Box 2
         // Print the payment box
        setTimeout(function(){
            var sum = jQuery("#wpas_סכום_ששולם").val();
            var token = jQuery("#wpas_מספר_יחודי").val();
            var box2 = '<div id="wpas-mb-payment" class="postbox "><button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">פתיחת/סגירת תפריט: התראות</span><span class="toggle-indicator" aria-hidden="true"></span></button><h2 class="hndle ui-sortable-handle"><span>זיכוי</span></h2><div class="inside"><div id="wpas-alerts"><form action="<?php echo admin_url( 'admin-post.php' ); ?>"><input type="hidden" name="action" value="wpse_79898"><?php submit_button( 'בצע זיכוי' ); ?><label for="wpas-issuer"><strong>סכום לזיכוי</strong></label><input type="number" id="sum" name="sum" value="'+sum+'"/><input type="hidden" name="token" value="'+token+'"></form></div></div></div>';
            jQuery('#wpas-mb-payment').replaceWith(box2);
            
            
        },5000); 
        
        <?php } ?>
        
        
        // Validations - Box
        setTimeout(function(){
            // Validate coin per country
            var country = jQuery("#wpas_מדינה_wrapper input").val();
            var coin = jQuery("#wpas_מטבע_wrapper select").val();
            var msg_coin = '<span style="color:red">אין התאמה בין המטבע למדינה</span>';

            if(coin == 0){
                if (country == 'ישראל'){
                    msg_coin = '<span style="color:green">תקין<span>'; }
            }else if(coin == 1){
                if (country == 'ארצות הברית'){
                    msg_coin = '<span style="color:green">תקין<span>'; }
            }else if(coin == 2){         
                    msg_coin = '<span style="color:green">תקין<span>'; }

            // Validate sickness sign
            var sign = jQuery("#wpas_האם_מדובר_במחלה_קיימת_wrapper select").val();
            var msg_sign = '<span style="color:green">תקין<span>';

            if(sign == 'option1'){
                msg_sign = '<span style="color:red">המבוטח הצהיר שסבל מהבעיה בעבר</span>'; }

            // Validate close-dates
            var date_visit = jQuery("#wpas_תאריך_ביקור_אצל_רופא").val().split("/"); 
            var date_visit_conv = new Date(date_visit[2], date_visit[1] - 1, date_visit[0]);
            var date_policy = jQuery("#acf-תאריך_תחילת_ביטוח input:eq(1)").val();
            if(date_policy != '' &&  date_policy !== undefined ){
                
                var date_policy = date_policy.split("/");
                var date_policy_conv  =new Date(date_policy[2], date_policy[1] - 1, date_policy[0]);
                var msg_data = '<span style="color:green">תקין<span>';

                if((date_visit_conv - date_policy_conv <= 5*24*60*60*1000 && date_visit_conv - date_policy_conv > 0) || (date_policy_conv -  date_visit_conv <= 5*24*60*60*1000 &&  date_policy_conv - date_visit_conv > 0) ){
                   msg_data = '<span style="color:red">תאריך הפוליסה קרוב לביקור</span>'; }
            }else{
                msg_data = '<span style="color:red">תאריך הפוליסה לא תקין</span>';
            }
            // Validate close-dates 2
            var date_out = jQuery("#wpas_תאריך_יציאה_מהארץ").val().split("/"); 
            var date_out_conv = new Date(date_out[2], date_out[1] - 1, date_out[0]);
            
             var date_policy = jQuery("#acf-תאריך_תחילת_ביטוח input:eq(1)").val();
            if(date_policy != '' &&  date_policy !== undefined ){
                
                var date_policy = date_policy.split("/");
                var date_policy_conv  =new Date(date_policy[2], date_policy[1] - 1, date_policy[0]);
                var msg_data_2 = '<span style="color:green">תקין<span>';

                if((date_out_conv - date_policy_conv != 0) ){
                   msg_data_2 = '<span style="color:red">תאריך הפוליסה שונה מהיציאה</span>'; }
            }else{
                msg_data_2 = '<span style="color:red">תאריך הפוליסה לא תקין</span>';
            }
            
        // Validate close-dates 3
           var date_out2 = jQuery("#wpas_תאריך_יציאה_מהארץ").val().split("/"); 
            var date_out_conv2 = new Date(date_out2[2], date_out2[1] - 1, date_out2[0]);
            var date_accident = jQuery("#wpas_תאריך_תאונה").val().split("/");
            var date_accident_conv  =new Date(date_accident[2], date_accident[1] - 1, date_accident[0]);
            var msg_data_3 = '<span style="color:green">תקין<span>';
           
            if((date_out_conv2 - date_accident_conv != 0) ){
               msg_data_3 = '<span style="color:red">תאריך התאונה שונה מתאריך היציאה</span>'; }
                       
            
            // Validate driver
            var driver = jQuery("#wpas_מי_נהג_ברכב_wrapper select").val();
            var msg_driver = '<span style="color:green">תקין<span>';
            
            if(driver == 'option1'){
                msg_driver = '<span style="color:red">המבוטח הצהיר שהוא הנהג</span>'; }

            // Validate pregnant
            var pregnent = jQuery("#wpas_כמה_עוברים_בהריון_זה").val();
            var msg_pregnent = '<span style="color:green">תקין<span>';
            
            if(pregnent != 'option1'){
                msg_pregnent = '<span style="color:red">הריון עם יותר מעובר אחד</span>'; }
          
             // Validate poriut
            var poriut = jQuery("#wpas_ההריון_הושג").val();
            var msg_poriut = '<span style="color:green">תקין<span>';
            
            if(poriut == 'options1'){
                msg_poriut = '<span style="color:red">היה צורך בטיפולים</span>'; }
            


            // Print the alert boxes
            var box = '<div id="wpas-mb-alerts" class="postbox "><button type="button" class="handlediv button-link" aria-expanded="true"><span class="screen-reader-text">פתיחת/סגירת תפריט: התראות</span><span class="toggle-indicator" aria-hidden="true"></span></button><h2 class="hndle ui-sortable-handle"><span>התראות</span></h2><div class="inside"><div id="wpas-alerts"><label for="wpas-issuer"><strong>תאימות המטבע למדינה</strong></label><p>'+msg_coin+'</p><hr><label for="wpas-assignee"><strong>הצהרת המבוטח למחלה</strong></label><p>'+msg_sign+'</p><hr><label for="wpas-assignee"><strong>תאריך ביקור ופוליסה</strong></label><p>'+msg_data+'</p><hr><label for="wpas-assignee"><strong>תאריך יציאה ופוליסה</strong></label><p>'+msg_data_2+'</p><hr><label for="wpas-assignee"><strong>תאריך יציאה ותאריך תאונה</strong></label><p>'+msg_data_3+'</p><hr><label for="wpas-assignee"><strong>הצהרת המובטח לנהג</strong></label><p>'+msg_driver+'</p><hr><label for="wpas-assignee"><strong>מספר עוברים</strong></label><p>'+msg_pregnent+'</p><hr><label for="wpas-assignee"><strong>טיפולי פוריות</strong></label><p>'+msg_poriut+'</p></div></div></div>';
            jQuery('#wpas-mb-alerts').replaceWith(box);
            
        },5000);
        
    }
   
});
</script> <?php
}
add_action('admin_head', 'pw_loading_scripts_wrong');