<?php

require_once( '../../../wp-load.php' );  // loading wordpress for WP commands like get_posts() so they would work in here (ajax file)

if(isset($_REQUEST['doc_name'])  && $_REQUEST['doc_name'] != ''){
    $doc_name = $_REQUEST['doc_name'];
    $rating = $_REQUEST['rating']; 
    $notes = $_REQUEST['notes'];
    $langs = $_REQUEST['langs'];
    $tel = $_REQUEST['tel'];
    $specialization = $_REQUEST['specialization']; 
    
    // Create post object
    $my_post = array(
      'post_title'    => $_REQUEST['doc_name'],
      'post_status'   => 'draft',
      'post_author'   => 1,
      'post_type'   => 'doctors'
    );

    // Insert the post into the database
    $post_id = wp_insert_post( $my_post);
    
    update_field('rating', $rating, $post_id);
    update_field('notes', $notes, $post_id);
    update_field('new_doctor', 'חדש', $post_id);
    update_field('phone_number', $tel, $post_id);
    update_field('language1', $langs, $post_id);
    update_field('specialization', $specialization, $post_id);
     
    echo 'done';
}else{
    echo 'error with fields';
}
return;

?>