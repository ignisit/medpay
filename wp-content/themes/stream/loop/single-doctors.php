<?php 

$firstOption="כתובת שגויה";
$secondOption="מספר טלפון שגוי";
$thirdOption="התמחות לא נכונה";
$fourthOption="הרופא אינו פעיל יותר";

echo "<div class='doctor_banner'>";

if (!get_the_post_thumbnail()){
    
       echo "<img src = 'http://medpay.c14.co.il/wp-content/uploads/2016/03/Layer-28.png' style='background-repeat:no-repeat;'";
    
}else{
    
      the_post_thumbnail();
}

    echo "<ul><li class='doctor_name'>";
       the_title();
    echo "</li>";

    echo "<li class='doctor_type'>".get_field('doctor_type')."</li>";

    echo "<li class='rating'><img src='/wp-content/themes/stream/assets/images/stars/". get_field('rating')."stars.png'></li>";
    echo "</ul></div>";

    echo "<div class='doctor_bar'><ul>";
    echo "<li class='call_to'><a href='tel:".get_field('phone_number')."'>חייג לרופא</a></li>";
    echo "<li class='to_web'><a href='".get_field('website')."'>לאתר</a></li>";

    echo "<li class='send_exp'><a href='/%d7%94%d7%92%d7%a9%d7%aa-%d7%aa%d7%91%d7%99%d7%a2%d7%94?doc_name=".get_the_title()."&doc_type=".get_field('doctor_type')."&doc_address=".get_field('adress')['address']."&doc_langs=".get_field('language1')."'>בקשה להחזר</a></li>";
    $cord_a =  get_field('cord_a');
    $cord_b =  get_field('cord_b');
    //echo do_shortcode('[my_waze]');
    @$adress = get_field('adress');
    if (isset($adress)){
         echo  "<li class='navigate'><a href='http://maps.google.com/?q=". $adress['address']."'>ניווט למרפאה</a></li>";
    }
   
    echo "</ul></div>";

echo "<div class='doctor_info'><ul>";
    echo "<li class='appartment'>". get_field('appartment')."</li>";
    @$adress = get_field('adress');
    if (isset($adress)){
        echo "<li class='adress'><a href='http://maps.google.com/?q=". $adress['address']."'>". $adress['address']."</a></li>";
    }else{
        
        echo "אין כתובת";
    }

    echo "<li class='phone_number'>". get_field('phone_number')."</li>";
    echo "<li class='website'><a href='".get_field('website')."'>". get_field('website')."</a></li>";
    echo "<li class='langs'>". get_field('language1')."</li>";
    
    echo "<li class='notes'>". get_field('notes')."</li>";

echo "</ul></div>";

    echo "<h6 style='color: black !important; font-weight: bold; margin-right:20px;'>דיווח על שגיאה:</h6>";
    echo "<select name='errors' class='doc_errors'>";
 
        echo "<option value=1></option>";
        echo "<option value=2>".$firstOption."</option>";
        echo "<option value=3>".$secondOption."</option>";
        echo "<option value=4>".$thirdOption."</option>";
        echo "<option value=5>".$fourthOption."</option>";  

    echo "</select>";
    echo "<span class='doc_result'>דיווח התקבל</span>";




?>