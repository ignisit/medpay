<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if((get_the_ID() != 2 && !is_user_logged_in()) && get_the_ID() != 43  && get_the_ID() != 418){ // If not in login page or register page - and user is not logged in, redirect to login.
       header("Location: /");
       exit;
}
if((get_the_ID() == 2  || get_the_ID() == 43) && is_user_logged_in()){ // If user is logged in and trying to get login/register page, redirect to dashboard
        header("Location: /dashboard");
       exit;
}

// Homepage - notifications
if((get_the_ID() == 13)){ // If user is logged in and trying to get login/register page, redirect to dashboard
    $user_id = get_current_user_id();
    $sql = "SELECT n.*
FROM `wp_posts` n
JOIN `wp_posts` t ON t.ID LIKE n.post_parent 
WHERE n.`post_excerpt` LIKE '' 
AND n.`post_type` LIKE 'ticket_reply' 
AND t.`post_author` LIKE ".$user_id;
    
    $notifications = $wpdb->get_results($sql);
    $found = false;
    foreach($notifications as $not){
        //echo $not->ID.', ';
        $found = true;
    }
    //echo "<style>.push_icon {display: none;}.no-push-icon{display:block !important;}</style>";
    
    //echo $sql;
    //echo "<br>";
}
if(get_post_type() == 'ticket'){
    $user_id = get_current_user_id();
    $ticket_id = get_the_ID();
    $sql = "UPDATE wp_posts
SET post_excerpt = 'seen'
WHERE post_type = 'ticket_reply'
AND post_parent = ".$ticket_id;
    
    $wpdb->get_results($sql);
}


$logo_img = get_theme_mod( 'image_logo' ); // Getting from option your choice.
$sticky_logo_img = get_theme_mod( 'image_sticky_header_logo' ); // Getting from option your choice.
if ( ! $sticky_logo_img )
	$sticky_logo_img = $logo_img;

$layout_site_default = 'wide';
$layout_site = get_theme_mod( 'layout_site', $layout_site_default );
if ( empty( $layout_site ) || ! in_array( $layout_site, array( 'normal', 'wide' ) ) )
	$layout_site = $layout_site_default;

?><!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" />
    <activity
android:name="com.example.android.GizmosActivity"
android:label="@string/title_gizmos" >
<intent-filter>
    <data android:scheme="anton" />
    <action android:name="android.intent.action.VIEW" />
    <category android:name="android.intent.category.DEFAULT" />
    <category android:name="android.intent.category.BROWSABLE" /> 
</intent-filter>
        
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php wp_head(); ?>
        <!-- link to font-awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
     <!-- Load jQuery UI CSS  -->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    
    <!-- Load jQuery JS -->
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <!-- Load jQuery UI Main JS  -->
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript"
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClwCaQKTwtPeEyfvOUCBvPShbS_I-DK_8&libraries=geometry">
</script></head>

<body <?php body_class(); ?>>
   
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri (); ?>/custom.css"> 
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri (); ?>/custom-extra.css"> 
    <script src="<?php echo get_template_directory_uri (); ?>/assets/js/autocomplete.js"></script>
    <script src="<?php echo get_template_directory_uri (); ?>/assets/js/custom.js"></script>
    <script src="<?php  echo get_template_directory_uri (); ?>/assets/js/custom2.js"></script>  
    <script src="<?php  echo get_template_directory_uri (); ?>/assets/js/custom3.js"></script>
    <script src="<?php  echo get_template_directory_uri (); ?>/assets/js/custom4.js"></script> 
    <script src="<?php  echo get_template_directory_uri (); ?>/assets/js/custom5.js"></script> 
    


    <?php // Pop ups 

if($found){ ?>
        <style>.push_icon{
            display:block !important;
        }.no-push-icon{
            display:none !important;
        }</style>
    <? }else{ ?>
        <style>.push_icon{
            display:none !important;
        }.no-push-icon{
            display:block !important;
        }</style>
   <?php  }


if(!isset($_SESSION['pop3'])){
    $_SESSION['pop3'] = true;
}
if(is_singular( 'doctors' ) && $_SESSION['pop3']){
    echo do_shortcode('[sg_popup id=3]');
    $_SESSION['pop3'] = false;
} ?>
<?php // more pop ups (consulting doctor popup)

   global $post;    
   $page_id = $post->ID;
                         
    if (($page_id == 13) && ($_GET['h'] == 1)){    
        echo do_shortcode('[sg_popup id=5]');                
    }
?>
    
  <?php $user_ID = get_current_user_id(); 
    echo "<input id='uid' type='hidden' name='uid' value='".$user_ID."'>";
?>  
      
<!--[if lt IE 7]><p class="chromeframe">Your browser is <em>ancient!</em>
	<a href="http://browsehappy.com/">Upgrade to a different browser</a> or
	<a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.
</p><![endif]-->

<div id="container" class="<?php echo esc_attr( str_replace( '_', '-', $layout_site ) ); ?>">
	<?php if ( ! pojo_is_blank_page() ) : ?>

	<?php po_change_loop_to_parent( 'change' ); ?>

	<div class="container-wrapper">

	<header id="header" class="logo-<?php echo ( 'logo_left' === get_theme_mod( 'header_layout' ) ) ? 'left' : 'right'; ?>" role="banner">
			<div class="<?php echo WRAP_CLASSES; ?>">
				<div class="logo">
                    <div class="home-img">
							<a href="<?php echo esc_url( home_url( '/dashboard/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri (); ?>/assets/images/home.png" alt="<?php bloginfo( 'name' ); ?>" class="home-img-primary" /></a>
						</div>
                    <div class="back_btn"> <a href="javascript: history.go(-1)" style="font-size: 140%; color: white;">
                        <!-- &#65515; --><i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
					<?php //if ( ! empty( $logo_img ) ) : ?>
                    <?php the_title("<div class='logo_php'>","</div>"); ?>
						<div class="logo-img">
							<a href="/" rel="home"><img src="http://app.medpay.global/wp-content/uploads/2016/02/Screen-Shot-2016-02-12-at-16.45.22.png" alt="<?php bloginfo( 'name' ); ?>" class="logo-img-primary" /></a>
						</div>
					<?php /*else : ?>
						<div class="logo-text">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
						</div>
					<?php endif; */?>
					<?php if ( pojo_has_nav_menu( 'primary' ) ) : ?>
					<button type="button" class="navbar-toggle visible-xs home-btn-left" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only"><?php _e( 'Toggle navigation', 'pojo' ); ?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<?php endif; ?>
				</div><!--.logo -->
				<nav class="nav-main" role="navigation">
					<div class="navbar-collapse collapse">
						<div class="nav-main-inner">
							<?php if ( has_nav_menu( 'primary' ) ) : ?>
								<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'menu_class' => 'sf-menu hidden-xs', 'walker' => new Pojo_Navbar_Nav_Walker() ) );
								wp_nav_menu( array( 'theme_location' => has_nav_menu( 'primary_mobile' ) ? 'primary_mobile' : 'primary', 'container' => false, 'menu_class' => 'mobile-menu visible-xs', 'walker' => new Pojo_Navbar_Nav_Walker() ) ); ?>
								<?php if ( get_theme_mod( 'chk_enable_menu_search' ) && pojo_has_nav_menu( 'primary' ) ) : ?>
									<div class="search-header hidden-xs">
										<a href="javascript:void(0);" class="search-toggle" data-target="#search-section-primary">
											<i class="fa fa-search"></i>
										</a>
									</div>
								<?php endif; ?>
							<?php elseif ( current_user_can( 'edit_theme_options' ) ) : ?>
								<mark class="menu-no-found"><?php printf( __( 'Please setup Menu <a href="%s">here</a>', 'pojo' ), admin_url( 'nav-menus.php?action=locations' ) ); ?></mark>
							<?php endif; ?>
						</div>
					</div>
				</nav><!--/#nav-menu -->
			</div><!-- /.container -->
		</header>

		<?php if ( get_theme_mod( 'chk_enable_menu_search' ) ) : ?>
			<div class="hidden-xs">
				<div id="search-section-primary" class="search-section" style="display: none;">
					<div class="<?php echo WRAP_CLASSES; ?>">
						<?php get_search_form(); ?><i class="fa fa-search"></i>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="sticky-header-running"></div>

		<?php if ( get_theme_mod( 'chk_enable_sticky_header' ) ) :?>
			<div class="sticky-header logo-<?php echo ( 'logo_left' === get_theme_mod( 'header_layout' ) ) ? 'left' : 'right'; ?>">
				<div class="<?php echo WRAP_CLASSES; ?>">
					<div class="logo">
						<?php if ( ! empty( $sticky_logo_img ) ) : ?>
							<div class="logo-img">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo esc_attr( $sticky_logo_img ); ?>" alt="<?php bloginfo( 'name' ); ?>" class="logo-img-secondary" /></a>
							</div>
						<?php else : ?>
							<div class="logo-text">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
							</div>
						<?php endif; ?>
						<?php if ( pojo_has_nav_menu( 'sticky_menu' ) ) : ?>
						<button type="button" class="navbar-toggle visible-xs" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only"><?php _e( 'Toggle navigation', 'pojo' ); ?></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<?php endif; ?>
					</div><!--.logo -->
					<nav class="nav-main" role="navigation">
						<div class="navbar-collapse collapse">
							<div class="nav-main-inner">
								<?php if ( has_nav_menu( 'primary' ) ) : ?>
									<?php wp_nav_menu( array( 'theme_location' => 'sticky_menu', 'container' => false, 'menu_class' => 'sf-menu hidden-xs', 'walker' => new Pojo_Navbar_Nav_Walker() ) );
									wp_nav_menu( array( 'theme_location' => has_nav_menu( 'primary_mobile' ) ? 'primary_mobile' : 'primary', 'container' => false, 'menu_class' => 'mobile-menu visible-xs', 'walker' => new Pojo_Navbar_Nav_Walker() ) ); ?>
									<?php if ( get_theme_mod( 'chk_enable_menu_search' ) && pojo_has_nav_menu( 'sticky_menu' ) ) : ?>
										<div class="search-header hidden-xs">
											<a href="javascript:void(0);" class="search-toggle" data-target="#search-section-sticky">
												<i class="fa fa-search"></i>
											</a>
										</div>
									<?php endif; ?>
								<?php elseif ( current_user_can( 'edit_theme_options' ) ) : ?>
									<mark class="menu-no-found"><?php printf( __( 'Please setup Menu <a href="%s">here</a>', 'pojo' ), admin_url( 'nav-menus.php?action=locations' ) ); ?></mark>
								<?php endif; ?>
							</div>
						</div>
					</nav><!--.nav-menu -->
				</div><!-- /.container -->
				<?php if ( get_theme_mod( 'chk_enable_menu_search' ) ) : ?>
					<div class="hidden-xs">
						<div id="search-section-sticky" class="search-section" style="display: none;">
							<div class="<?php echo WRAP_CLASSES; ?>">
								<?php get_search_form(); ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; // end sticky header ?>
	<?php endif; // end blank page ?>

		<?php pojo_print_titlebar(); ?>
		<?php po_change_loop_to_parent(); ?>
        
		<div id="primary">
          
		<div class="<?php echo WRAP_CLASSES; ?>">
			<div id="content" class="<?php echo CONTAINER_CLASSES; ?>">