<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Pojo_Network_Plugins_Reporter extends Pojo_Info_Base_Reporter {

	public function get_title() {
		return __( 'Network Plugins', 'pojo' );
	}

	public function is_enabled() {
		return is_multisite();
	}

	public function get_fields() {
		return array(
			'network_active_plugins' => __( 'Network Plugins', 'pojo' ),
		);
	}

	public function get_network_active_plugins() {
		$active_plugins = get_site_option( 'active_sitewide_plugins' );

		return array(
			'value' => array_intersect_key( get_plugins(), $active_plugins ),
		);
	}
}