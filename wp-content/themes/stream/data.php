<?php

    $arr = array(
        array(
            'optionValue' => 1,
            'optionName' => 'ישראל'
        ),
        array(
            'optionValue' => 2,
            'optionName' => 'ארצות הברית'
        ),
        array(
            'optionValue' => 3,
            'optionName' => 'רוסיה'
        ),
        array(
            'optionValue' => 4,
            'optionName' => 'גרמניה'
        ),
        array(
            'optionValue' => 5,
            'optionName' => 'מרוקו'
        ),
        array(
            'optionValue' => 6,
            'optionName' => 'שוויץ'
        )
    );

//var_dump($arr); die(); ///////////////////////////////////////////////////////////////

     $filtered = array();

    if (strlen($_GET['filterValue']) > 0) {
        foreach ($arr as $a) {
            if (strpos(strtolower($a['optionName']), strtolower($_GET['filterValue'])) !== FALSE) {
                $filtered[] = $a;
            }
        }
    }

    echo json_encode($filtered);