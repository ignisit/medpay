<?php
if(isset($_REQUEST['file'])){
    require( '../../../wp-load.php' );
    
    $list = array();
    
    if($_REQUEST['file']=='file1'){
       //דו"ח של מבוטחים שנרשמו לאפלקציה כדי למצוא רופא אך לא הגישו תביעה
        $sql = "SELECT DISTINCT ID,display_name FROM wp_users WHERE ID NOT IN (SELECT    b.ID FROM `wp_posts` a, wp_users b WHERE `post_type` = 'ticket' AND a.post_author = b.ID ORDER BY post_author)";
        //$sql = "SELECT ID, display_name FROM wp_users ORDER BY ID"; all users
        $results = $wpdb->get_results($sql) or die(mysql_error());
        
        foreach ( $results as $userid ) {
            $user_id       = (int) $userid->ID;
            $user_login    = stripslashes($userid->user_login);
            $display_name  = stripslashes($userid->display_name);
            $list[0] .= $display_name.',';
            //echo $list;
        }
        
    }else if($_REQUEST['file']=='file2'){
        //דו"ח של מבוטחים שפנו לרופא שנמצא ברשימה הקיימת
        $sql = "SELECT DISTINCT a.post_author, b.display_name FROM `wp_posts` a, wp_users b WHERE `post_type` = 'ticket' AND a.post_author = b.ID ORDER BY post_author"; 
        $results = $wpdb->get_results($sql) or die(mysql_error());
        
        foreach ( $results as $userid ) {
            $user_id       = (int) $userid->ID;
            $user_login    = stripslashes($userid->user_login);
            $display_name  = stripslashes($userid->display_name);
            $list[0] .= $display_name.',';
           
        }
    }else if($_REQUEST['file']=='file3'){
        //דו"ח של תביעות שנדחו 
        $sql = "SELECT * FROM  `wp_posts` WHERE  `post_status` LIKE  'refused'AND  `post_type` LIKE  'ticket'ORDER BY  `wp_posts`.`ID` DESC"; 
        $results = $wpdb->get_results($sql) or die(mysql_error());
        
        foreach ( $results as $ticket ) {
            $ticket_id       = (int) $ticket->ID;
            $ticket_link    = $ticket->guid;
            $list[0] .= $ticket_id.',';
            $list[1] .= $ticket_link.',';
           
        }
        
        //תביעות שחיכו
        $sql = "SELECT * FROM  `wp_posts` WHERE  `post_status` LIKE  'refused'AND  `post_type` LIKE  'ticket'ORDER BY  `wp_posts`.`ID` DESC"; 
        $results = $wpdb->get_results($sql) or die(mysql_error());
        foreach ( $results as $ticket ) {
            $ticket_id       = (int) $ticket->ID;
            $ticket_link    = $ticket->guid;
            
            $ticked_end = strtotime($ticket->post_modified);
            $ticked_start = strtotime($ticket->post_date);
            $gap = $ticked_end - $ticked_start;
            $days = 1; // Max Delay Time In Days
            
            if($gap > $days*60*60*24){
                $list[2] .= $ticket_id.',';
                $list[3] .= $ticket_link.',';
            }
           
        }
        
    }else if($_REQUEST['file']=='file4'){
        //דו"ח של זמן טיפול ממוצע וזמן המתנה ממוצע של תביעה
        $sql = "SELECT * FROM  `wp_posts` WHERE  `post_status` LIKE  'processing' AND  `post_type` LIKE  'ticket' ORDER BY  `wp_posts`.`ID` ASC"; 
        $results = $wpdb->get_results($sql) or die(mysql_error());
        
        $counter = 0;
        $total = 0;
        
        foreach ( $results as $ticket ) {
            $ticket_id       = (int) $ticket->ID;
            $ticket_link    = $ticket->guid;
            $ticked_end = strtotime($ticket->post_modified);
            $ticked_start = strtotime($ticket->post_date);
            $gap = $ticked_end - $ticked_start;
            $total = $total + $gap;
            $counter ++;
        }
        
        $avg_process = time2string(round($total/$counter));
        
        $sql = "SELECT * FROM  `wp_posts` WHERE  `post_status` LIKE  'accepted' AND  `post_type` LIKE  'ticket' ORDER BY  `wp_posts`.`ID` ASC"; 
        $results = $wpdb->get_results($sql) or die(mysql_error());
        
        $counter = 0;
        $total = 0;
        
        foreach ( $results as $ticket ) {
            $ticket_id       = (int) $ticket->ID;
            $ticket_link    = $ticket->guid;
            $ticked_end = strtotime($ticket->post_modified);
            $ticked_start = strtotime($ticket->post_date);
            $gap = $ticked_end - $ticked_start;
            $total = $total + $gap;
            $counter ++;
        }
        
        $avg_end = time2string(round($total/$counter));
        
        $list[0] .= 'avg_waiting='.$avg_process.',';
        $list[1] .= 'avg_end='.$avg_end.',';
        
    }else if($_REQUEST['file']=='file5'){
        //דו"ח תפוקה כללי ודו"ח תפוקה לפי מסלקת (כמה תביעות סולקו)
        $sql = "SELECT a.meta_value,b.display_name FROM `wp_postmeta` a,wp_users b WHERE a.meta_key = '_wpas_assignee' AND b.ID = a.meta_value"; 
        $results = $wpdb->get_results($sql) or die(mysql_error());
        $counter = 0;
        $agents = array();
        foreach ( $results as $ticket ) {
            $ticket_agent   = $ticket->display_name;
            $ticket_link    = $ticket->guid;
            
            if(!isset($agents[$ticket_agent])){
               $agents[$ticket_agent] = 0;}
            
            $agents[$ticket_agent]++;
            $counter ++;
        }
        foreach ( $agents as $agent => $count ) {
            $list[0] .= $agent.'='.$count.',';
        }
        
        $list[1] .= 'total='.$counter.',';
    }
    else if($_REQUEST['file']=='file6'){
        //דו"ח של רופאים לפי מדינה / עיר / דירוג / שפות / מחיר
        $sql = "SELECT a.ID, a.post_title, b.meta_key, b.meta_value FROM `wp_posts` a, `wp_postmeta` b WHERE  b.post_id = a.ID AND `post_type` LIKE 'doctors'AND post_status = 'publish' ORDER BY `post_title` ASC "; 
        $results = $wpdb->get_results($sql) or die(mysql_error());
        $docs = array();
        $doc_adress = array();
        $doc_rating = array();
        $doc_lang = array();
        $doc_price = array();
        
        foreach ( $results as $doctor ) {
           $docs[$doctor->ID] = $doctor->post_title;
            if($doctor->meta_key == 'adress'){
                $doc_adress[$doctor->ID] = $doctor->meta_value;}
            if($doctor->meta_key == 'rating'){
                $doc_rating[$doctor->ID] = $doctor->meta_value;}
            if($doctor->meta_key == 'language1'){
                $doc_lang[$doctor->ID] = $doctor->meta_value;}
            if($doctor->meta_key == 'price'){
                $doc_price[$doctor->ID] = $doctor->meta_value;}
        }
        
        $list[0] .= "name,adress,rating,lang,price";
        foreach($docs as $doc_id => $name){
             $list[] = $name.','.unserialize($doc_adress[$doc_id])['address'].','.$doc_rating[$doc_id].','.$doc_lang[$doc_id].','.$doc_price[$doc_id].','; 
        }
          
        
    }
    else if($_REQUEST['file']=='file7'){
        //דו"ח של רופאים חדשים שנוספו למאגר
         $sql = "SELECT a.ID, a.post_title, b.meta_key, b.meta_value FROM `wp_posts` a, `wp_postmeta` b WHERE  b.post_id = a.ID AND `post_type` LIKE 'doctors' ORDER BY `post_title` ASC "; 
        $results = $wpdb->get_results($sql) or die(mysql_error());
        
        foreach ( $results as $doctor ) {
            if($doctor->meta_key == 'new_doctor'){
                if($doctor->meta_value == 'חדש'){
                    $list[0] .= $doctor->post_title.',';
                }
            }
        }
        
    }
    else if($_REQUEST['file']=='file8'){
        //דו"ח של מבוטחים שהגישו תביעה אך לא חיפשו רופא
            // meta_vaule 1 = from search
            // meta_vaule 2 = defaul no search
        $sql = "SELECT a.post_author, b.display_name, a.ID, c.meta_value
FROM
     wp_posts a
INNER JOIN
     wp_users b on a.post_author = b.ID
LEFT JOIN 
     wp_postmeta c on c.post_id = a.ID
WHERE 
     a.post_type = 'ticket'
     AND c.meta_key = '_wpas_שדה_נסתר'
     AND c.meta_value = 2
     ORDER BY post_author";
        
        $results = $wpdb->get_results($sql) or die(mysql_error());
        
        foreach ( $results as $userid ) {
            //$user_id       = (int) $userid->post_author;
            $display_name  = stripslashes($userid->display_name);
            $list[0] .= $display_name.',';
           
        }
    }
    
    
    
    
    $url = get_template_directory();
    $file_name = $_REQUEST['file'];
    $file = fopen($url."/reports/".$file_name.".csv","w");

    foreach ($list as $line)
      {
      fputcsv($file,explode(',',$line));
      }

    fclose($file);
    echo $file_name;
}
else{
    echo "Error, Filename was no found.";
}
?>