    // In Page 43  registration page
$(document).ready(function () {
    
    if ($(".page-id-43")[0]) {
        
        $('.form-submit #register').val('הירשמו');
        var agree = $('#agree_div').html();
        $('.form-submit').before(agree);
        $('#agree_div').remove();
        $('#username').attr("type","number");
        
        
        jQuery('#username').blur(function(){
        
            if (jQuery('#username').val().length != 9){
                
                jQuery('#username').focus().css("border-color","red");
                alert("ת.ז. לא חוקי");

            }else{
                jQuery('#username').css("border-color","transparent");
            }        
        
        });
        
        
        
        if(jQuery('#wppb_general_top_error_message').html()=="There was an error in the submitted form"){    
            jQuery('#wppb_general_top_error_message').html("שגיאה באחד השדות").css("color","red");
        }
        
        
        
        jQuery('#wppb-register-user ul li span.wppb-form-error').each(function(){
            
          if(jQuery(this).html() == "This username is invalid because it uses illegal characters.<br>Please enter a valid username."){
              
                jQuery(this).html("משתמש זה לא חוקי כי יש שימוש בתווים לא חוקים. בבקשה להכניס משתמש חוקי.").css("color","red");
          }
            
          else if(jQuery(this).html() == "This field is required"){
             jQuery(this).html("שדה חובה").css("color","red");
          }
            else if(jQuery(this).text() == "This username already exists.Please try a different one!"){
                
                 jQuery(this).text("שמ משתמש קיים!").css("color","red"); 
            }
            else if(jQuery(this).text() == "This email is already in use.Please try a different one!"){
                
                 jQuery(this).text("אימייל קיים!").css("color","red"); 
            }
            else if (jQuery(this).val() == "The account "+ username + " has been successfully created!"){
                
                 jQuery(this).val("משתמש נוצר בהצלחה!").css("color","red"); 
            
            }
        });
        
    
        
    //  jQuery('#wppb-register-user').attr('action','/');   // to attend later on , redirect issue, doesn't register new users
        jQuery('.form-submit #register').click(function(e){

              var a = jQuery('input#takanon').is(":checked");
              if (a == true){
                  console.log("a: " + a);
                  
                  var username = jQuery('#username').val();
                  console.log("username: " + username);
             //     jQuery('#register').unbind('click');
                    // move to dashboard page after registration
                  if (jQuery('#wppb-form-element-2').val()!= 0) {
                      window.location.href ="/";
                  }
                  
               setTimeout(function(){     
                        setInterval(function(){
                              console.log("moving...");
                            window.location.href='/'; 
                        }, 2000);    
                   });

              }else{
              
                   e.preventDefault();
                //   console.log("a: " + a);                        
                   alert('חובה לאשר את תקנון השימוש');

              }//else
      });//jquery
        
    }// page id 43
     
    
    
    
    if ($(".page-id-300")[0] || (".single-ticket")[0]) {
       $('.wpas-alert-info').text('לא נפתחו פניות');
       $('#wpas-ticket-status').text('סטטוס');
        $('#wpas-ticket-title').text('קישור');
        $('#wpas-ticket-date').text('תאריך');
        $('.single-ticket .wpas-ticket-details-header th').each(function(){
           var th = $(this).text();
             if (th == 'ID'){
               $(this).text('מספר פניה');
           }
            if (th == 'Status'){
               $(this).text('סטטוס');
           }if (th == 'Date'){
               $(this).text('תאריך שליחה');
           }
        });
       $('#wpas-reply-textarea').attr('placeholder','כתוב את תגובתך כאן');
       $('#wpas-reply-textarea').attr('rows','5');
        $('.wpas-attachment-container label').text('קבצים מצורפים');
        $('.wpas-attachment-container a').text('לחץ כאן להוספת קובץ');
         $('.single-ticket .wpas-btn.wpas-btn-default').text('שלח');
        $('.single-ticket h3').text('כתוב תגובה חדשה');
        
        
       $('.wpas-label').each(function(){
           var label = $(this).text();
           if (label == 'New'){
               $(this).text('בטיפול');
           }
           if (label == 'In Progress'){
               $(this).text('בטיפול');
           }
           if (label == 'Closed'){
               $(this).text('נסגרה');
           }
           if (label == 'Accepted'){
               $(this).text('אושרה');
           }
           if (label == 'Refused'){
               $(this).text('נדחתה');
           }
           if (label == 'On Hold'){
               $(this).text('בטיפול');
           }
           
       });
        $('table#wpas_ticketlist a').each(function(){
               $(this).text('לחץ כאן');
        });

             $('#main').show();
         
    }
    
    
});