function isInternetExplorer() {
    var b = document.createElement('B'),
        d = document.documentElement,
        isInternetExplorer;

    b.innerHTML = '<!--[if lte IE 8]><b id="gsietest"></b><![endif]-->';
    d.appendChild(b);
    isInternetExplorer = !!document.getElementById('gsietest');
    d.removeChild(b);
    return isInternetExplorer;
}

function GsAutocomplete(element, data) {
    data = data || {};
    var element = element || document.getElementById('gs-input'),                                   // the current element
        parent = element.parentNode,                                                                // the main container
        dataListValues = parent.querySelectorAll('datalist option'),                                // values found in the datalist object
        availableOptions = [],                                                                      // variable to store available options
        theValues,                                                                                  // the values
        autoValue,
        elementValue,                                                                               // the value of the element
        filteredValues = [],                                                                        // filtered values
        currentValue = -1,                                                                          // current navigation value
        actualInput = document.createElement('input'),                                              // the hidden input
        request,                                                                                    // variable to store the requests to the server
        cancelFilter = false,
        queryTimeout = 0,

        // setting defaults
        useURL = data.useURL || false,                                                              // set whether to use the theSourceURL URL for the initial data load or not
                                                                                                    //  - only if grabOnChange option is false;
        useDatalist =  data.useDatalist || false,                                                   // set wheter to get the data from a datalist HTML element or not
                                                                                                    //  - only if grabOnChange option is false;
                                                                                                    //  - if true, it overwrites the useURL option to false;
        grabOnChange =  data.grabOnChange || false,                                                 // set true if you want the theSourceURL to be called each time
                                                                                                    // the element's value is changed
        theSourceURL = data.theSourceURL || '',                                                     // the URL source to use with useURL or grabOnChange options
        maxValuesToShow = data.maxValuesToShow || 4,                                                // the maximum number of values to be shown
        requestDelay = data.requestDelay || 200,                                                    // the minimum time to wait before an AJAX call is fired
        searchableIndex = data.searchableIndex || 'optionValue',                                    // the property that should be matched with the input value when filtering
        returnValue = data.returnValue || 'optionValue',                                    // the property that should be matched with the input value when filtering
        template = data.template || '<div class="gs-input-value">{@optionName}</div>',              // the HTML template for each separate option in the autocomplete values
                                                                                                    //  - all the {@property-name} will be replaced with the actual properties
                                                                                                    // in the object
        beforeServerRequest = data.beforeServerRequest || function() {  },                          // callback to execute before the AJAX calls are made to the server
        onServerSuccess = data.onServerSuccess || function() {  },                                  // callback to execute when an AJAX call was successfull
        onServerFailure = data.onServerFailure || function() {  },                                  // callback to execute when an AJAX call failed
        onSelectedOption = data.onSelectedOption || function() {  },                                // callback to execute when an option is selected
        onCancel = data.onCancel || function() {  };                                                // callback to execute when the user presses the 'Esc' key

    actualInput.setAttribute('type', 'hidden');
    actualInput.setAttribute('name', 'actual-' + element.getAttribute('name'));
    parent.appendChild(actualInput);

    element.className += ' gs-autocomplete-field';


    if (useDatalist) {
        useURL: false;
    }

    if (grabOnChange) {
        useDatalist = false;
        useURL: false;
    }

    if (!theSourceURL) {
        grabOnChange = false;
        useURL: false;
        useDatalist: true;
    }


    // get the object elements, if the object is generated at start
    function grabElements() {
        var request, additional, queryString, additionalParrent;
        if (useDatalist) {
            availableOptions.length = 0;
            for (i = 0; i < dataListValues.length; i++) {
                availableOptions[i] = {
                    optionValue: dataListValues[i].getAttribute('value')
                };
            }
        } else {
            additionalParrent = parent.querySelector('.gs-additional-inputs');
			additional = [];
			if (additionalParrent) {additional = additionalParrent.querySelectorAll('input')}
            queryString = '';
            if (additional.length) {
                for (var i = 0; i < additional.length; i++) {
                    queryString += ((queryString.length) ? '&' : '') + additional[i].getAttribute('name') + '=' + additional[i].value;
                }
            }
            if (useURL && !grabOnChange) {
                beforeServerRequest();
                request =  new XMLHttpRequest();
                request.open('get', theSourceURL + (theSourceURL.indexOf('?') !== -1 ? '&' : '?') + queryString);
                request.send();
                request.onreadystatechange = function() {
                    if (request.readyState === 4 && request.status === 200) {
                        availableOptions = JSON.parse(request.responseText);
                        onServerSuccess(request.responseText);
                    } else {
                        if (request.readyStateChange === 4) {
                            onServerFailure({ status: request.status });
                        }
                    }
                }
                availableOptions = '';
            }
        }
    }

    // build the options list
    function createElement() {
        var paths;
        theValues = document.createElement('ul');
        theValues.className = 'gs-autocomplete-values';
        parent.appendChild(theValues);

        if (isInternetExplorer()) {
            theValues.attachEvent('onclick', clickValue);
        } else {
            theValues.addEventListener('click', clickValue);
        }
        
        function clickValue(e) {
            var clickedElement = e.target || e.srcElement;
            while ((clickedElement.tagName.toLowerCase() !== 'li' || clickedElement.className.search('gs-autocomplete-element') === -1) && clickedElement.parentNode) {
                clickedElement = clickedElement.parentNode;
            }
            
            if (clickedElement.tagName.toLowerCase !== 'body') {
                actualInput.value = clickedElement.getAttribute('data-value');
                element.value = clickedElement.getAttribute('data-text');
                cancelFilter = false;
                currentValue = -1;
                theValues.innerHTML = '';
                onSelectedOption(clickedElement.getAttribute('data-value'));
            }
        }
    }

    // filter the object on each input change
    function filterObject() {
        var value = element.value;
        filteredValues.length = 0;
        if (!grabOnChange) {
            if (value) {
                if (!useDatalist) {
                    for (var i = 0; i < availableOptions.length; i++) {
                        if (availableOptions[i][searchableIndex].toLowerCase().search(value.toLowerCase()) !== -1) {
                            filteredValues.push(availableOptions[i]);
                        }
                    }
                } else {
                    for (var i = 0; i < availableOptions.length; i++) {
                        if (availableOptions[i].optionValue.toLowerCase().search(value.toLowerCase()) !== -1) {
                            filteredValues.push(availableOptions[i]);
                        }
                    }
                }
                buildOptions();
            }
        } else {
            clearTimeout(queryTimeout);
            queryTimeout = setTimeout(function() { getInstantValues(); }, requestDelay);
        }
    }

    // if the instant value option is enabled, we will make the call to the server
    function getInstantValues() {
        var value = element.value, additional, additionalParrent,
            queryString = 'filterValue=' + element.value
			additionalParrent = parent.querySelector('.gs-additional-inputs');
			additional = [];
			if (additionalParrent) {additional = additionalParrent.querySelectorAll('input')}
        if (additional.length) {
            for (var i = 0; i < additional.length; i++) {
                queryString += ((queryString.length) ? '&' : '') + additional[i].getAttribute('name') + '=' + additional[i].value;
            }
		}
            beforeServerRequest();
            request =  new XMLHttpRequest();
            request.open('get', theSourceURL + (theSourceURL.indexOf('?') !== -1 ? '&' : '?') + queryString);
            request.send();
            request.onreadystatechange = function() {
                if (request.readyState === 4 && request.status === 200) {
                    filteredValues = JSON.parse(request.responseText);
                    onServerSuccess(request.responseText);
                    buildOptions();
                } else {
                    if (request.readyStateChange === 4) {
                        onServerFailure({ status: request.status });
                    }
                }
            }
    }

    // build the new options list according with the filter
    function buildOptions() {
        theValues.innerHTML = '';
        for (var i = 0; i < filteredValues.length; i++) {
            elementValue = template;
            autoValue = document.createElement('li');
            autoValue.className = ' gs-autocomplete-element';
            autoValue.setAttribute('data-value', filteredValues[i].optionValue);
            autoValue.setAttribute('data-text', filteredValues[i][searchableIndex]);
            for (var key in filteredValues[i]) {
                do {
                    elementValue = elementValue.replace('{@' + key + '}', filteredValues[i][key])
                } while (elementValue.search('{@' + key + '}') !== -1);
            }
            autoValue.innerHTML = elementValue;
            theValues.appendChild(autoValue);
            if (i >= maxValuesToShow - 1) return;
        }
        currentValue = -1;
    }

    // move the current highlighted option up or down
    function moveOption(direction) {
        var curentSelected;
        if (filteredValues.length) {
            if (direction === 'up') {
                currentValue++;
            } else if (direction === 'down') {
                currentValue--;
            }
            if (currentValue >= Math.min(maxValuesToShow, filteredValues.length)) currentValue = 0;
            if (currentValue < 0) currentValue = (maxValuesToShow <= filteredValues.length) ? maxValuesToShow - 1 : filteredValues.length - 1;
            curentSelected = theValues.querySelector('.current-selected');
            if (curentSelected) curentSelected.className = curentSelected.className.replace(' current-selected', '');
            theValues.querySelectorAll('li')[currentValue].className = theValues.querySelectorAll('li')[currentValue].className.replace(' current-selected', '') + ' current-selected';
        }
    }

    // track all the input value changes
    if (isInternetExplorer()) {
        element.attachEvent('onkeydown', elementChanged);
    } else {
        element.addEventListener('keydown', elementChanged);
    }
    
    function elementChanged(e) {
        var curentSelectedElement;
        if (e.keyCode === 38 || e.keyCode === 40 || e.keyCode === 27 || e.keyCode === 13) {
            cancelFilter = true;
            e.preventDefault();
            switch(e.keyCode) {
                case 27:
                    element.value = '';
                    theValues.innerHTML = '';
                    element.blur();
                    cancelFilter = false;
                    onCancel();
                break;
                case 13:
                    curentSelectedElement = theValues.querySelector('.current-selected');
                    theValues.innerHTML = '';
                    element.blur();
                    if(curentSelectedElement) {
                        actualInput.value = curentSelectedElement.getAttribute('data-value');
                        element.value = curentSelectedElement.getAttribute('data-text');
                    }
                    cancelFilter = false;
                    currentValue = -1;
                    if (curentSelectedElement) {
                        onSelectedOption(curentSelectedElement.getAttribute('data-value'));
                    } else {
                        onSelectedOption(element.value);
                        actualInput.value = element.value;
                    }
                break;
                case 40:
                    moveOption('up');
                break;
                case 38:
                    moveOption('down');
                break;
            }
        }
    };

    if (isInternetExplorer()) {
        element.attachEvent('onkeyup', elementKeyup);
    } else {
        element.addEventListener('keyup', elementKeyup);
    }
    
    function elementKeyup(e) {
        if (!cancelFilter) {
            filterObject();
            buildOptions();
        }
        cancelFilter = false;
    };

    grabElements();
    createElement();
};

if (isInternetExplorer()) {
    document.attachEvent('click', cancelDropdowns);
} else {
    document.addEventListener('click', cancelDropdowns);
}

function cancelDropdowns(e) {
    var inputs = document.querySelectorAll('.gs-autocomplete-field');
    for (var i = 0; i < inputs.length; i++) {
        if (e.target.className.search('gs-autocomplete-field') === -1 || (e.target.id && e.target.id !== inputs[i].id)) {
            inputs[i].parentNode.querySelector('.gs-autocomplete-values').innerHTML = '';
        }
    }
}

