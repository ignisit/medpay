<?php

require_once( '../../../wp-load.php' );  // loading wordpress for WP commands like get_posts() so they would work in here (ajax file)

$args = array(
    
	'posts_per_page'   => -1, // no restriction
	'offset'           => 0,
	'category'         => '',
	'category_name'    => '',
	'orderby'          => 'title',
	'order'            => 'DESC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'doctors',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'author'	   => '',
	'post_status'      => 'publish',
	'suppress_filters' => true 
    
);

$posts_array = get_posts( $args );

$arr = array();
$i= 1;
$hook = "_";// hook to be able to identify whether that is a new doctor in the system

foreach($posts_array as $doctor){
    $new = array('optionValue' => $i, 'optionName' => $doctor->post_title);
    $i++;
    $arr[] = $new;
    
    $doctors_name_hebrew = get_field('doctors_name_hebrew',$doctor->ID);   

    $new2 = array('optionValue' => $i, 'optionName' => $doctors_name_hebrew);
    $i++;
    $arr[] = $new2;
    
}


     $filtered = array();

    if (strlen($_GET['filterValue']) > 0) {
        foreach ($arr as $a) {
            if (strpos(strtolower($a['optionName']), strtolower($_GET['filterValue'])) !== FALSE) {
                $filtered[] = $a;
            }
        }
    }

    echo json_encode($filtered);
