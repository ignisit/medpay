<?php

require_once( '../../../wp-load.php' );  // loading wordpress for WP commands like get_posts() so they would work in here (ajax file)
$args = array(
    
	'posts_per_page'   => -1, // no restriction
	'offset'           => 0,
	'category'         => '',
	'category_name'    => '',
	'orderby'          => 'title',
	'order'            => 'DESC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'doctors',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'author'	   => '',
	'post_status'      => 'publish',
	'suppress_filters' => true 
    
);

$posts_array = get_posts( $args );
if(isset($_REQUEST['doc'])  && $_REQUEST['doc'] != ''){
    $doc_name = $_REQUEST['doc'];
    foreach($posts_array as $doctor){

        $doctors_name_hebrew = get_field('doctors_name_hebrew',$doctor->ID);
        if($doc_name == $doctor->post_title || $doc_name == $doctors_name_hebrew){
            echo 'קיים';
            return;
        }
    }
}
echo 'חדש';
return;

?>