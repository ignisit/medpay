��          �      L      �  q   �  2   3     f     m          �     �  "   �  1   �  )   �  !        <     B     G     c     l     �  &   �  �   �  �   �  ^   f     �     �     �     �     �  -   	  5   7  4   m  4   �     �  
   �     �                9  )   L                                                                  
              	                    Add a Waze navigation button to your mobile WordPress site and get visitors navigate to your location in a click! Autocomplete's returned place contains no geometry Center Current Location: Left MyWaze MyWaze Settings Please enter the desired location: Please make sure all the settings are configured. Please select one of the following icons: Please select the icon alignment: Right Save Savvy Wordpress Development Settings Waze Button Settings http://savvy.co.il https://wordpress.org/plugins/my-waze/ PO-Revision-Date: 2015-12-06 23:42:41+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1100
Project-Id-Version: Development (trunk)
 תוסף המוסיף כפתור ניווט של ווייז בגרסת המובייל של האתר, ומאפשר לגולשים לנווט למיקום שלך בלחיצה! ההשלמה האוטומטית החזירה מיקום שאינו מכיל גיאומטריה אמצע מיקום נוכחי: שמאל MyWaze הגדרות MyWaze יש לציין את המיקום הרצוי: יש לוודא שכל ההגדרות מוגדרות. יש לבחור את אחד הסמלים הבאים: יש לבחור כיוון-יישור את הסמל: ימין שמירה Savvy WordPress Development אפשרויות הגדרת כפתור ווייז http://savvy.co.il https://he.wordpress.org/plugins/my-waze/ 