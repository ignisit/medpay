-- MySQL dump 10.13  Distrib 5.5.14, for Linux (x86_64)
--
-- Host: localhost    Database: ima_medpay
-- ------------------------------------------------------
-- Server version	5.5.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alerts`
--

DROP TABLE IF EXISTS `alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alerts` (
  `claim_id` int(11) NOT NULL,
  `alert_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alerts`
--

LOCK TABLES `alerts` WRITE;
/*!40000 ALTER TABLE `alerts` DISABLE KEYS */;
/*!40000 ALTER TABLE `alerts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alerts_description`
--

DROP TABLE IF EXISTS `alerts_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alerts_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alerts_description`
--

LOCK TABLES `alerts_description` WRITE;
/*!40000 ALTER TABLE `alerts_description` DISABLE KEYS */;
INSERT INTO `alerts_description` VALUES (1,'מבוטח הגיש תביעה בעבר'),(2,'מספר קבלה הוקלד בעבר'),(3,'תאריך האירוע סמוך לתאריך יציאה מהארץ (3ימים) '),(4,'תאריך תחילת הפוליסה הוא לאחר תאריך יציאה מהארץ או לאחר תאריך האירוע'),(5,'המטבע שהוצהר לא תואם את המדינה בה קרה האירוע'),(6,'מחלה - המבוטח הצהיר שמדובר במצב רפואי קיים'),(7,'תאונה/תאונת דרכים - תאריך התאונה קודם לתאריך היציאה מהארץ'),(8,'הריון - המבוטחת הצהירה שמדובר בהיריון מרובה עוברים');
/*!40000 ALTER TABLE `alerts_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claimes`
--

DROP TABLE IF EXISTS `claimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claimes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` decimal(11,2) NOT NULL,
  `price_gived` decimal(11,2) NOT NULL,
  `date_doctor` varchar(255) NOT NULL COMMENT 'time of visit doctor',
  `date_event` varchar(255) NOT NULL COMMENT 'time of the event',
  `date_out` varchar(255) NOT NULL COMMENT 'time of living israel',
  `date_in` varchar(255) NOT NULL COMMENT 'time_of come back to israel',
  `status` int(11) NOT NULL,
  `cause` varchar(255) NOT NULL,
  `description` varchar(2000) NOT NULL,
  `signs_description` varchar(2000) NOT NULL COMMENT 'when signs start to show',
  `disease` tinyint(1) NOT NULL,
  `disease_description` varchar(2000) NOT NULL,
  `type_count` varchar(2000) NOT NULL COMMENT 'type of price',
  `currency` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claimes`
--

LOCK TABLES `claimes` WRITE;
/*!40000 ALTER TABLE `claimes` DISABLE KEYS */;
/*!40000 ALTER TABLE `claimes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctors`
--

DROP TABLE IF EXISTS `doctors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `second_phone` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creator` varchar(255) NOT NULL,
  `home_visit` tinyint(1) NOT NULL,
  `licens` varchar(255) NOT NULL,
  `clinic` varchar(255) DEFAULT NULL,
  `price_local` decimal(11,2) NOT NULL,
  `price_dollar` decimal(11,2) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `create_at` datetime DEFAULT NULL,
  `comments` varchar(2000) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctors`
--

LOCK TABLES `doctors` WRITE;
/*!40000 ALTER TABLE `doctors` DISABLE KEYS */;
INSERT INTO `doctors` VALUES (12,'','','united kingdom','','london','18 Soho Square, London W1D 3QL, United Kingdom','','+44 02031 314 107','enquiries@londondoctorsclinic.co.uk',1,'MedPay',0,'','london doctor clinic oxford',55.00,0.00,'','2016-01-31 12:19:12','',''),(13,'FirstMed ','clinic	','Hungary	','','Budapest	','Budapest, Hattyú u. 14, 1015 Hungary','','+3612249090	','radi@firstmedcenters.com',1,'MedPay',0,'','FirstMed clinic	',50000.00,170.00,'','2015-12-20 11:39:02',' 5th floor',NULL),(14,'','','united kingdom','','london','117 Waterloo Rd, London SE1 8UL, United Kingdom','','+44 02031 314 107','enquiries@londondoctorsclinic.co.uk',1,'MedPay',0,'','london doctor clinic waterloo',55.00,0.00,'+44 020 7183 0596','2016-01-31 12:26:36','',''),(15,'','','united kingdom','','london','80 Cheapside, London EC2V 6EE, United Kingdom','','+44 0845 437 0353','MedicentreAppointments@roodlane.co.uk',1,'MedPay',0,'','medi centre',70.00,0.00,'','2016-01-31 12:31:39','',''),(16,'','','united kingdom','','london','Victoria Station, London SW1V 1JT, United Kingdom','','+44 44 0845 437 0353','MedicentreAppointments@roodlane.co.uk',1,'MedPay',0,'','medi centre',70.00,0.00,'','2016-01-31 14:11:39','',''),(17,'Broadgate general practice','','united kingdom','','london','65 London Wall, London EC2M 5TU, United Kingdom','','+44 020 3411 1257','doctor@broadgategp.co.uk',1,'MedPay',0,'','Broadgate general practice',65.00,0.00,'','2016-01-31 14:17:24','',''),(18,'Emergency Dentist London Baker Street Dental Clinic','','united kingdom','','london','102 Baker Street, London W1U 6FY, United Kingdom','','+44  20 8748 9365','info@24hour-emergencydentist.co.uk',1,'MedPay',0,'','Emergency Dentist London Baker Street Dental Clinic',0.00,0.00,'','2016-01-31 14:27:47','',''),(19,'Emergency Dentist London Liverpool Street Dental Clinic','','united kingdom','','london','9 Artillery Lane, City of London, London E1 7LP, United Kingdom','','+44 20 8748 9365','info@24hour-emergencydentist.co.uk',1,'MedPay',0,'','Emergency Dentist London Liverpool Street Dental Clinic',0.00,0.00,'','2016-01-31 19:44:10','',''),(20,'Emergency Dentist London The Dental Lounge','','united kingdom','','london','221 - 225 Old Brompton Rd, London SW5 0EA, United Kingdom','','+44 20 8748 9365','info@24hour-emergencydentist.co.uk',1,'MedPay',0,'','Emergency Dentist London The Dental Lounge',0.00,0.00,'','2016-01-31 19:49:27','',''),(21,'expat medical centre','','Netherlands','','Amsterdam','Bloemgracht 112, 1015 TN Amsterdam','','+31 20 427 5011','expatmc@planet.nl',1,'MedPay',0,'','expat medical centre',54.00,0.00,'+ 31 0204223913','2015-12-21 12:40:00','',NULL),(22,'Huisartsenpraktijk P.C. Bossen','','Netherlands','','Amsterdam','Tweede Helmersstraat 82, 1054 CM Amsterdam','','+31 20 618 0952','',1,'MedPay',0,'','Huisartsenpraktijk P.C. Bossen',0.00,0.00,'+31 20 6895979','2015-12-21 13:04:18','',NULL),(23,'Groepspraktijk Trompenburg','','Netherlands','','Amstardam','Trompenburgstraat 115, 1079 TV Amsterdam','','+31 20 442 1202','assistent@groepspraktijktrompenburg.nl',1,'MedPay',0,'','Groepspraktijk Trompenburg',29.00,0.00,'','2015-12-21 15:25:57','',''),(24,'Huisartsen Vrolikstraat','','Netherlands','','Amsterdam','Vrolikstraat 191E,1091 TW Amsterdam','',' + 31 20 6938577','info@huisartsenvrolikstraat.nl',1,'MedPay',0,'','Huisartsen Vrolikstraat',26.00,0.00,'+31 20 6656489','2015-12-21 13:22:48','',NULL),(25,'Huisartsenpraktijk Reguliersgracht ','','Netherlands','','Amsterdam','Reguliersgracht 78C, 1017 LV Amsterdam, Netherlands','','+31 20 3449247','',1,'MedPay',0,'','Huisartsenpraktijk Reguliersgracht ',27.00,0.00,'+31 020 6234041','2015-12-21 14:37:17','Websitewww.reguliersgracht.praktijkinfo.nl ',NULL),(28,'united care family medical center','','United States','California','Los angeles','2324 W Pico Blvd Los Angeles, CA 90006','','+1 213 383 3600 ','',1,'MedPay',0,'','united care family medical center',50.00,50.00,'','2016-02-01 09:15:10','','http://www.unitedcaremedical.com'),(29,'united care family medical center','','United States','California','Los Angeles','1835 S La Cienega Blvd Ste 215Los Angeles, CA 90035','','+1 310 836 2273 ','',1,'MedPay',0,'','united care family medical center',50.00,50.00,'','2016-02-01 09:19:53','','http://www.unitedcaremedical.com'),(30,'GRAND MEDICAL ASSOCIATE','','United States','California','Los Angeles','1513 S Grand Ave Ste 320 Los Angeles, CA 90015','','+1 213  7477307 ','',1,'MedPay',0,'','GRAND MEDICAL ASSOCIATE',40.00,40.00,'+1213 7477093','2016-02-01 10:50:11','',''),(31,'Family health center','','United States','California','Los Angeles','1701 E Cesar E Chavez Ave Ste 230 Los Angeles, CA 90033','','+1 323 226 1100 ','',1,'MedPay',0,'','Family care specialists ',75.00,75.00,'+1323 2261101','2016-02-01 09:57:02','suite 230','http://fcsmg.com'),(32,'Family care specialists medical group','','United States','California','Los Angeles','1701 Cesar Chavez Ave, los angeles CA 90033 ste 402','','+1 323  3179200','',1,'MedPay',0,'','Boyle Heights II Clinic',75.00,75.00,'+1 323 3179206','2016-02-01 10:46:41','ste 402','http://fcsmg.com'),(33,' Mary ','McCarthy','Spain','','Barcelona','Carrer d\'Aribau, 08021 Barcelona, Spain','+34 93 200 2924 ','+34 607 220 040','11932mmb@comb.cat',1,'MedPay',0,'','',80.00,0.00,'','2016-01-26 16:18:55','',''),(34,'Haus- und Facharzt-Zentrum','','Germany','','Frankfurt','Schweizer Straße 5, 60594, Frankfurt am Main, Germany','','+49 69 61 99 52 90','info@arzt-frankfurt.de',1,'MedPay',1,'','Haus- und Facharzt-Zentrum',0.00,0.00,' +49 69 61 51 41','2016-01-27 12:18:29','Dr. Ala Weiner is an Hebrew speaker doctor\r\nportable: +49171486268','www.arzt-frankfurt.de'),(35,'Karl-Hermann ','Freybe','Germany','','Munich','Belgradstraße 5, 80796, Munich, Germany','','+49 89 30002503','',1,'MedPay',0,'','',0.00,0.00,'49 89  30002504','2016-01-27 13:31:30','',''),(36,'Dr. Frühwein & Partner','','Germany','','Munich','Brienner Straße 11, 80333, Munich, Germany','','+49 89 223523','',1,'MedPay',0,'','Dr. Frühwein & Partner',0.00,0.00,'+49 89  22 836 45','2016-01-27 13:37:19','','http://www.drfruehwein.de'),(37,'Michael','Drefers','Germany','','Munich','Nymphenburger Str. 151, 80634, Munich, Germany','','+49 89 1665640  ','',1,'MedPay',0,'','',0.00,0.00,'','2016-01-27 13:40:44','',''),(38,'Munich Dental Clinic Dr. Koty & Partner','','Germany','','Munich','Trautenwolfstraße 8, 80802, Munich, Germany','','+49 89 222110','empfang@koty.de',1,'MedPay',0,'','Munich Dental Clinic Dr. Koty & Partner',0.00,0.00,'+49 89 22 21 10','2016-01-27 13:44:25','',''),(39,'Christiane','Klein','Germany','','Frankfurt','Berger Straße 159, 60385, Frankfurt, Germany','','+49 69 44 40 83','',1,'MedPay',0,'','Facharztpraxis für Allgemeinmedizin Dr. med. Elke Iburg - Christiane Klein',0.00,0.00,'','2016-01-27 11:55:34','','https://www.iburg-klein.de'),(40,'Elke','Iburg','Germany','','Frankfurt','Berger Straße 159, 60385, Frankfurt, Germany','','+49 69 44 40 83','',1,'MedPay',0,'','Facharztpraxis für Allgemeinmedizin Dr. med. Elke Iburg - Christiane Klein',0.00,0.00,'','2016-01-27 12:01:26','','https://www.iburg-klein.de'),(41,'Dental care solutions','','Germany','','Frankfurt','Leipziger Straße 4, 60487, Frankfurt, Germany','','+49 69 97 78 33 60','',1,'MedPay',0,'','Dental care solutions',0.00,0.00,'','2016-01-27 12:06:26','','http://www.dr-ott.net'),(42,'Padilla Dental Care Clinic ','','Germany','','Frankfurt','Goethestraße 25, 60313, Frankfurt, Germany','','+49 69 13383674','praxis@zahnarzt-padilla.de',1,'MedPay',0,'','Padilla Dental Care Clinic ',0.00,0.00,'+49 69 21089823 ','2016-01-27 12:10:51','','http://www.zahnarzt-padilla.com'),(43,'New York Doctors walk in urgent care','','United States','New York','New York','65 West 13th Street, New York, NY 10011, United States','','+1 212 414 2800','',1,'MedPay',0,'','New York Doctors  urgent care, PLLC',125.00,125.00,'','2016-02-01 11:36:25','walk in clinic','http://www.nydoctorsurgentcare.com'),(44,'New York Doctors walk in urgent care','','United States','New York','New York','205 Lexington Avenue, New York, NY 10016, United States','','+1 212 684 4700','',1,'MedPay',0,'','Midtown NY Doctors Urgent Care, PLLC ',125.00,125.00,'','2016-02-01 11:41:57','walk in clinic','http://www.nydoctorsurgentcare.com'),(45,'Heijnen & de Meij','','Netherlands','','Amsterdam','Oudezijds Voorburgwal 151,1012 ES Amsterdam','','+31 206227471','',1,'MedPay',0,'','Heijnen & de Meij',30.00,0.00,'','2016-01-26 15:36:10','','https://heijnendemeij.zorgrotonde.net'),(46,'citymd','','United States','New York','New York','87 Chambers Street, New York, NY 10007, United States','','+1 212 335 0594','',1,'MedPay',0,'','CityMD Urgent Care TriBeCa',125.00,125.00,'','2016-02-01 11:46:15','','www.citymd.com'),(47,'citymd','','United States','New York','New York','1500 Lexington Avenue, New York, NY 10029, United States','','+1 212 710 1065','',1,'MedPay',0,'','East 96th Urgent Care',125.00,125.00,'','2016-02-01 11:52:16','','www.citymd.com'),(48,'citymd','','United States','New York','New York','345 West 42nd Street, New York, NY 10036, United States','','+1 646 518 0159','',1,'MedPay',0,'','CityMD Urgent Care 42nd Street ',125.00,125.00,'','2016-02-01 11:54:34','','www.citymd.com'),(49,'citymd','','United States','New York','New York','216 E 14th St, New York, NY 10003, United States','','+1 212 256 1049','',1,'MedPay',0,'','Union Square East Urgent Care',125.00,125.00,'','2016-02-01 11:56:37','','www.citymd.com'),(50,'citymd','','United Srares','New York','New York','14 W 14th St, New York, NY 10011, United States','','+1 212 390 0558','',1,'MedPay',0,'','CityMD Urgent Care in Union Square',125.00,125.00,'','2016-02-01 11:59:17','','www.citymd.com'),(51,'citymd','','United States','New York','Brooklyn','418-420 5th Avenue, Brooklyn, NY 11215, United States','','+1 718 965 2273','',1,'MedPay',0,'','Park Slope urgent care center ',125.00,125.00,'','2016-02-01 15:37:41','','www.citymd.com'),(52,'citymd','','United States','New York','Brooklyn','288 Flatbush Avenue, Brooklyn, NY 11217, United States','','+1 718 656 1290','',1,'MedPay',0,'','CityMD Urgent Care Prospect Heights',125.00,125.00,'','2016-02-01 15:41:33','','www.citymd.com'),(53,'citymd','','United States','New York','Brooklyn','228-230 Court Street, Brooklyn, NY 11201, United States','','+1 718 280 5362','',1,'MedPay',0,'','Cobble Hill Urgent Care Center',125.00,125.00,'','2016-02-01 15:45:05','','www.citymd.com'),(54,'citymd','','United States','New York','New York','2398 Broadway, New York, NY 10024, United States','','+1 212 721 2111','',1,'MedPay',0,'','CityMD Urgent Care Upper West Side',125.00,125.00,'','2016-02-01 12:00:57','','www.citymd.com'),(55,'citymd','','United States','New York','New York','336 East 86th Street, New York, NY 10028, United States','','+1 212 772 3627','',1,'MedPay',0,'','CityMD Upper East Side Urgent Care',125.00,125.00,'','2016-02-01 12:04:55','','www.citymd.com'),(56,'Kawaratani & Shigematsu Mds','','United States','California','Los Angeles','420 E 3rd St, Los Angeles, CA 90013 ','','+1 213 628 1020','',1,'MedPay',0,'','Kawaratani & Shigematsu Mds',70.00,70.00,'+1 213 628 8780','2015-12-24 22:54:39','ste 705',''),(57,'Dr. James C. Feng DDS, Emergency Dentist of Los Angeles','','United States','California','Los Angeles','350 S Figueroa St, Los Angeles, CA 90071, United States','','+1 818 794 9251','',1,'MedPay',0,'','Dr. James C. Feng DDS, Emergency Dentist of Los Angeles',150.00,150.00,'','2015-12-24 23:13:16','world trade center suite 551\r\n24/7 service','http://www.bettersleepdentist.com'),(58,'Emergency Dentist NYC','','United States','New York','New York','100 E 12th St, New York, NY 10003, United States','','+1 646 336 8478','',1,'MedPay',0,'','Emergency Dentist NYC',0.00,0.00,'','2016-02-01 12:06:52','','http://emergencydentistnyc.com/index.html'),(59,'Rolf Marc ','Anner','Switzerland','','Geneva','Rue de Monthoux 64, 1201 Geneva, Switzerland','','+41 22  731 81 37  ','rolfanner@swissonline.ch',1,'MedPay',0,'','',100.00,0.00,'+ 22 741 16 36','2016-01-27 17:12:23','',''),(60,'Eric','Bierens de Haan ','Switzerland','','Geneva','Boulevard des Tranchees 44, 1206 Geneva, Switzerland','','+41 22 789 0494','ericbdh@hin.ch',1,'MedPay',1,'','',150.00,0.00,'+41 22 346 8549','2016-01-27 17:10:06','',''),(61,'Dieter','Kraft','Switzerland','','Geneva','Chemin Gilbert-Trolliet 2, 1209 Geneva, Switzerland','','+41 22 740 11 11  ','',1,'MedPay',1,'','',100.00,0.00,'+41 22 740 11 11','2016-01-27 17:19:35','',''),(62,'Centre Dentaire Lancy','','Switzerland','','Geneve','Chemin de la Caroline 18A, 1213 Lancy, Geneve, Switzerland','','+41 22 793 75 45','info@cdlancy.ch',1,'MedPay',0,'','Centre Dentaire Lancy',180.00,0.00,'+41 22 793 75 46','2016-01-27 17:26:23','','http://www.cdlancy.ch/index.php'),(63,' Knut ','Müller','Germany','','Freiburg','Engelbergerstraße 33, 79106, Freiburg, Germany','','+49 761 277881','info@hausarzt-freiburg.com',1,'MedPay',0,'','Dres. Knut Müller und Arne Bernd Müller',20.00,0.00,'+49 761 2025272 ','2016-01-27 13:53:21','',''),(64,'Arne ','Bernd Müller','Germany','','Freiburg','Engelbergerstraße 33, 79106, Freiburg, Germany','','+49 761 277881','info@hausarzt-freiburg.com',1,'MedPay',0,'','Dres. Knut Müller und Arne Bernd Müller',20.00,0.00,'+49 761  202 52 72','2016-01-27 13:56:34','',''),(65,'Marian',' Gregor Scheffczyk','Germany','','Freiburg','Bertoldstraße 45, 79098, Freiburg im Breisgau, Germany','','+49 761 16844',' praxis.dr.scheffczyk@web.de',1,'MedPay',0,'','',40.00,0.00,'+49 761 16824','2016-01-27 14:03:47','','http://praxis-dr-scheffczyk.de'),(66,'Ulrich ','Denz ','Germany','','Freiburg','Bertoldstraße 3, 79098, Freiburg im Breisgau, Germany','','+49 761 33300','praxis@hausaerzte-bertoldsbrunnen.de',1,'MedPay',0,'','Hausärzte am Bertoldsbrunnen',30.00,0.00,'+49 761 35053','2016-01-27 14:09:32','','http://www.hausaerzte-bertoldsbrunnen.de'),(67,'Peter ','Haas','Germany','','Freiburg','Bertoldstraße 3, 79098, Freiburg im Breisgau, Germany','','+49 761 33300','praxis@hausaerzte-bertoldsbrunnen.de',1,'MedPay',0,'','Hausärzte am Bertoldsbrunnen',30.00,0.00,'+49 761 35053','2016-01-27 14:14:11','','http://www.hausaerzte-bertoldsbrunnen.de'),(68,'Carola','Anders','Germany','','Berlin','Garzauer Straße 6, 12683, Berlin, Germany','','+49 30 5405456','',1,'MedPay',0,'','',50.00,0.00,'+49 30 56301329','2016-01-27 14:33:25','',''),(69,'Sylvia','Kollmann','Germany','','Berlin','Nassauische Straße 25, 10717, Wilmersdorf, Berlin, Germany','','+49 30 873 8303','',1,'MedPay',0,'','',50.00,0.00,'+49 30  861 0178','2016-01-27 14:39:30','','http://www.family-practice-berlin.de'),(70,'Allgemeinpraxis Berlin Mitte','','Germany','','Berlin','Kapweg 5, 13405, Berlin, Germany','','+49 30  45 13 039','info@allgemeinpraxisberlin.de ',1,'MedPay',0,'','Allgemeinpraxis Berlin Mitte',30.00,0.00,'+49 030  45 97 8279','2016-01-27 14:44:15','','http://allgemeinpraxisberlin.de'),(71,'Medeco Zahnklinik Berlin Wedding ','','Germany','','Berlin','Prinzenallee 89 /90, 13357, Berlin, Germany','','+49 30 494 000 0','',1,'MedPay',0,'','Medeco Zahnklinik Berlin Wedding ',0.00,0.00,'+49 030 494 000 74','2016-01-27 14:48:53','','http://www.medeco.de/zahnarzt-berlin/wedding/startseite'),(72,'Medeco Zahnklinik Potsdamer Platz','','Germany','','Berlin','Stresemannstraße 121, 10963, Berlin, Germany','','+49 30 23095960','',1,'MedPay',0,'','Medeco Zahnklinik Potsdamer Platz',0.00,0.00,'+49 30 230 95 96 74','2016-01-27 14:52:36','','http://www.medeco.de/en/treatment-centres/berlin/berlin-potsdamer-platz'),(73,'MEDECO Center Ars Dentalis München ','','Germany','','Munich','Giesinger Bahnhofplatz 7-9, 81539, Munich, Germany','','+49 89 38 666 290','',1,'MedPay',0,'','MEDECO Center Ars Dentalis München ',0.00,0.00,'+49 89 322 99 1829','2016-01-27 14:28:36','','http://arsdentalis.net'),(74,'Zahnarzt Zürich ZahnCity','','Switzerland','','Zurich','Freischützgasse 1, 8004 Zürich, Switzerland','','+41 43 317 17 39 ','info@zahncity.ch',1,'MedPay',0,'','Zahnarzt Zürich ZahnCity',120.00,0.00,'','2016-01-27 18:51:32','','http://www.zahncity.ch/en/contact'),(75,'Anlela','Caddick','Switzerland','','Zurich','Freischützgasse 1, 8004 Zürich, Switzerland','','+41 44 262 27 13','dr.angela.caddick@gmail.com',1,'MedPay',0,'','',45.00,0.00,'+44 262 7670','2016-01-27 18:59:07','','http://www.drangelacaddick.com'),(76,'Zahnärzte von Ziegler','','Switzerland','','Zurich','Mühlebachstrasse 90, 8008 Zürich, Switzerland','','+41 44 382 03 00','zahnaerzte@vonziegler.ch',1,'MedPay',0,'','Zahnärzte von Ziegler',0.00,0.00,'+41 44 382 03 60','2015-12-28 16:52:34','','http://www.vonziegler.ch'),(77,'Bernhard','Beck','Switzerland','','Zurich','Rämistrasse 3, 8001 Zürich, Switzerland','','+41 44 252 30 60','beck@tropdoc.ch',1,'MedPay',0,'','',60.00,0.00,'41 44 252 30 35','2016-01-27 19:05:35','','http://www.tropenreisemed.ch'),(78,'corinne','weper-Dällenbach','Switzerland','','Zurich','Minervastrasse 10, 8032 Zürich, Switzerland','','+41 44 252 16 44  ','praxisweber@hin.ch',1,'MedPay',0,'','',60.00,0.00,'+44 252 16 70','2016-01-27 19:13:09','','http://www.doktorweber.ch'),(79,'PERMANENCE HAUPTBAHNHOF','','Switzerland','','Zurich','Bahnhofplatz 15, 8001 Zürich, Switzerland','','+41 44 215 4444','',1,'MedPay',0,'','PERMANENCE HAUPTBAHNHOF',0.00,0.00,'+41 44 215 4445 ','2016-01-27 19:35:13','drop in clinic - ללא קביעת תור','http://www.permanence.ch'),(80,'Spital MEDAS','','Romania','','Bucharest','Strada Doctor Iacob Felix 83a, Bucharest, Romania','','+40 213 219232','',1,'MedPay',0,'','Spital MEDAS',0.00,0.00,'','2016-01-28 16:37:51','Sector 1','http://www.med-as.ro/servicii-pacienti/contact/spital-medas'),(81,'DentalMed','','Romania','','Bucharest','102 Calea 13 Septembrie, Bucharest, Romania','+40 726 770 357','+40 21 402 8735','contact@dental-med.ro',1,'MedPay',0,'','DentalMed',300.00,0.00,'+40 31 816 4251','2016-01-28 16:44:33','Bldg. 48A\r\nNext to JW Marriott Hotel','http://www.dental-med.ro'),(82,'BIO MEDICA Floreasca','','Romania','','Bucharest','111-113 Calea Floreasca, Sector 1, Bucharest, Romania','','+40 21 311 77 93','office@bio-medica.ro',1,'MedPay',0,'','BIO MEDICA Floreasca',0.00,0.00,'+40 21  311 77 98','2016-01-28 16:48:50','','http://www.bio-medica.ro/en'),(83,'Centre de Santé Haussmann','','France','','Paris','2 Boulevard Haussmann, 75009 Paris, France','','+33 1 48 00 24 00','',1,'MedPay',0,'','Centre de Santé Haussmann',0.00,0.00,'+33 1 48 00 24 49','2016-01-31 11:31:51','','http://www.centrehaussmann.com'),(84,'Dan','Belhassen','France','','Paris','73 Boulevard du Montparnasse, 75006 Paris, France','','+33 01 45 49 28 11','',1,'MedPay',0,'','',60.00,0.00,'','2016-01-31 11:36:51','','https://www.doctolib.fr/medecin-generaliste/paris/dan-belhassen'),(85,'Centre de Santé MIROMESNIL','','France','','Paris','6 Avenue César Caire, 75008 Paris','','+33 1 55 56 62 50','',1,'MedPay',0,'','Centre de Santé MIROMESNIL',0.00,0.00,'','2016-01-31 11:52:49','','http://www.centre-medical-miromesnil.com/index.php'),(86,'Centre Médical International Saint-Germain','','France','','Paris','72 Boulevard Saint-Germain, 75005 Paris, France','','+33 1 45 45 27 94','',1,'MedPay',0,'','Centre Médical International Saint-Germain',30.00,0.00,'','2016-01-31 11:57:03','','https://www.doctolib.fr/cabinet-medical/paris/centre-medical-international'),(87,'Centre Médical et Dentaire Etoile ','','France','','Paris','12 Avenue de la Grande Armée, 75017 Paris, France','','+33 01 43 80 48 07','',1,'MedPay',0,'','Centre Médical et Dentaire Etoile ',0.00,0.00,'+33 01 45 72 56 05','2016-01-31 12:14:05','','http://www.cmetoile.fr'),(88,'Sanacare Gruppenpraxis','','Switzerland','','Bern','Bubenbergplatz 10, 3011 Bern, Switzerland','','+41 31 385 71 11  ','bern@sanacare.ch',1,'MedPay',0,'','Sanacare Gruppenpraxis',0.00,0.00,'+31 382 92 33','2016-01-28 15:49:15','','http://www.sanacare.ch/htm/praxen.htm?kontakt&navid=1&pid=3'),(89,'zahnarztzentrum','','Switzerland','','Bern','Bahnhofplatz 10A, 3011 Bern, Switzerland','','+41 31 326 70 00 ','bern@zahnarztzentrum.ch',1,'MedPay',0,'','zahnarztzentrum',0.00,0.00,'+31 326 70 01 ','2016-01-28 15:56:09','','zahnarztzentrum.ch '),(90,'Zahnarztzentrum','','Switzerland','','Basel','Dufourstrasse 49, 4052 Basel, Switzerland','','+41 61 276 90 00 ','basel.aesch@zahnarztzentrum.ch',1,'MedPay',0,'','Zahnarztzentrum',0.00,0.00,'+41 61 276 90 13 ','2016-01-28 15:21:13','','http://zahnarztzentrum.ch/en/dentist-basel.html'),(91,'Zahnarztzentrum','','Switzerland','','Basel','Centralbahnstrasse 11, 4051 Basel, Switzerland','','+41 61 228 11 00 ','basel.bhf@zahnarztzentrum.ch',1,'MedPay',0,'','Zahnarztzentrum',0.00,0.00,'','2016-01-28 15:27:59','','zahnarztzentrum.ch'),(92,'MEDIX TOUJOURS','','Switzerland','','Basel','Centralbahnstrasse 3, 4051 Basel, Switzerland','','+41 61 500 11 00','basel@medix-toujours.ch',1,'MedPay',0,'','MEDIX TOUJOURS',0.00,0.00,'+41 61 500 11 01','2016-01-28 15:33:01','','http://www.medix-toujours.ch/en/'),(93,'Beat','Hornstein','Switzerland','','Basel','Steinenvorstadt 26, 4051 Basel, Switzerland','','+41 61 281 71 50','beat.hornstein@hin.ch',1,'MedPay',0,'','',0.00,0.00,'+41 61 281 71 56','2016-01-28 15:37:03','',''),(94,'Hugo','Gold','Austria','','Vienna','Taborstraße 59 1020 Vienna, Austria','','+43 1 21 63 949','praxis@hugogold.at',1,'MedPay',0,'','',20.00,0.00,'','2016-01-31 20:18:37','Taborstrasse 59/ E/ 7','http://www.hugogold.at'),(95,'CHRISTIANE','Brunner','Austria','','Vienna','Esterhazygasse 35 1060 Vienna, Austria','+43 676 88088886','','termin@drbrunner.at',1,'MedPay',1,'','',30.00,0.00,'+43 1 9081625','2016-01-31 20:27:19','','http://www.drbrunner.at/'),(96,'Franz','Mayrhofer','Austria','','Vienna','Mariahilfer Straße 95 1060 Vienna, Austria','','+43 1 5974337','praxis@medizinmariahilf.at',1,'MedPay',0,'','Medizin Mariahilf',50.00,0.00,'+43 1 5974337-5','2016-01-31 20:31:35','','http://medizinmariahilf.at/index.html#'),(97,'Wolfgang ','Mückstein','Austria','','Vienna','Mariahilfer Straße 95 1060 Vienna, Austria','','+43 1 59 74 337','praxis@medizinmariahilf.at',1,'MedPay',0,'','Medizin Mariahilf',50.00,0.00,' +43 1 59 74 337 5 ','2016-01-31 20:35:48','','http://medizinmariahilf.at/index.html'),(98,'Fabienne ','Lamel','Austria','','Vienna','Mariahilfer Straße 95 1060 Vienna, Austria','','+43 1 59 74 337','praxis@medizinmariahilf.at',1,'MedPay',0,'','Medizin Mariahilf',50.00,0.00,'+43 1 59 74 337 5','2016-01-31 20:43:04','','http://medizinmariahilf.at/index.html#'),(99,'Andreas','Werner','Austria','','Vienna','Seilerstätte 11/5 1010 Vienna, Austria','','+43 1  512 55 66','praxis@drwerner.at',1,'MedPay',0,'','',0.00,0.00,'','2016-01-31 20:48:09','','http://drwerner.at/'),(100,'Martin','Schiffl','Austria','','Salzburg','Aigner Straße 21 5026 Salzburg, Austria','','+43 662 84 45 50','ordination@blen-dent.at',1,'MedPay',0,'','Zahnarzt Salzburg Stadt | Dr. Martin Schiffl',0.00,0.00,'','2016-01-31 20:54:01','','http://www.blen-dent.at/'),(101,'Maria','Kohler','Austria','','Salzburg','Schrannengasse 11 5020 Salzburg, Austria','','+43 662 877602','',1,'MedPay',0,'','',60.00,0.00,'+43 662 872485','2016-01-31 21:00:11','',''),(102,'Regine','Stoll-Ettenauer','Austria','','Salzburg','Guggenmoosstraße 41 5020 Salzburg, Austria','','+43 662 43 33 91','',1,'MedPay',0,'','',0.00,0.00,'+43 662 422723','2016-01-31 21:03:51','',''),(103,'unidad medica','','Spain','','Madrid','Calle Conde de Aranda, 1, 28001 Madrid','','+34 914 35 18 23','um@unidadmedica.com',1,'MedPay',1,'','unidad medica',0.00,0.00,'','2016-01-27 09:34:25','24/24 house calls','http://www.unidadmedica.com/en/'),(104,'Ruben','Borras','Spain','','Madrid','Calle de Padilla, 20, 28006 Madrid, Spain','','+34 915 75 98 34','',1,'MedPay',1,'','',46.00,0.00,'','2016-01-27 09:39:40','',''),(105,' Franzreb ','Cobelletti','Spain','','Madrid','Pº Castellana 171 bajo izquierda, 28046 Madrid, Spain ','','+34 914 49 19 57','info@drmarcofranzreb.com',1,'MedPay',1,'28/5223','',80.00,0.00,'','2016-01-27 09:49:17','','http://www.drmarcofranzreb.com/'),(106,'Camilo ','Albare','Spain','','Madrid','Calle de Montesa, 18, 28006 Madrid, Spain','','+34 914 02 78 98','',1,'MedPay',0,'','Centro Medico Radiologico',50.00,0.00,'','2016-01-27 10:17:29','',''),(107,'Mario Eugenio ','Marchio','Spain','','Madrid','Calle de Núñez de Balboa, 28, 28001 Madrid, Spain','','+34 914 358 166','',1,'MedPay',0,'','',0.00,0.00,'','2016-01-27 10:35:26','',''),(108,'Creta Interclinic','','Greece','','Iraklio','Leof. Minoos 63, Iraklio, 713 04, Greece','','+30 281 037 3800','administration@cic.gr',1,'MedPay',1,'','Creta Interclinic',140.00,0.00,'+30 2810 371206','2016-01-31 23:10:42','','http://www.cic.gr/en/'),(109,'Doral Urgent Care Medical Center','','United States','Florida','Miami','9065 SW 87th Ave, Miami, FL 33176, United States','','+1 305 595 9880','',1,'MedPay',0,'','Doral Urgent Care Medical Center',20.00,0.00,'+1 305-595-9884','2016-02-01 17:39:34','','https://www.urgentcarelocations.com/'),(110,'FastCare Miami Beach','','United States','Florida','Miami Beach','825 Arthur Godfrey Rd, Miami Beach, FL 33140, United States','','+1  786 923 4000','fastcare@myfastcare.com',1,'MedPay',0,'','FastCare Miami Beach',135.00,135.00,'+1 786 472 30 34','2016-02-01 17:47:45','','http://www.myfastcare.com/'),(111,'Acevedo Medical Group','','United States','Florida','Miami','2525 NW 54th St, Miami, FL 33142, United States','','+1 305 633 9090','contact@acevedomedicalgroup.com',1,'MedPay',0,'','Acevedo Medical Group',60.00,60.00,'+1 305 633 9383','2016-02-01 17:55:32','','http://www.acevedomedicalgroup.com/'),(112,'Miami Urgent Care Center','','United States','Florida','Miami','2645 South Douglas Road, Miami, FL 33133, United States','','+1  305 448 8134','',1,'MedPay',0,'','Miami Urgent Care Center',135.00,135.00,'','2016-02-01 18:01:42','SUITE 502','www.miamiurgentcare.com'),(113,'Mauricio','Tijerino','United States','Florida','Miami Beach','1111 Lincoln Rd, Miami Beach, FL 33139, United States','','+1 305 851 3997','',1,'MedPay',0,'','',0.00,0.00,'','2016-02-01 18:06:20','SUITE 740',''),(114,'RiteCare Urgent Care Medical Center - Brickell','','United States','Florida','Miami','1250 South Miami Avenue, Miami, FL 33130, United States','','+1  305 513 3818','',1,'MedPay',0,'','RiteCare Urgent Care Medical Center - Brickell',100.00,100.00,'+1  305  571 6251','2016-02-01 18:41:17','','http://www.ritecaremc.com/brickell/'),(115,'RiteCare Urgent Care Medical Center - The Falls / S. Miami','','United States','Florida','Miami','14201 S Dixie Hwy, Miami, FL 33176, United States','','+1 305 513 3818','',1,'MedPay',0,'','RiteCare Urgent Care Medical Center - The Falls / S. Miami',100.00,100.00,'+1 786 242  3982','2016-02-01 18:51:41','','http://www.ritecaremc.com/the-falls/'),(116,'Fox MedicaI Center - Downtown','','United States','Florida','Miami','606 West Flagler Street, Miami, FL 33130, United States','','+1 305 545 9292','info@foxmedicalcenters.com',1,'MedPay',0,'','Fox MedicaI Center - Downtown',100.00,100.00,'','2016-02-01 18:59:26','','http://www.foxmedicalcenters.com/'),(117,'Fox MedicaI Center - Westchester','','United States','Florida','Miami','6090 Bird Road, Miami, FL 33155, United States','','+1 305 661 6336','info@foxmedicalcenters.com',1,'MedPay',0,'','Fox MedicaI Center - Westchester',100.00,100.00,'','2016-02-01 19:05:40','','http://www.foxmedicalcenters.com/'),(118,'Fox Medical Center - Kendall','','United States','Florida','Miami','10860 SW 88th St, Miami, FL 33176, United States','','+1 305 595 1300','info@foxmedicalcenters.com',1,'MedPay',0,'','Fox Medical Center - Kendall',100.00,100.00,'','2016-02-01 19:13:55','','http://www.foxmedicalcenters.com/'),(119,'Victor','De Leon','United States','Nevada','Las Vegas','3131 W Charleston Blvd, Las Vegas, NV 89102, United States','','+1 702 878 2801','',1,'MedPay',0,'','',80.00,80.00,'+1 702 877 6711','2016-02-01 20:00:15','suite 150 ',''),(120,'Janie','Kwak-Tran','United States','Nevada','Las Vegas','5052 S Jones Blvd, Las Vegas, NV 89118, United States','','+1 702  220-9611','',1,'MedPay',0,'','',115.00,115.00,'','2016-02-01 20:06:31','suite 105',''),(121,'Janie','Kwak-Tran','United States','Nevada','Las Vegas','2725 S Jones Blvd, Las Vegas, NV 89146, United States','','+1 702 703 6863','',1,'MedPay',0,'','',115.00,115.00,'+1 702 220 9163 ','2016-02-01 20:09:36','suite 107',''),(122,'Evan','Allen','United States','Nevada','Henderson','1701 N Green Valley Pkwy, Henderson, NV 89074, United States','','+1 702 541 8240','',1,'MedPay',0,'','Total Care Family Practice',200.00,200.00,'+1 702 541 8241 ','2016-02-01 20:12:42','suite 5C','http://www.totalcarefp.com/'),(123,'Amir','Nickname','United States','Nevada','Las Vegas','1712 Bearden Dr, Las Vegas, NV 89106, United States','','+1 702 835 9760','',1,'MedPay',0,'','Southern Nevada Family Medicine',80.00,80.00,'+1 702 852 0580','2016-02-01 20:16:26','',''),(124,'Canyon  Medical Center','','United States','Nevada','Las Vegas','5061 North Rainbow Boulevard, Las Vegas, NV 89130, United States','','+1 702 220 8001','officestaff@canyonmedicalcenterlv.com',1,'MedPay',0,'','Canyon  Medical Center',100.00,100.00,'+1 702 395 4500','2016-02-01 20:19:43','SUITE 180','www.canyonmedicalcenterlv.com/'),(125,'Urgent Care Extra','','United States','Nevada','Las Vegas','8300 W Cheyenne Ave, Las Vegas, NV 89129, United States','','+1 702 656 0911','',1,'MedPay',0,'','Urgent Care Extra',125.00,125.00,'+1 702-656-0113','2016-02-01 20:24:34','SUITE 106','https://www.urgentcareextra.com/'),(126,'Quick Care in Las Vegas','','United States','Nevada','Las Vegas','3111 S Maryland Pkwy, Las Vegas, NV 89109, United States','','+1 702 732 7407','info@quickcarelv.com',1,'MedPay',0,'','Quick Care in Las Vegas',100.00,100.00,'','2016-02-01 20:26:44','',''),(127,'Urgent Care Extra','','United States','Nevada','Las Vegas','4575 West Charleston Boulevard, Las Vegas, NV 89102, United States','','+1 702 877 8777','',1,'MedPay',0,'','Urgent Care Extra',125.00,125.00,'+1 702 877 8770','2016-02-01 20:30:36','','www.urgentcareextra.com/'),(128,'Access Emergency Dental Care ','','United States','Nevada','Las Vegas','2585 S Jones Blvd, Las Vegas, NV 89146','','+1 702 319 4734 ','dentalcarelasvegas@gmail.com ',1,'MedPay',0,'','Access Emergency Dental Care ',0.00,0.00,'','2016-02-01 20:33:58','','http://dentalemergencies.com/index.html'),(129,'Las Vegas Urgent Care','','United States','Nevada','Las Vegas','2901 N Tenaya Way, Las Vegas, NV 89128, United States','','+1  702 852 2000‏','quick@lvurgentcare.com',1,'MedPay',0,'','Las Vegas Urgent Care',90.00,90.00,'','2016-02-01 20:38:40','','http://lasvegasurgentcare.com/'),(130,'Buena Vista Urgent Care','','United States','Florida','Orlando','8216 World Center Drive, Orlando, FL 32821, United States','','+1 407 465 1110 ','buenavistauc@gmail.com',1,'MedPay',0,'','Buena Vista Urgent Care',220.00,220.00,'+ 1 407 465 1222 ','2016-02-01 22:17:08','','http://www.buenavistaurgentcare.com/'),(131,'Emergy-care','','United States','Florida','Orlando','4800 S Apopka Vineland Rd, Orlando, FL 32819, United States','','+1 407 876 5555','',1,'MedPay',0,'','Emergy-care',150.00,150.00,'','2016-02-01 22:19:09','','http://www.jainemergicare.com/'),(132,'Paramount Urgent Care','','United States','Florida','Orlando','8972 Turkey Lake Road, Orlando, FL 32819, United States','','+1 407 226 1906','',1,'MedPay',0,'','Paramount Urgent Care',120.00,120.00,'','2016-02-01 22:21:11','','http://paramounturgentcare.com/'),(133,'Paramount Urgent Care','','United States','Florida','Oviedo','1984 Alafaya Trail, Oviedo, FL 32765, United States','','+1 407 542 0346','',1,'MedPay',0,'','Paramount Urgent Care',120.00,120.00,'','2016-02-01 21:59:35','','http://paramounturgentcare.com/'),(134,'Paramount Urgent Care','','United States','Florida','Clermont','628 S Hwy 27, Clermont, Florida 34714, United States','','+1 352 242 1988','',1,'MedPay',0,'','Paramount Urgent Care',120.00,120.00,'','2016-02-01 22:44:53','','http://paramounturgentcare.com/'),(135,'urgentmedcare','','United States','Florida','Orlando','716 S Goldenrod Rd, Orlando, FL 32822, United States','','+1 407 658 1719','',1,'MedPay',0,'','urgentmedcare',115.00,115.00,'','2016-02-01 22:46:37','',''),(136,'USA Medical Care','','United States','Florida','Orlando','3900 S Goldenrod Rd, Orlando, FL 32822, United States','','+1 407 985 3916 ','',1,'MedPay',0,'','USA Medical Care',59.00,59.00,'','2016-02-01 22:48:29','SUITE 142','http://www.usamedcare.com/'),(137,'Hunter\'s Creek Dental ','','United States','Florida','Orlando','3924 Town Center Blvd, Orlando, FL 32837, United States','','+1 407 966 4782 ','',1,'MedPay',0,'','Hunter\'s Creek Dental ',0.00,0.00,'','2016-02-01 23:00:38','','http://www.hunterscreekdental.com/'),(138,'Jose Maria ','Boada Gil','Spain','','Barcelona','Carrer de Numància, 4, 08029 Barcelona, Espanya','','+34 934 30 18 36','boadagil@ono.com',1,'MedPay',0,'','',30.00,0.00,'','2016-01-11 12:44:49','',''),(139,'The British Dental Clinic','','Spain','','Barcelona','Av. Diagonal, 281, 08013 Barcelona, Espanya','','+34 932 65 80 70','info@thebritishdentalclinic.com',1,'MedPay',0,'','The British Dental Clinic',0.00,0.00,'','2016-01-27 09:06:52','','http://www.thebritishdentalclinic.com/'),(140,'Centre d’Urgència Perecamps','','Spain','','Barcelona','Avinguda de les Drassanes, 13 - 15, 08001 Barcelona, Espanya','','+34 934 410 600','',1,'MedPay',0,'','Centre d’Urgència Perecamps',30.00,0.00,'','2016-01-27 09:12:23','',''),(141,'Centre Aribau','','Spain','','Barcelona','Carrer d\'Aribau, 168, 08036 Barcelona, Espanya','','+34 932 810 000','',1,'MedPay',0,'','Centre Aribau',30.00,0.00,'','2016-01-27 09:16:47','',''),(142,'Schuman Medical Practice','','Belgium','','Bruxelles','Boulevard Charlemagne 37 1000 Bruxelles, Belgique','','+32  2 735 6694 ','englishdoctorbrussels@gmail.com',1,'MedPay',0,'','Schuman Medical Practice',35.00,0.00,'','2016-01-31 08:45:37','','http://englishdoctorbrussels.com/wp/contact/'),(143,'Jean- Paul','Robberechts ','Belgium','','Brussel','Priemstraat 33 1000 Brussel, Belgium','','+32 2 513 49 75','j.robberechts@skynet.be',1,'MedPay',0,'','',50.00,0.00,'+32 2 5141474','2016-01-31 08:52:18','',''),(144,'Van Breusegem – Sharafedin','','Belgium','','Brussel','Papenvest 10 1000 Brussel, Belgium','','+32 2 502 14 07','doktervanbreusegem@skynet.be',1,'MedPay',0,'','Van Breusegem – Sharafedin',50.00,0.00,'+32 2 5141474','2016-01-31 09:03:52','',''),(145,'Kurek-Dental-Clinic ','','Belgium','','Bruxelles','Avenue Winston Churchill 240 1180 Bruxelles, Belgique','','+32 2 3461144','',1,'MedPay',0,'','Kurek-Dental-Clinic ',0.00,0.00,'','2016-01-31 08:58:59','','https://kurekdentistebruxelles.wordpress.com/'),(146,'Wilanów Family Practice ','','Poland','','Warsaw','Kosiarzy 37 02-953 Warszawa, Polska','+48 602 26 88 26','+48 22 642 74 04 ','andrzej.gajer@wilanowfamilypractice.pl    ',1,'MedPay',1,'','Wilanów Family Practice ',150.00,0.00,'+48 22 379 77 27','2016-01-31 22:44:47','','http://www.wilanowfamilypractice.pl/index.php?lang=eng'),(147,'Praktyka Rodzinna Healthcare Niepubliczny Zakład Opieki Zdrowotnej','','Poland','','Warsaw','Chorągwi Pancernej 50 02-951 Warszawa, Polska','','+48 22 858 0101         ','healthcare@medionet.pl',1,'MedPay',0,'','Praktyka Rodzinna Healthcare Niepubliczny Zakład Opieki Zdrowotnej',220.00,0.00,'+48 22 651 7473','2016-01-31 22:48:45','',''),(148,'Michalina','Bou-Matar','Poland','','Warsaw','skwer Kardynała Stefana Wyszyńskiego 5 01-001 Warsaw, Polska','+48 792 737 739','','ppl.mboumatar@gmail.com',1,'MedPay',0,'','',150.00,0.00,'','2016-01-31 22:53:09','','http://familydoctor.pl/'),(149,'Dental Excellence','','Poland','','Warsaw','Aleje Jerozolimskie 56C 00-801 Warszawa, Polska','','+48  22 242 84 86','info@dentalexcellence.pl',1,'MedPay',0,'','Dental Excellence',100.00,0.00,'+48 22 242 89 97','2016-01-31 22:56:54','','http://en.dentalexcellence.pl/OKlinice.aspx'),(150,'Aventino Medical Group','','Italy','','Rome','Via Sant\'Alberto Magno, 5, Rome, 00153 RM, Italia','','+39 6 5780738','info@aventinomedicalgroup.com',1,'MedPay',0,'','Aventino Medical Group',100.00,0.00,'+39 6 5780738','2016-01-31 22:11:46','','http://www.aventinomedicalgroup.com/index.htm'),(151,'Michael ','Schmitz','Italy','','Rome','Via Tirso, 6, Roma, 00198 RM, Italia','','+39  6 855 5852','studio@drschmitz.it',1,'MedPay',0,'','Studio dentistico Dr. Schmitz',0.00,0.00,'','2016-01-31 22:17:18','','http://drschmitz.it/'),(152,'doctors in Italy','','Italy','','Rome','Via Frattina, 48, Roma, 00187 RM, Italia','','+ 39 6 6790695','info@doctorsinitaly.com',1,'MedPay',0,'','doctors in Italy',100.00,0.00,'','2016-01-31 22:22:35','3rd floor','https://www.doctorsinitaly.com/contacts/'),(153,'MEDinACTION','','Italy','','Rome','The Center of Rome, Via Cola di Rienzo, Roma, RM, Italia','+39  320 4065709','+39 347 746 2066','andrea.guerriero@medinaction.com',1,'MedPay',1,'','MEDinACTION',90.00,0.00,'','2016-01-31 22:31:27','house calls','http://www.medinaction.com/'),(154,'Downtown Dental Studio 24','','United States','Massachusetts','Boston','24 School St, Boston, MA 02108, United States','','+1 617 426 8029',' info@downtowndentalstudio24.com',1,'MedPay',0,'','Downtown Dental Studio 24',0.00,0.00,'+1 617 542 8970','2016-02-06 16:03:23','8th Floor','http://downtowndentalstudio24.com/'),(155,'Urgent Care Walk In Med Clinic','','United States','Massachusetts','Boston','581 Boylston St, Boston, MA 02116, United States','','+1 617 247-1400','',1,'MedPay',0,'','Urgent Care Walk In Med Clinic',0.00,0.00,'','2016-02-06 16:46:31','Suite 800',''),(156,'AFC Doctors Express Urgent Care Watertown','','United States','Massachusetts','Watertown','376 Arsenal Street, Watertown, MA 02472, United States','','+1  617 923 2273','watertownteam@afcurgentcare.com',1,'MedPay',0,'','AFC Doctors Express Urgent Care Watertown',169.00,169.00,'','2016-02-06 16:50:18','','watertownteam@afcurgentcare.com'),(157,'East Boston Neighborhood Health Center','','United States','Massachusetts','Boston','10 Gove St, Boston, MA 02128, United States','','+1 617-569-5800','',1,'MedPay',0,'','East Boston Neighborhood Health Center',0.00,0.00,'','2016-02-06 16:53:20','','http://www.ebnhc.org/en/'),(158,'Fenway Health','','United States','Massachusetts','Boston','1340 Boylston St, Boston, MA 02215, United States','','+1 617 927 6000','',1,'MedPay',0,'','Fenway Health',0.00,0.00,'','2016-02-06 16:55:24','Ansin Building','http://fenwayhealth.org/'),(159,'Fenway Health','','United States','Massachusetts','Boston','142 Berkeley Street, Boston, MA 02116, United States','','+1 617 247 7555','',1,'MedPay',0,'','Fenway Health ',0.00,0.00,'','2016-02-06 16:57:49','South End','http://fenwayhealth.org/'),(160,'Codman Square Health Center','','United States','Massachusetts','Boston','637 Washington St, Boston, MA 02124, United States','','+1 617-825-9660','',1,'MedPay',0,'','Codman Square Health Center',0.00,0.00,'','2016-02-06 17:00:11','','http://www.codman.org/contact.html'),(161,'Eisner Pediatric & Family Medical Center','','United States','California','Los Angeles','1530 S Olive St, Los Angeles, CA 90015, United States','','+1 213 747 5542 ','',1,'MedPay',0,'','Eisner Pediatric & Family Medical Center',135.00,135.00,'+1213 746-9379','2016-02-06 17:21:05','','http://www.pedcenter.org/about-us.html'),(162,'Emergency Dentist of Los Angeles','','United States','California','Los Angeles','350 S Figueroa St, Los Angeles, CA 90071, United States','','+1 818 794 9251','',1,'MedPay',0,'','Emergency Dentist of Los Angeles',250.00,250.00,'','2016-02-06 17:23:10','suite 551 world trade center','http://www.bettersleepdentist.com/index.html'),(163,'Hollywood Walk-in Clinic','','United States','California','Hollywood','6430 Selma Avenue, Hollywood, CA 90028, United States','','+1 323 848 4522','info@hollywoodclinic.net',1,'MedPay',0,'','Hollywood Walk-in Clinic',79.00,79.00,'','2016-02-06 17:26:53','','http://hollywoodurgentcare.snappages.com/'),(164,'Eric','Rafoth','Czech Republic','','Prague','Pštrossova 10, Nové Město, Praha-Praha 1, Česká republika','','+420 7735 05773','info@americandentist.cz',1,'MedPay',0,'','American Dentist in Prague',0.00,0.00,'+420 221 595 013','2016-02-07 08:58:46','','www.americandentist.cz  '),(165,'Ivana','Grohova','Czech Republic','','Prague','Sokolovská 57/40, Praha-Praha 8, Česká republika','','+420 2248 14758','doctor.groh@seznam.cz',1,'MedPay',0,'','',0.00,0.00,'','2016-02-07 09:07:28','','http://www.doctor-prague.com/'),(166,'Vladimir',' Kraus','Czech Republic','','Prague','Učňovská 1, Hrdlořezy, Praha-Praha 9, Česká republika','','+420 2661 06201','info@drkraus.com',1,'MedPay',0,'','',0.00,0.00,'','2016-02-07 09:53:09','','http://www.drkraus.com/page.php?cont=home2'),(167,'Canadian Medical Care','','Czech Republic','','Prague','Veleslavínská 14, Praha-Praha 6, Czech Republic','','+420 2353 60133','cmc@cmcpraha.cz',1,'MedPay',0,'','Canadian Medical Care',0.00,0.00,'+420 235 364 640','2016-02-07 10:01:49','','http://www.cmcpraha.cz/en-US'),(168,'Dental Office H33','','Czech Republic','','Prague','Hvězdova 33, Nusle, Praha-Praha 4, Česká republika','','+420 7337 37337','info@dental-office.cz',1,'MedPay',0,'','Dental Office H33',0.00,0.00,'','2016-02-07 10:09:31','','http://www.dental-office.cz/en/'),(169,'Health Centre Prague','','Czech Republic','','Prague','Vodičkova 28, Nové Město, Praha-Prague 1, Česká republika','','+420 603 433 833','info@doctor-prague.cz',1,'MedPay',0,'','Health Centre Prague',0.00,0.00,'','2016-02-07 10:16:05','3rd SCHODISTE (entrance), 2nd Floor','http://www.doctor-prague.cz/hebrew.html'),(170,' Sarvenaz  ','S. Mobasser','United States','California','Los Angeles','204 E Pico Blvd, Los Angeles, CA 90015, United States','','+1  213 748 8111','',1,'MedPay',0,'',' Angeles Medical Clinic',100.00,100.00,'+1 213 457 6000','2016-02-15 22:55:33','','http://angelesmedicalclinic.com/'),(171,'Brentview Medical Urgent Care  ','','United States','California','West Hollywood','8264 Santa Monica Blvd, West Hollywood, CA 90046, United States','','+1 323 522 2222','info@BrentviewMedical.com',1,'MedPay',0,'','Brentview Medical Urgent Care  ',125.00,125.00,'+1 323 654 2221','2016-02-15 23:00:42','','http://www.brentviewmedical.com/'),(172,'Brentview Medical Urgent Care','','United States','California','Los Angeles','11611 San Vicente Boulevard, Los Angeles, CA 90049, United States','','+1 310 820 0013 ','info@BrentviewMedical.com',1,'MedPay',0,'','Brentview Medical Urgent Care',125.00,125.00,'+1 310 207 2630','2016-02-15 23:14:53','Ground Floor','http://www.brentviewmedical.com/'),(173,'Sunset Walk-In Healthcare and Occupational Medicine Clinic','','United States','California','West Hollywood','9201 W Sunset Blvd, West Hollywood, CA 90069, United States','','+1 310 273 1155','sunsetwalkinhealthcare@gmail.com',1,'MedPay',0,'','Sunset Walk-In Healthcare and Occupational Medicine Clinic',110.00,110.00,'','2016-02-15 23:23:38','suite M-155','http://sunsetwalk-inhealthcare.com/'),(174,'Santa Monica Urgent Care ','','United States','California','Santa Monica','524 Colorado Avenue, Santa Monica, CA 90401, United States','','+1 310 394 2273','',1,'MedPay',0,'','Santa Monica Urgent Care ',125.00,125.00,'+1 310 394 9700','2016-02-15 23:33:40','','http://www.santamonicauc.com/'),(175,'Beijing United Family Hospital','','China','','Beijing','2 Jiangtai Road, Chaoyang District, Beijing 100015, China','','+86 10 5927 7000','patientservices@ufh.com.cn',1,'MedPay',0,'','Beijing United Family Hospital',1670.00,0.00,'','2016-02-29 20:27:55','','http://beijing.ufh.com.cn/locations?lang=en'),(176,'United Family Shunyi Clinic','','China','','Beijing','Pinnacle Plaza, Yu Yang Lu, Shunyi Qu, Beijing Shi, סין','','+86 10 8046 5432','',1,'MedPay',0,'','United Family Shunyi Clinic',1700.00,0.00,'','2016-02-29 20:36:21','Pinnacle Plaza unit # 806',''),(177,'United Family Shunyi Dental Clinic','','China','','Beijing','Pinnacle Plaza, Yu Yang Lu, Shunyi Qu, Beijing Shi, סין','','+86 10 8046 1102','',1,'MedPay',0,'','United Family Shunyi Dental Clinic',0.00,0.00,'','2016-02-29 20:39:52','Pinnacle Plaza unit # 818',''),(178,'Vista Medical Center','','China','','Beijing','1 Guanghua Road, Chaoyang District, Beijing 100020, China','','+86 10 8529 6618','vista@vista-china.net',1,'MedPay',0,'','Vista Medical Center',660.00,0.00,'+86 10 8529 6615','2016-02-29 20:46:25','Level 3 Kerry Centre Shopping Mall','www.vista-china.net'),(179,'Bayley & Jackson Medical Center ','','China','','Beijing','7 Ri Tan Dong Lu, Chaoyang Qu, Beijing Shi, בייג\'ינג 100020, סין','','+86 10 8562 9998','bjmc@ikang.com',1,'MedPay',0,'','Bayley & Jackson Medical Center ',500.00,0.00,'+86 10 8561 4866','2016-02-29 20:58:53','','http://www.bjhealthcare.com/index.html'),(180,'United Family Guangqumen Clinic','','China','','Beijing','39 Guangqu Road, Chaoyang District, Beijing 100022, China','','+86 10 6779 7518','',1,'MedPay',0,'','United Family Guangqumen Clinic',1670.00,0.00,'+86 10 6771 7768','2016-02-29 21:17:42','2nd Floor, Handu International Building','http://beijing.ufh.com.cn/locations?lang=en'),(181,'United Family CBD Clinic','','China','','Beijing','6 Chaowai South Street, Chaoyang, Beijing 100020, China','','+86 10 5907 1266','',1,'MedPay',0,'','United Family CBD Clinic',1670.00,0.00,'','2016-02-29 21:21:35','Vantone Center, Suite 3017, Building AB','http://beijing.ufh.com.cn/locations?lang=en'),(182,'Bayley & Jackson Dental Surgeons ','','Hong Kong','','Hong Kong','Jardine House, Connaught Place, 1, Hong Kong','','+850 2526 1061','cs@bjdental.com',1,'MedPay',0,'','Bayley & Jackson Dental Surgeons ',0.00,0.00,'','2016-03-01 15:51:55','Podium Level, 2/F, 210-218 ','http://www.bjd...om/index.html'),(183,'Premier Medical Centre','','Hong Kong','','Hong Kong','Central Building, Pedder St, 1, Hong Kong','','+852 3651 1733 ','pmc@premiermedical.com.hk',1,'MedPay',0,'','Premier Medical Centre',900.00,0.00,'','2016-03-01 17:04:50','718-733 Central Building','http://www.premiermedical.com.hk/'),(184,'Premier Medical Centre','','Hong Kong','','Kowloon','Nathan Road, 26, Tsim Sha Tsui, Hong Kong, Kowloon','','+1 852 2724 1001','pmc-tst@premiermedical.com.hk',1,'MedPay',0,'','Premier Medical Centre Kowloon',900.00,0.00,'+1  852 2369 0969','2016-03-01 17:11:23','Suite 1001','http://www.premiermedical.com.hk/'),(185,'OT & P Healthcare','','Hong Kong','','Hong Kong','1501, Shui On Centre 8 Harbour Road, Wanchai, HK','','+ 852 2824 9112   ','',1,'MedPay',0,'','OT & P Healthcare Wanchai',900.00,0.00,'+  852 2824 6123','2016-03-01 17:22:44','','http://www.otandp.com/'),(186,'OT & P Healthcare','','Hong Kong','','Hong Kong','109 Repulse Bay Rd, Repulse Bay, Hong Kong','','+ 852 2813 1978   ','',1,'MedPay',0,'','OT & P Healthcare Repulse Bay',900.00,0.00,'+ 852 2813 2998 ','2016-03-01 17:28:35','G205, the Repulse Bay','http://www.otandp.com/'),(187,'OT & P Healthcare','','Hong Kong','','Hong Kong','Clear Water Bay Rd & Razor Hill Rd, Clear Water Bay','','+ 852 2719 6366   ','',1,'MedPay',0,'','OT & P Healthcare The Bay Practice',900.00,0.00,'+  852 2719 9262','2016-03-01 17:31:45','1/F, Razor Hill Dairy Farm Shopping Centre','http://www.otandp.com/'),(188,'OT & P Healthcare','','Hong Kong','','Hong Kong','D\'Aguilar Street, 1, Central, Hong Kong','','+ 852 2521 3181   ','',1,'MedPay',0,'','OT & P Healthcare Central 1',900.00,0.00,'+ 852 2521 3858','2016-03-01 17:35:22','5/F, Century Square','http://www.otandp.com/'),(189,'The London Medical Clinic','','Hong Kong','','Hong Kong','Duddell Street, 1, Central, Hong Kong','','+ 852 2537 6898','info@thelondonmedicalclinic.com',1,'MedPay',0,'','The London Medical Clinic',900.00,0.00,'+852 2537 6000','2016-03-01 17:38:25','9th Floor above the Shanghai Tang store','http://www.thelondonmedicalclinic.com/');
/*!40000 ALTER TABLE `doctors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experties`
--

DROP TABLE IF EXISTS `experties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experties` (
  `expert_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experties`
--

LOCK TABLES `experties` WRITE;
/*!40000 ALTER TABLE `experties` DISABLE KEYS */;
INSERT INTO `experties` VALUES (1,13),(1,21),(1,22),(1,24),(1,25),(1,27),(2,27),(1,23),(1,56),(4,57),(4,76),(1,138),(1,45),(1,33),(4,139),(1,140),(2,140),(1,141),(1,103),(1,104),(1,105),(6,105),(1,106),(4,107),(1,39),(1,40),(4,41),(4,42),(1,34),(1,35),(1,36),(1,37),(4,38),(1,63),(1,64),(1,65),(1,66),(1,67),(4,73),(1,68),(1,69),(1,70),(4,71),(4,72),(1,60),(1,59),(1,61),(4,62),(4,74),(1,75),(1,77),(1,78),(1,79),(4,90),(4,91),(1,92),(1,93),(1,88),(4,89),(1,80),(4,81),(1,82),(1,142),(2,142),(1,143),(4,145),(1,144),(1,83),(4,83),(1,84),(1,85),(1,86),(1,87),(1,12),(1,14),(1,15),(1,16),(1,17),(4,18),(4,19),(4,20),(1,94),(1,95),(1,96),(1,97),(1,98),(4,99),(4,100),(1,101),(1,102),(1,150),(4,151),(1,152),(1,153),(1,146),(1,147),(1,148),(2,148),(4,149),(1,108),(2,108),(1,28),(1,29),(1,31),(1,32),(1,30),(1,43),(1,44),(1,46),(1,47),(1,48),(1,49),(1,50),(1,54),(1,55),(4,58),(1,51),(1,52),(1,53),(1,109),(1,110),(1,111),(2,111),(3,111),(1,112),(4,113),(1,114),(2,114),(1,115),(2,115),(1,116),(2,116),(1,117),(2,117),(1,118),(2,118),(1,119),(1,120),(1,121),(1,122),(1,123),(1,124),(1,125),(1,126),(1,127),(4,128),(1,129),(1,133),(2,133),(1,130),(2,130),(1,131),(2,131),(1,132),(2,132),(1,134),(2,134),(1,135),(1,136),(4,137),(4,154),(1,155),(2,155),(1,156),(1,157),(2,157),(1,158),(2,158),(1,159),(2,159),(1,160),(2,160),(1,161),(2,161),(3,161),(4,161),(4,162),(1,163),(2,163),(4,164),(1,165),(1,166),(1,167),(4,167),(4,168),(1,169),(2,169),(1,170),(10,170),(1,171),(1,172),(1,173),(1,174),(1,175),(2,175),(3,175),(5,175),(6,175),(7,175),(8,175),(9,175),(10,175),(11,175),(1,176),(2,176),(4,177),(1,178),(2,178),(1,179),(2,179),(4,179),(1,180),(2,180),(1,181),(2,181),(4,182),(1,183),(2,183),(1,184),(2,184),(1,185),(2,185),(1,186),(2,186),(1,187),(2,187),(1,188),(2,188),(1,189),(2,189);
/*!40000 ALTER TABLE `experties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experties_description`
--

DROP TABLE IF EXISTS `experties_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experties_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expert` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experties_description`
--

LOCK TABLES `experties_description` WRITE;
/*!40000 ALTER TABLE `experties_description` DISABLE KEYS */;
INSERT INTO `experties_description` VALUES (1,'משפחה / פנימי'),(2,'ילדים'),(3,'גניקולוגיה'),(4,'שיניים'),(5,'אף אוזן גרון'),(6,'אורטופדיה'),(7,'עיניים'),(8,'גסטרואנטרולוג'),(9,'נוירולוגיה'),(10,'עור'),(11,'כירורגיה'),(12,'אחר');
/*!40000 ALTER TABLE `experties_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `claim_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_description`
--

DROP TABLE IF EXISTS `language_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_description`
--

LOCK TABLES `language_description` WRITE;
/*!40000 ALTER TABLE `language_description` DISABLE KEYS */;
INSERT INTO `language_description` VALUES (1,'אנגלית'),(2,'ערבית'),(3,'ספרדית'),(4,'צרפתית'),(5,'סינית'),(6,'הולנדית'),(7,'איטלקית'),(8,'רומנית'),(9,'צ\'כית'),(10,'עברית'),(11,'גרמנית');
/*!40000 ALTER TABLE `language_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `language_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,13),(1,21),(1,22),(1,24),(6,24),(1,25),(6,25),(11,25),(1,27),(2,27),(1,23),(6,23),(1,45),(6,45),(11,45),(1,33),(3,33),(1,139),(3,139),(1,140),(3,140),(1,141),(3,141),(1,103),(3,103),(3,104),(1,105),(3,105),(4,105),(7,105),(11,105),(1,106),(3,106),(1,107),(3,107),(11,39),(11,40),(11,41),(11,42),(1,34),(10,34),(11,34),(11,35),(11,36),(11,37),(11,38),(11,63),(11,64),(11,65),(1,66),(4,66),(11,66),(11,67),(11,73),(11,68),(1,69),(11,69),(1,70),(11,70),(1,71),(11,71),(1,72),(11,72),(1,60),(4,60),(11,60),(1,59),(4,59),(11,59),(1,61),(4,61),(11,61),(1,62),(3,62),(4,62),(11,62),(1,74),(11,74),(1,75),(11,75),(1,77),(4,77),(11,77),(1,78),(4,78),(11,78),(1,79),(11,79),(1,90),(4,90),(11,90),(1,91),(4,91),(11,91),(1,92),(4,92),(11,92),(1,93),(11,93),(1,88),(11,88),(1,89),(4,89),(7,89),(11,89),(1,80),(8,80),(1,81),(8,81),(1,82),(8,82),(1,142),(4,142),(1,143),(4,143),(6,143),(1,145),(4,145),(6,145),(1,144),(4,144),(6,144),(1,83),(4,83),(1,84),(4,84),(1,85),(4,85),(1,86),(4,86),(1,87),(4,87),(1,12),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,94),(11,94),(1,95),(11,95),(1,96),(11,96),(1,97),(11,97),(1,98),(11,98),(1,99),(11,99),(1,100),(11,100),(11,101),(11,102),(1,150),(4,150),(7,150),(1,151),(7,151),(11,151),(1,152),(7,152),(1,153),(7,153),(1,146),(1,147),(1,148),(4,148),(1,149),(11,149),(1,108),(1,28),(3,28),(1,29),(3,29),(1,31),(3,31),(1,32),(1,30),(3,30),(1,43),(1,44),(1,46),(1,47),(1,48),(1,49),(1,50),(1,54),(1,55),(1,58),(1,51),(1,52),(1,53),(1,109),(1,110),(1,111),(1,112),(1,113),(1,114),(1,115),(1,116),(1,117),(1,118),(1,119),(1,120),(1,121),(1,122),(1,123),(1,124),(1,125),(1,126),(1,127),(1,128),(1,129),(1,133),(1,130),(1,131),(1,132),(1,134),(1,135),(1,136),(1,137),(1,154),(1,155),(1,156),(1,157),(1,158),(1,159),(1,160),(1,161),(1,162),(1,163),(1,164),(9,164);
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating` (
  `user_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personal_id` varchar(255) NOT NULL,
  `born_date` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `policy_number` int(255) NOT NULL,
  `policy_begin` varchar(255) NOT NULL,
  `policy_end` varchar(255) NOT NULL,
  `policy_type` varchar(255) NOT NULL,
  `under_eighteen` tinyint(1) NOT NULL,
  `under_name` varchar(255) NOT NULL,
  `under_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-02 14:46:35
