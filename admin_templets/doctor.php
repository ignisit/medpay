			<?php  include 'header.php'  ?>

			
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar" style="display:none">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Data Tables</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Responsive Datatables</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="note note-success">
						<p>
							 פרטי רופא עודכנו בהצלחה
						</p>
					</div>

					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>רשימת רופאים
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-bordered">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										 שם הרופא
									</th>
									<th>
										 התמחות
									</th>
									<th>
										 מיקום
									</th>
									<th>
										 שפות
									</th>
									<th>
										 תאריך עדכון אחרון
									</th>
									<th>
										 מספר מבוטחים
									</th>
                                    <th>
										 דירוג ממוצע
									</th>
                                    <th>
										 סטטוס
									</th>
                                    <th>
										 מחיר בסיסי
									</th>
                                    <th>
										 כרטיס רופא
									</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>
										 1
									</td>
									<td>
										 דוריס דיי
									</td>
									<td>
										 עור
									</td>
									<td>
										 ניו יורק
									</td>
									<td>
										 אנגלית
									</td>
									<td>
										 22 june 2015
									</td>
									<td>
										 200
									</td>
                                    <td>
										 5
									</td>
                                    <td>
										 פעיל
									</td>
                                    <td>
										 22 דולר
									</td>
                                    <td>
                                        <a href="edit_doctor.php">כרטיס רופא</a>
									</td>
								</tr>
								<tr>
									<td>
										 2
									</td>
									<td>
										 דוריס דיי
									</td>
									<td>
										 עור
									</td>
									<td>
										 ניו יורק
									</td>
									<td>
										 אנגלית
									</td>
									<td>
										 22 june 2015
									</td>
									<td>
										 200
									</td>
                                    <td>
										 5
									</td>
                                    <td>
										 ממתין לאישור
									</td>
                                    <td>
										 22 דולר
									</td>
                                    <td>
										 <a href="edit_doctor.php">כרטיס רופא</a>
									</td>
								</tr>
								<tr>
									<td>
										 3
									</td>
									<td>
										דוריס דיי
									</td>
									<td>
										 עור
									</td>
									<td>
										 ניו יורק
									</td>
									<td>
										 אנגלית
									</td>
									<td>
										 22 june 2015
									</td>
									<td>
										 200
									</td>
                                    <td>
										 5
									</td>
                                    <td>
										 לא פעיל
									</td>
                                    <td>
										 22 דולר
									</td>
                                    <td>
										 <a href="edit_doctor.php">כרטיס רופא</a>
									</td>
								</tr>
								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
			</div>
			
<?php  include 'footer.php'  ?>