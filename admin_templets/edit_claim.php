
<?php  include 'header.php'  ?>

			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->

			<div class="page-bar" style="display:none">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Form Stuff</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Material Design Form Controls</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
              <div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase"> פרטי תביעה</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal">
								<div class="form-body">
									<div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">שם המבוטח</label>
										<div class="col-md-10">
											<input type="text" class="form-control header_form" id="form_control_1" placeholder="Enter your name" value="ישראל ישראלי" readonly>
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                    <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">שם חברת הביטוח</label>
										<div class="col-md-10">
											<input type="text" class="form-control header_form" id="form_control_1" placeholder="Enter your name" value="מגדל" readonly>
											<div class="form-control-focus">
											</div>
										</div>
									</div>

								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
            
            
			<div class="row">
				<div class="col-md-6 ">
                    
                <!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered" style="min-height:390px">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">פרטי מבוטח</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
                             	<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מחיר של המבוטח האחרון" value="ישראל ישראלי" readonly>
										<label for="form_control_1">שם המבוטח</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מחיר של המבוטח האחרון" value="123456789" readonly>
										<label for="form_control_1">ת״ז</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                  <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מגדל" value="9/9/1990" readonly>
										<label for="form_control_1">תאריך לידה</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                     <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מגדל" value="9/9/2015" readonly>
										<label for="form_control_1">תאריך יציאה מהארץ</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מגדל" value="11/9/2015" readonly>
										<label for="form_control_1">תאריך האירוע</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מגדל" value="september 18 2015 22:00" readonly>
										<label for="form_control_1">תאריך הגשת התביעה</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="ארצות הברית" value="ארצות הברית" readonly>
										<label for="form_control_1">ארץ</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="ניו יורק" value="ניו יורק" readonly>
										<label for="form_control_1">עיר</label>
										<span class="help-block">Some help goes here...</span>
									</div>
								</div>

							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
                    
                      <!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered" style="min-height:390px">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">פרטי ברטיס אשראי</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
                                <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מגדל" value="123456789012345" readonly>
										<label for="form_control_1">מספר כרטיס</label>
										<span class="help-block">Some help goes here...</span>
									</div>    
                                    
                             	<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מחיר של המבוטח האחרון" value="11/2015" readonly>
										<label for="form_control_1">תוקף</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מחיר של המבוטח האחרון" value="123" readonly>
										<label for="form_control_1">שלוש ספרות בגב הכרטיס</label>
										<span class="help-block">Some help goes here...</span>
									</div>
								</div>

							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
                    
                    
                    <!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered" style="min-height:390px">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">פרטי פוליסה</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
                                <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מגדל" value="מגדל" readonly>
										<label for="form_control_1">חברת ביטוח</label>
										<span class="help-block">Some help goes here...</span>
									</div>    
                                    
                             	<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מחיר של המבוטח האחרון" value="1מגדל11222015" readonly>
										<label for="form_control_1">מספר סידורי</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מחיר של המבוטח האחרון" value="146" readonly>
										<label for="form_control_1">מספר פוליסה</label>
										<span class="help-block">Some help goes here...</span>
									</div>
								</div>

							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
                    
                    
                    
                  <!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered" style="min-height:390px">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">פרטי רופא</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
                                <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מגדל" value="דוריס דיי" readonly>
										<label for="form_control_1">שם הרופא</label>
										<span class="help-block">Some help goes here...</span>
									</div>    
                                    
                             	<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מחיר של המבוטח האחרון" value="פעיל" readonly>
										<label for="form_control_1">סטאטוס רופא במערכת</label>
										<span class="help-block">Some help goes here...</span>
									</div>
								</div>

							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
                    
                    
                    
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">מסמכים מצורפים</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
                                   <ul>
                                    <li>להורדת סיכום רפואי - <a href="#">לחץ כאן</a></li>
                                     <li>להורדת קבלה - <a href="#">לחץ כאן</a></li>
                                        <li>להורדת מרשם - <a href="#">לחץ כאן</a></li>
                                    </ul>
 
								</div>

							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
                    
            <!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">סכום נדרש</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
                                    <table border="1" id="list-doctor" style="width:100%">
                                      <tr>
                                          <th>קבלה</th>
                                          <th>סכום</th> 
                                      </tr>
                                      <tr>
                                         <td>קבלה</td>
                                         <td>110 ש״ח</td>
                                      </tr>
                                      <tr>
                                         <td>קבלה</td>
                                         <td>110 ש״ח</td>
                                      </tr>
                                      <tr>
                                         <td>קבלה</td>
                                         <td>110 ש״ח</td>
                                      </tr>
                                    
                                    </table>
                                    <br>
                                    <div class="sum">סכום:  &nbsp; 330  ש״ח</div>
								</div>

							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
                    
                    
                    
                    
				</div>
				<div class="col-md-6 ">

            	<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered" style="height: 300px;">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">סטטוס תביעות</span>
							</div>

						</div>
                            <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">סטטוס תביעות</label>
										<div class="col-md-10">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" id="checkbox32" class="md-check">
													<label for="checkbox30">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													התקבל </label>
												</div>
												<div class="md-checkbox">
													<input type="checkbox" id="checkbox32" class="md-check" checked>
													<label for="checkbox31">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													אושר במלואו (בשעה 22:00) </label>
												</div>
												<div class="md-checkbox">
													<input type="checkbox" id="checkbox32" class="md-check">
													<label for="checkbox32">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													אושר חלקית (בשעה ) </label>
												</div>
                                            <div class="md-checkbox">
													<input type="checkbox" id="checkbox32" class="md-check">
													<label for="checkbox32">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													ממתין למבוטח/השלמת מסמכים</label>
												</div>
                                            <div class="md-checkbox">
													<input type="checkbox" id="checkbox32" class="md-check">
													<label for="checkbox32">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													ממתין לחברת ביטוח </label>
												</div>
											</div>
										</div>
									</div>
					</div>
                    <!-- END SAMPLE FORM PORTLET-->
                    
                    <!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">התראות</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
                                  <ul>
                                   <li>הריון- מבוטחת הצהירה שמדובר בהריון מרובה עוברים</li>    
                                    <li>תאריך הפוליסה סמוך לתאריך יציאה מהארץ (3 ימים)</li>
                                </ul>
								</div>
	
							</form>
						</div>
					</div>
                     <!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>




<?php  include 'footer.php'  ?>