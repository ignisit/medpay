
<?php  include 'header.php'  ?>

			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->

			<div class="page-bar" style="display:none">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Form Stuff</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Material Design Form Controls</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
              <div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase"> פרטי הרופא</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal">
								<div class="form-body">
									<div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">שם</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="Enter your name" value="דוריס דיי">
											<div class="form-control-focus">
											</div>
										</div>
									</div>

									<div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">סטטוס</label>
										<div class="col-md-10">
											<select class="form-control" id="form_control_1">
												<option value=""></option>
												<option value="">ממתין לאישור</option>
												<option value="" selected>פעיל</option>
												<option value="">לא פעיל</option>
												<option value="">רשימה שחורה</option>
											</select>
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                    
                                    	<div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">מספר רישיון</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="Enter your name" value="12345678">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>
            
            
			<div class="row">
				<div class="col-md-6 ">
                    
                    					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered" style="min-height:483px">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">פרטים כלליים</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
                                	<div class="form-group form-md-line-input has-info">
										<select class="form-control" id="form_control_1">
											<option value=""></option>
											<option value="1" selected>MedPay</option>
											<option value="2">מבוטח</option>
										</select>
										<label for="form_control_1">כיצד נוצר</label>
									</div>
                                    
									<div class="form-group form-md-line-input has-info">
										<select class="form-control" id="form_control_1">
											<option value=""></option>
											<option value="1" selected>עור</option>
											<option value="2">פלסיקה</option>
											<option value="3">כירורגיה</option>
											<option value="4">פסיכיאטריה</option>
										</select>
										<label for="form_control_1">התמחות</label>
									</div>
                                    
                            <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מרפאה" value="מרפאה פרטית">
										<label for="form_control_1">מרפאה</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    
                            <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">שפות</label>
										<div class="col-md-10">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" id="checkbox30" class="md-check" checked>
													<label for="checkbox30">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													אנגלית </label>
												</div>
												<div class="md-checkbox has-error">
													<input type="checkbox" id="checkbox31" class="md-check" checked>
													<label for="checkbox31">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													עברית </label>
												</div>
												<div class="md-checkbox has-warning">
													<input type="checkbox" id="checkbox32" class="md-check">
													<label for="checkbox32">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													ערבית </label>
												</div>
											</div>
										</div>
									</div>

								</div>

							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
                    
                    
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">פרטי התקשרות</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
                             	<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="הכנס טלפון נייד" value="0547807568">
										<label for="form_control_1">טלפון נייד</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                   <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="הכנס טלפון נייח" value="097717568">
										<label for="form_control_1">טלפון נייח</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                   <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="הכנס מייל" value="mail@mail.com">
										<label for="form_control_1">מייל</label>
										<span class="help-block">Some help goes here...</span>
									</div>
									<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="הכנס רחוב" value="רחוב 52">
										<label for="form_control_1">כתובת מלאה</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    		<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="הכנס עיר" value="ניו יורק">
										<label for="form_control_1">עיר</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    		<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="הכנס מדינה" value="ניו יורק">
										<label for="form_control_1">מדינה</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                    <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="הכנס ארץ" value="ארצות הברית">
										<label for="form_control_1">ארץ</label>
										<span class="help-block">Some help goes here...</span>
									</div>
									<div class="form-group form-md-line-input has-error">
										<input type="text" class="form-control" readonly value="5" id="form_control_1">
										<label for="form_control_1">דירוג</label>
									</div>
								</div>
								<div class="form-actions noborder">
									<button type="button" class="btn blue">שלח</button>
									<button type="button" class="btn default">ביטול</button>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
				<div class="col-md-6 ">

            	<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">מחירים</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
                             	<div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מחיר של המבוטח האחרון" value="54 דולר">
										<label for="form_control_1">מחיר של המבוטח האחרון</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                   <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="ממוצע של כל המבוטחים" value="60 דולר">
										<label for="form_control_1">ממוצע של כל המבוטחים</label>
										<span class="help-block">Some help goes here...</span>
									</div>
                                   <div class="form-group form-md-line-input">
										<input type="text" class="form-control" id="form_control_1" placeholder="מחיר ממוצע באותה עיר" value="40 דולר">
										<label for="form_control_1">מחיר ממוצע באותה עיר</label>
										<span class="help-block">Some help goes here...</span>
									</div>

								</div>
	
							</form>
						</div>
					</div>
                    <!-- END SAMPLE FORM PORTLET-->
                    
                    <!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">מבוטחים שהיו אצל הרופא</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form">
								<div class="form-body">
                             	<table border="1" style="width: 100%;" id="list-doctor">
                                    <tr>
                                        <th>id</th>
                                        <th>שם</th>
                                        <th>מתי נוצר</th>
                                        <th>כמה שילמו</th>
                                        <th>דירוג</th>
                                        <th>ביקורת</th>
                                    </tr>
                                      <tr>
                                          <td>123456789</td>
                                          <td>יוסי יוסף</td>
                                          <td>22 ביוני 2015</td>
                                          <td>22 דולר</td>
                                          <td>5</td>
                                          <td>היה רופא מצויין</td>
                                    </tr>
                                        <tr>
                                          <td>123456789</td>    
                                          <td>יוסי יוסף</td>
                                          <td>22 ביוני 2015</td>
                                          <td>22 דולר</td>
                                          <td>5</td>
                                          <td>היה רופא מצויין</td>
                                    </tr>
                                        <tr>
                                          <td>123456789</td>    
                                          <td>יוסי יוסף</td>
                                          <td>22 ביוני 2015</td>
                                          <td>22 דולר</td>
                                          <td>5</td>
                                          <td>היה רופא מצויין</td>
                                    </tr>
                                </table>
								</div>
	
							</form>
						</div>
					</div>
                     <!-- END SAMPLE FORM PORTLET-->
				</div>
			</div>




<?php  include 'footer.php'  ?>