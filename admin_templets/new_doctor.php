
<?php  include 'header.php'  ?>
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->

			<div class="page-bar" style="display: none">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Form Stuff</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Material Design Form Controls</a>
					</li>
				</ul>

			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->

			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-green-haze">
								<i class="icon-settings font-green-haze"></i>
								<span class="caption-subject bold uppercase">רופא חדש</span>
							</div>

						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal">
								<div class="form-body">
									<div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">שם</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="שם הרופא">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                    <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">התמחות</label>
										<div class="col-md-10">
											<select class="form-control" id="form_control_1">
												<option value=""></option>
												<option value="">עור</option>
												<option value="">כירורגיה</option>
												<option value="">פלסטיקה</option>
												<option value="">פסיכיאטריה</option>
											</select>
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                    <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">קליניקה</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="פרטית או בית רופאים">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                     <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">מחיר במטבע מקומי</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="מחיר במטבע מקומי">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                     <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">מחיר בדולר</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="מחיר בדולר">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                     <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">רחוב</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="רחוב">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                    <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">עיר</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="עיר">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                     <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">מדינה</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="מדינה">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                     <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">ארץ</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="ארץ">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                     <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">טלפון נייד</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="טלפון נייד">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                     <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">טלפון נייח</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="טלפון נייח">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                     <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">פקס</label>
										<div class="col-md-10">
											<input type="text" class="form-control" id="form_control_1" placeholder="פקס">
											<div class="form-control-focus">
											</div>
										</div>
									</div>
                                   <div class="form-group form-md-line-input">
										<label class="col-md-2 control-label" for="form_control_1">שפות</label>
										<div class="col-md-10">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" id="checkbox30" class="md-check">
													<label for="checkbox30">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													אנגלית </label>
												</div>
												<div class="md-checkbox has-error">
													<input type="checkbox" id="checkbox31" class="md-check">
													<label for="checkbox31">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													עברית </label>
												</div>
												<div class="md-checkbox has-warning">
													<input type="checkbox" id="checkbox32" class="md-check">
													<label for="checkbox32">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>
													ערבית </label>
												</div>
											</div>
										</div>
									</div>

								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-2 col-md-10">
                                            <button type="button" class="btn blue">שלח</button>
											<button type="button" class="btn default">ביטול</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
                    
				</div>
			</div>
			
<?php  include 'footer.php'  ?>